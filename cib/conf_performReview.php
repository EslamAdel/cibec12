<?php
session_start();
  include("conf_common.php");
  
	$uid 		= $_GET['id'];
	$pid 		= $_GET['pid'];
	$nrv		= $_GET['nrv'];


		if($nrv == "true")
			$message = "    Please complete the following fields carefully. ";	
		else if($nrv == "false")
			$message = "    Error in uploading review...Please try again ";	
		
    if(!isset($_SESSION['id']))
  {
    $str = sprintf("Location: submit.html");
		header($str);
  }

		printSubmitNewReviewPage($uid, $pid, $message, "info.png");




function printSubmitNewReviewPage( $userNum, $paperNum, $message, $icon)
{

	printUpperBanner();
	printReviewForm( $userNum, $paperNum,  $message, $icon);
	printFooter();
}


function printReviewForm( $userNum, $paperNum, $message, $icon)
{

	include("conn.php");
	
  $revresult=mysqli_query($link, "SELECT paper.*,user.*,rev.*,paper.title as ptitle from paper,user,rev where rev.pid=paper.id and rev.rev=user.id and rev.rev=$userNum and paper.id=$paperNum ");
  $rev_row = mysqli_fetch_array( $revresult );
  
	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
        printf("<tr><td style='border-width:0;'><P>&nbsp;</P>\n");
		printf(" <tr><td style='border-width:0;'  >\n");

    
     printf("<br><div class=title>\n");	
    printf("<table width =100%%>\n");	
    printf("<td style='border-width:0;'   align=left><img src=\"user.png\"><i><b>%s %s - %s</b></i>\n", $rev_row['firstname'], $rev_row['lastname'],$userNum);
    printf("<td style='border-width:0;'   align=right><a href=\"conf_showPaperList.php?id=%s&start=0\">
	<img src=\"openfolder.png\" alt=\"My Papers\"/></a><i>My Papers</i>\n", $userNum);
    printf("<a href=\"conf_submitNewPaper.php?id=%s\">
	<img src=\"arrow.png\" alt=\"Submit a paper\"/></a><i>Submit Paper</i>\n", $userNum );
	 printf("<a href=\"conf_logout.php\"><img src=\"logout.png\" alt=\"Logout\"/></a><i>Logout</i>\n");
    
    printf("</table>");
    printf("</div>\n");

     printf("<div class=info_background>\n");
   printf("<p><b>CIBEC 2012 Manuscript Evaluation Form</b>\n");
     printf("<br>Thank you for taking part in the review process of CIBEC 2012 manuscripts.
	Your contribution to CIBEC 2012 is highly appreciated. \n");
    printf("<p>Please rate your satisfaction level with each of the following statements:\n");
    printf("<br>1 = very dissatisfied \n");
    printf("<br>2 = somewhat dissatisfied \n");
    printf("<br>3 = neutral \n"); 
    printf("<br>4 = somewhat satisfied \n");
    printf("<br>5 = very satisfied \n");
   
   
    printf("<p>\n");
    

    $revForm[0]['text'] 	= "Relevance to the conference";
    $revForm[0]['tag'] = "relevance";
    $revForm[1]['text'] = "Scientific merit of contents ";
    $revForm[1]['tag'] = "merit";
    $revForm[2]['text'] = "Clarity and novelty of methdology";
    $revForm[2]['tag'] = "clarity";
    $revForm[3]['text'] = "Experimental verification";
    $revForm[3]['tag'] = "verification";
    $revForm[4]['text'] = "Analysis of results and discussion of methods advantages/limitations";
    $revForm[4]['tag'] = "analysis";
    $revForm[5]['text'] = "Review of literature and comparison to previous techniques";
    $revForm[5]['tag'] = "literature";
   $revForm[6]['text'] = "Potential for clinical application";
    $revForm[6]['tag'] = "application";

    
    $revForm[7]['text'] = "Paper style and English language";
    $revForm[7]['tag'] = "language";
   

     printf("<div class=info>\n");
    printf("<table>\n");
	 printf("<form enctype=\"multipart/form-data\" action=\"conf_submitPaperReview.php\" method=\"post\">\n");
	

	printf("<tr><td style='border-width:0;'  >Paper id  : <b>%s</b></td>\n", $paperNum);
	printf("<tr><td style='border-width:0;'   colspan = 6 ><b>%s</b></td>\n", $rev_row['ptitle']);

	printf("<tr><td style='border-width:0;'  >&nbsp\n");

	printf("<tr><td style='border-width:0;'   colspan=6 align=center><hr></td>\n");
	printf("<tr><td style='border-width:0;'   colspan=6 align=center><b>Review Form</b></td>\n");
	printf("<tr><td style='border-width:0;'   colspan=6 align=center><hr></td>\n");
	printf("<tr><td style='border-width:0;'  >&nbsp\n");


	printf("<tr><td style='border-width:0;'  >\n");
	printf("<td style='border-width:0;'  >Worst\n");
	printf("<td style='border-width:0;'  ><td style='border-width:0;'  ><td style='border-width:0;'  ><td style='border-width:0;'  >Best\n");

	printf("<tr><td style='border-width:0;'  >\n");
	for($j=0; $j< 5; $j++)
		printf("<td style='border-width:0;'   valign=top align=center>%d\n", $j+1);

/*
	for($i=0; $i < 8; $i++)
	{
		printf("<tr><td style='border-width:0;'   valign=top><i>%s</i></td>\n", $revForm[$i]['text']);
		for($j=0; $j< 5; $j++)
		{
		     if($j == 2)
		       printf("<td style='border-width:0;'   valign=top align=center width=6%%><input type =\"radio\"  name=\"%s\" value=\"%d\" checked>\n", $revForm[$i]['tag'], $j+1);
		     else
		       printf("<td style='border-width:0;'   valign=top align=center width=6%%><input type =\"radio\"  name=\"%s\" value=\"%d\">\n", $revForm[$i]['tag'], $j+1);
		}
	}
*/	
	
      for($i=0; $i < 8; $i++)
	{
	printf("<tr><td style='border-width:0;'   valign=top><i>%s</i></td>\n", $revForm[$i]['text']);
	for($j=0; $j< 5; $j++)
	{
     if($j == 2 && $rev_row[$revForm[$i]['tag']]==null)
	   printf("<td style='border-width:0;'   valign=top align=center width=6%%><input type =\"radio\"  name=\"%s\" value=\"%d\" checked>\n", $revForm[$i]['tag'], $j+1);
	   else if($j == $rev_row[$revForm[$i]['tag']] -1)
	   printf("<td style='border-width:0;'   valign=top align=center width=6%%><input type =\"radio\"  name=\"%s\" value=\"%d\" checked>\n", $revForm[$i]['tag'], $j+1);
     else
     printf("<td style='border-width:0;'   valign=top align=center width=6%%><input type =\"radio\"  name=\"%s\" value=\"%d\">\n", $revForm[$i]['tag'], $j+1);

       }
    }


	printf("<tr><td style='border-width:0;'  >&nbsp\n");
	printf("<tr><td style='border-width:0;'   valign=top><b>Overall Recommendation</b></td>\n");


/*
	printf("<td style='border-width:0;'   colspan = 5 align=center><input type =\"radio\"  name=\"status\" value=\"accepted\" checked>Accept\n");
	printf("<input type =\"radio\"  name=\"status\" value=\"rejected\">Reject\n");
	
	printf("<tr><td style='border-width:0;'  >&nbsp\n");
	printf("<tr><td style='border-width:0;'   valign=top><b>Comments</b></td>\n");
	printf("<tr><td style='border-width:0;'   colspan=6><textarea  rows=\"15\" cols=\"65\" name=\"comments\"></textarea>\n");
*/	


	    if(strcmp($rev_row['accept'] ,"accepted")==0)
	    {
		printf("<td style='border-width:0;'   colspan = 5 align=center><input type =\"radio\"  name=\"status\" value=\"accepted\" checked>Accept\n");
		printf("<input type =\"radio\"  name=\"status\" value=\"rejected\">Reject\n");
	
	    }
	   else
	     {
		printf("<td style='border-width:0;'   colspan = 5 align=center><input type =\"radio\"  name=\"status\" value=\"accepted\">Accept\n");
		printf("<input type =\"radio\"  name=\"status\" value=\"rejected\" checked>Reject\n");
	
	      }

	
        printf("<tr><td style='border-width:0;'  >&nbsp\n");
	printf("<tr><td style='border-width:0;'   valign=top><b>Comments to author</b></td>\n");

		printf("<tr><td style='border-width:0;'   colspan=6><textarea  rows=\"15\" cols=\"65\" name=\"comments\">%s</textarea>\n", $rev_row['comments']);



	
	printf("<input type=\"hidden\" value=\"%s\" name=\"id\" />\n", $userNum);
    	printf("<input type=\"hidden\" value=\"%s\" name=\"pid\" />\n",$paperNum);

	printf("<tr><td style='border-width:0;'   colspan=6><img src=\"upload.png\" valign=bottom ><input type=\"submit\" value=\"Submit Review\" />\n");


	printf("</form></table>\n");

    
    
	printf("</form></table>\n");

 printf("</div></div>\n");

printf("</table>\n");

}

?>
