<?php
session_start();

  if(!isset($_SESSION['id']))
  {
    $str = sprintf("Location: submit.html");
		header($str);
}

	include("conf_common.php");
	//include("conf_readPaperReview.php");

	$pid 		= $_GET['pid'];
	$uid 		= $_GET['id'];

  printShowReviewPage($uid, $pid, $message, $icon);
	   


function printShowReviewPage( $userNum, $paperNum, $message, $icon)
{

	printUpperBanner();
	printShowPaperReviewForm($userNum, $paperNum, $message, $icon);
	printFooter();
}




function printShowPaperReviewForm($userNum, $paperNum, $message, $icon)
{
 	include("conn.php");
 	
 $userresult=mysqli_query($link, "SELECT * from user where id=$userNum  ");
 $user_row = mysqli_fetch_array( $userresult );

 $paperresult=mysqli_query($link, "SELECT * from paper where id=$paperNum  ");
 $paper_row = mysqli_fetch_array( $paperresult );
	    
	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
        printf("<tr><td style='border-width:0;'  ><P>&nbsp;</P>\n");
		printf(" <tr><td style='border-width:0;'  >\n");


    printf("<br><div class=title>\n");
    printf("<table width =100%%>\n");	
    //printf("<td style='border-width:0;'  ><H1>CIBEC 2012 Paper Submission</H1>\n");
    printf("<td style='border-width:0;'   align=left><img src=\"user.png\"><i><b>%s %s - %s</b><i>\n", $user_row['firstname'], $user_row['lastname'],$userNum);
    printf("<td style='border-width:0;'   align=right><a href=\"conf_showPaperList.php?id=%s&start=0&nu=false\">
	<img src=\"openfolder.png\" alt=\"My Papers\"/></a><i>My papers</i>\n", $userNum );
    printf("<a href=\"conf_submitNewPaper.php?id=%s\">
	<img src=\"arrow.png\" alt=\"Submit a paper\"/></a><i>Submit Paper</i>\n", $userNum);
    
    printf("</table>");

    printf("</div>\n");

    
   
    printf("<div class=info_background>\n");
    

    printf("<div class=info>\n");
    printf("<table width=100%%>\n");
	
	printf("<tr><td style='border-width:0;'   align=right colspan=6><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i>Final Status: <b> %s</b></i></td>\n", $paper_row['status']);

	
	
	printf("<tr><td style='border-width:0;'   >&nbsp\n");
	printf("<tr><td style='border-width:0;'   align=center colspan=6><FONT SIZE=\"4\" FACE=\"Verdana, Arial\" ><b>%s</b></td>\n", $paper_row['title']);
	printf("<tr><td style='border-width:0;'   align=center colspan=6><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i>Paper ID: %s</i></td>\n", $paper_row['id']);
	
	
	$revForm[0]['text'] 	= "Relevance to the conference";
   	$revForm[0]['tag'] 	= "relevance";
    	$revForm[1]['text'] 	= "Scientific merit of contents";
    	$revForm[1]['tag'] 	= "merit";
    	$revForm[2]['text'] 	= "Clarity and novelty of methdology";
    	$revForm[2]['tag'] 	= "clarity";
    	$revForm[3]['text'] 	= "Experimental verification";
    	$revForm[3]['tag'] 	= "verification";
    	$revForm[4]['text'] 	= "Analysis of results and discussion of methods advantages/limitations";
    	$revForm[4]['tag'] 	= "analysis";
    	$revForm[5]['text'] 	= "Review of literature and comparison to previous techniques";
    	$revForm[5]['tag'] 	= "literature";
   	$revForm[6]['text'] 	= "Potential for clinical application";
    	$revForm[6]['tag'] 	= "application";
	$revForm[7]['text'] 	= "Paper style and English language";
    	$revForm[7]['tag'] 	= "language";
	

  $revresult=mysqli_query($link, "SELECT *,paper.title as ptitle from rev,user,paper where rev.pid=paper.id and rev.rev=user.id and pid=$paperNum  ");
	$numReviews = mysqli_num_rows( $revresult );
	if($numReviews==0) printf("<p><tr><td style='border-width:0;'  ><i>No reviews available</i></td>\n");
	else
	{
  while($rev_row = mysqli_fetch_array( $revresult ))
		{

			printf("<tr><td style='border-width:0;'  >&nbsp\n");
			printf("<tr><td style='border-width:0;'   colspan=6 align=center><hr></td>\n");
			printf("<tr><td style='border-width:0;'  ><b>Reviewer ID :</b> %s \n" , $rev_row['rev']);
                        printf("<tr><td style='border-width:0;'  ><b>Reviewer:</b> %s %s\n" , $rev_row['firstname'], $rev_row['lastname']);
			printf("<td style='border-width:0;'   colspan=6 align=right><i>%s </i></td>\n" , $rev_row['date']);
			printf("<tr><td style='border-width:0;'   colspan=6 align=center><hr></td>\n");
			printf("<tr><td style='border-width:0;'  >&nbsp\n");

			
			printf("<tr><td style='border-width:0;'  >\n");
			printf("<td style='border-width:0;'  >Worst\n");
			printf("<td style='border-width:0;'  ><td style='border-width:0;'  ><td style='border-width:0;'  ><td style='border-width:0;'  >Best\n");
			printf("<tr><td style='border-width:0;'  >\n");
			for($j=0; $j< 5; $j++)
				printf("<td style='border-width:0;'   valign=top align=center>%d\n", $j+1);
			$score = 0;
			for($ii=0; $ii < 8; $ii++)
			{
                                $score += $rev_row[$revForm[$ii]['tag']];
				printf("<tr><td style='border-width:0;'   valign=top><i>%s</i></td>\n", $revForm[$ii]['text']);
				for($j=0; $j< 5; $j++)
				{
		     			if($j == $rev_row[$revForm[$ii]['tag']] -1)
		       				printf("<td style='border-width:0;'   valign=top align=center width=6%%>X\n");
					else
		       				printf("<td style='border-width:0;'   valign=top align=center width=6%%>\n");
				}
		     
			}

			printf("<tr><td style='border-width:0;'  >&nbsp\n");
                        printf("<tr><td style='border-width:0;'   ><b>Average Score </b> <td style='border-width:0;'   colspan=5><b>%s </b></td>\n" , $score/8);
			printf("<tr><td style='border-width:0;'   ><b>Overall Recommendation  </b> <td style='border-width:0;'   colspan=5><b>%s </b></td>\n" , $rev_row['accept']);
			printf("<tr><td style='border-width:0;'  >&nbsp\n");

			printf("<tr><td style='border-width:0;'   colspan=6><b>Comments</b> ");
			printf("<tr><td style='border-width:0;'   colspan=6>%s </td>\n" , $rev_row['comments']);

		
			
		}
		
	}

    	
		
	printf("</table>\n");
   printf("</div>\n");
   printf("</div>\n");

	printf("</table>\n");


}

?>
