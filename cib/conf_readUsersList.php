<?php

function loadUsersList($usersList, $filename)
{
        xml = domxml_open_file($filename);

        $root = $xml->document_element();

        $nodes = $root->child_nodes();
	$numUsers = 0;
       	foreach($root->child_nodes() as $user)
	{
	     if($user->node_name() == "user") 
	     {
               foreach ($user->child_nodes() as $item)
  	           {
		       if($item->node_name() == "id") $usersList[ $numUsers ]['id']      		= $item->get_content();
		if($item->node_name() == "category")$usersList[ $numUsers ]['category'] 	= $item->get_content();
		if($item->node_name() == "firstname")$usersList[ $numUsers ]['firstname']  =$item->get_content(); 
		if($item->node_name() == "lastname")$usersList[ $numUsers ]['lastname']   =$item->get_content(); ;
		if($item->node_name() == "title")$usersList[ $numUsers ]['title']   		=$item->get_content(); 
		if($item->node_name() == "email")$usersList[ $numUsers ]['email']    	= $item->get_content();
		if($item->node_name() == "username")$usersList[ $numUsers ]['username'] =$item->get_content(); 
		if($item->node_name() == "password")$usersList[ $numUsers ]['password'] =$item->get_content(); 
		if($item->node_name() == "address")$usersList[ $numUsers ]['address']   	= $item->get_content();
		if($item->node_name() == "city")$usersList[ $numUsers ]['city']    		=$item->get_content(); 
		if($item->node_name() == "state")$usersList[ $numUsers ]['state'] 	= $item->get_content();
		if($item->node_name() == "country")$usersList[ $numUsers ]['country']    	= $item->get_content();
		if($item->node_name() == "zip")$usersList[ $numUsers ]['zip']    		=$item->get_content(); 
		if($item->node_name() == "phone")$usersList[ $numUsers ]['phone'] 	= $item->get_content();
		if($item->node_name() == "fax")$usersList[ $numUsers ]['fax'] 		= $item->get_content();
		if($item->node_name() == "organization")$usersList[ $numUsers ]['organization'] 	= $item->get_content();
                if($item->node_name() == "paid")$usersList[ $numUsers ]['paid'] 	= $item->get_content();


	           }	      
              $numUsers++;
	     }	
	}
	$usersList['numUsers'] = $numUsers;
	return($numUsers);		

}	
/*
function loadUsersList($usersList, $filename)
{
	$reader = simplexml_load_file($filename);
	
	$numUsers = 0;
	$numPapers = 0;
	foreach($reader->user as $user)
	{
		$usersList[ $numUsers ]['id']      	= $user->id;
		$usersList[ $numUsers ]['category'] 	= $user->category;
		$usersList[ $numUsers ]['firstname']   	= $user->firstname;
		$usersList[ $numUsers ]['lastname']    	= $user->lastname;
		$usersList[ $numUsers ]['title']   	= $user->title;
		$usersList[ $numUsers ]['email']    	= $user->email;
		$usersList[ $numUsers ]['username'] 	= $user->username;
		$usersList[ $numUsers ]['password'] 	= $user->password;
		$usersList[ $numUsers ]['address']   	= $user->address;
		$usersList[ $numUsers ]['city']    	= $user->city;
		$usersList[ $numUsers ]['state'] 	= $user->state;
		$usersList[ $numUsers ]['country']    	= $user->country;
		$usersList[ $numUsers ]['zip']    	= $user->zip;
		$usersList[ $numUsers ]['phone'] 	= $user->phone;
		$usersList[ $numUsers ]['fax'] 		= $user->fax;
		$usersList[ $numUsers ]['organization'] 	= $user->organization;


		foreach($user->paperID as $paperID){
			$usersList[ $numUsers ]['paperID'][$numPapers]= $paperID;
			$numPapers++;
		}
		$usersList[ $numUsers ]['numPapers'] = $numPapers;
		$numUsers++;
		$numPapers = 0;
	}
	$usersList['numUsers'] = $numUsers;
	return($numUsers);		

}	

*/

function isAuth($usersList, $email, $password)
{
	
	$numUsers = $usersList[ 'numUsers' ] ;
	for($i=0; $i < $numUsers; $i++){
		
		if ( !strcmp( $usersList[$i]['email'] , $email ) 
			&& !strcmp($usersList[$i]['password'] , $password ) ) return($i);
	}
	
	return(-1);  //not in the system
}

function isRegistered($usersList, $email)
{
	$numUsers = $usersList[ 'numUsers' ] ;
	for($i=0; $i< $numUsers; $i++){
		if ( !strcmp($usersList[$i]['email'], $email) )return($i);
	}
	
	return(-1); //not registered
}



function printUsersList($usersList)
{
	$numUsers = $usersList[ 'numUsers' ] ;
	for($i=0; $i <$numUsers;$i++){
		echo "<br>".$usersList[ $i ]['id'] ;
		echo "<br>".$usersList[ $i ]['category'] ;	
		echo "<br>".$usersList[ $i ]['firstname']  ; 	
		echo "<br>".$usersList[ $i ]['lastname'] ;   
		echo "<br>".$usersList[ $i ]['title'];   	
		echo "<br>".$usersList[ $i ]['email'] ;   	
		echo "<br>".$usersList[ $i ]['username'] ;	
		echo "<br>".$usersList[ $i ]['password'] ;	
		echo "<br>".$usersList[ $i ]['address'] ; 	
		echo "<br>".$usersList[ $i ]['city'] ;
		echo "<br>".$usersList[ $i ]['state'] ;	
		echo "<br>".$usersList[ $i ]['country']   ; 	
		echo "<br>".$usersList[ $i ]['zip'] ;
		echo "<br>".$usersList[ $i ]['phone'] ;	
		echo "<br>".$usersList[ $i ]['fax'] ;	
		echo "<br>".$usersList[ $i ]['organization'] ;
		echo "<br>";
	}		
}

function getUserNumById($usersList, $uid)
{
	$numUsers = $usersList[ 'numUsers' ] ;
	for($i=0; $i < $numUsers; $i++){	
		if ( strcmp($usersList[$i]['id'],$uid) == 0) return($i);
	}
	
	return(-1);  //not in the system
}


function addUser($usersList, $user, $filename)
{

	$numUsers = $usersList[ 'numUsers' ] ;

	//Pick a new uid
	$uid = $usersList[$numUsers -1]['id'] + 53;

	$usersList[ $numUsers ]['id']      	= $uid;
	$usersList[ $numUsers ]['category'] 	= $user['category'];
	$usersList[ $numUsers ]['firstname']   	= $user['firstname'];
	$usersList[ $numUsers ]['lastname']    	= $user['lastname'];
	$usersList[ $numUsers ]['title']   	= $user['title'];
	$usersList[ $numUsers ]['email']    	= $user['email'];
	$usersList[ $numUsers ]['username'] 	= $user['username'];
	$usersList[ $numUsers ]['password'] 	= $user['password'];
	$usersList[ $numUsers ]['address']   	= $user['address'];
	$usersList[ $numUsers ]['city']    	= $user['city'];
	$usersList[ $numUsers ]['state'] 	= $user['state'];
	$usersList[ $numUsers ]['country']    	= $user['country'];
	$usersList[ $numUsers ]['zip']    	= $user['zip'];
	$usersList[ $numUsers ]['phone'] 	= $user['phone'];
	$usersList[ $numUsers ]['fax'] 		= $user['fax'];
	$usersList[ $numUsers ]['organization'] 	= $user['organization'];

	$numUsers++;
	$usersList[ 'numUsers' ] = $numUsers;

	return $uid;
	
}

function saveUsersList($usersList, $filename)
{

	$numUsers = $usersList[ 'numUsers' ] ;

	$fp = fopen($filename,"w");

	$str = sprintf("<users>\n");
	fputs($fp, $str);
	for($i=0; $i< $numUsers; $i++){
		$str = sprintf("<user>\n");
		fputs($fp, $str);
		$str = sprintf("<id>%s</id>\n",$usersList[$i]['id']);
		fputs($fp, $str);
		$str = sprintf("<category>%s</category>\n",$usersList[$i]['category']);
		fputs($fp, $str);
		$str = sprintf("<title>%s</title>\n",$usersList[$i]['title']);
		fputs($fp, $str);
		$str = sprintf("<firstname>%s</firstname>\n",$usersList[$i]['firstname']);
		fputs($fp, $str);
		$str = sprintf("<lastname>%s</lastname>\n",$usersList[$i]['lastname']);
		fputs($fp, $str);
		$str = sprintf("<email>%s</email>\n",$usersList[$i]['email']);
		fputs($fp, $str);
		$str = sprintf("<username>%s</username>\n",$usersList[$i]['username']);
		fputs($fp, $str);
		$str = sprintf("<password>%s</password>\n",$usersList[$i]['password']);
		fputs($fp, $str);
		$str = sprintf("<organization>%s</organization>\n",$usersList[$i]['organization']);
		fputs($fp, $str);
		$str = sprintf("<address>%s</address>\n",$usersList[$i]['address']);
		fputs($fp, $str);
		$str = sprintf("<city>%s</city>\n",$usersList[$i]['city']);
		fputs($fp, $str);
		$str = sprintf("<state>%s</state>\n",$usersList[$i]['state']);
		fputs($fp, $str);
		$str = sprintf("<country>%s</country>\n",$usersList[$i]['country']);
		fputs($fp, $str);
		$str = sprintf("<zip>%s</zip>\n",$usersList[$i]['zip']);
		fputs($fp, $str);
		$str = sprintf("<phone>%s</phone>\n",$usersList[$i]['phone']);
		fputs($fp, $str);
		$str = sprintf("<fax>%s</fax>\n",$usersList[$i]['fax']);
		fputs($fp, $str);
                $str = sprintf("<paid>%s</paid>\n",$usersList[$i]['paid']);
		fputs($fp, $str);


		$str = sprintf("</user>\n");
		fputs($fp, $str);
	}
	$str = sprintf("</users>\n");
	fputs($fp, $str);

	fclose($fp);

	return;
}

?>