<?php
session_start();
	include("conn.php");
	include("conf_common.php");

 	$target_path = "papers/";

	$authorID 	= $_POST['id'];

  $userresult=mysqli_query($link, "SELECT count(*) as cnt from user where id=$authorID ");
	$user_row = mysqli_fetch_array( $userresult );

	$userNum = $user_row["cnt"];
	
if($_POST['track']=="-1" )
  {
    $message = "<strong>   ERROR: Uploading paper failed...please select Track!!</strong>";
		printSubmitNewPaperPage($_POST['title'], $_POST['author'] , $_POST['abstract'] , $authorID, $message, "delete.png");
  }

if(trim($_POST['title'])=="" && trim($_POST['author'])=="" && trim($_POST['abstract'])=="" )
  {
    $message = "<strong>   ERROR: Uploading paper failed...please insert title, authors, and abstract!!</strong>";
		printSubmitNewPaperPage($_POST['title'], $_POST['author'] , $_POST['abstract'] , $authorID, $message, "delete.png");
  }


 $ftype = end(explode('.', strtolower($_FILES['uploadedfile']['name'])));
 if( $ftype !="pdf")
 {
    $message = "<strong>   ERROR: Uploading paper failed...please upload pdf file!!</strong>";
		printSubmitNewPaperPage($_POST['title'], $_POST['author'] , $_POST['abstract'] , $authorID, $message, "delete.png");
 }

	if($userNum > 0)
	{
			//Pick a new paper id
			$pid = date("YmdHis");
		
			//upload file
			$baseFilename = cleanStr($authorID."_".$pid."_".basename( $_FILES['uploadedfile']['name'])); 
			$fullFilename = $target_path.$baseFilename;
			if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $fullFilename)) 
			{		     
				chmod($fullFilename,0644);
				$title= mysqli_real_escape_string($link, $_POST["title"]);
				$author= mysqli_real_escape_string($link, $_POST["authors"]);
				$abstract= mysqli_real_escape_string($link, $_POST["abstract"]);

				mysqli_query($link, "insert into paper(id,title,author,authorID,abstract,status,attachment,attachmentwithoutauthor,track) values($pid,'$title','$author',$authorID,'$abstract','Not reviewed','$baseFilename','$baseFilename','$_POST[track]') ");
				$message = sprintf("<i>   Paper submitted successfully..<br> New Paper ID = %d </i>", $pid);
				//printWelcomePage($usersList, $paperList, $userNum, $message, "check1.png");
				
	$to = "review@cibec2012.org";
	$subject="New CIBEC 2012 Paper Submission";
	$message="please check the new paper with id = ".$pid." and title = ".$_POST['title'];
	$message.="<br>\nBest Regards";
	
     	$headers = "From: cibec2012@cibec2012.org\r\n";
	$headers .= "Reply-To: cibec2012@cibec2012.org\r\n";
	$headers .= "Return-Path: cibec2012@cibec2012.org\r\n";
	
      	$headers .= "Content-type: text/html\r\n"; 

        
         
        if (mail($to,$subject,$message,$headers) ) {
	   echo "email sent";
	} else {
	   echo "email could not be sent";
	}
		

				//redirect to prevent resubmission of form in case of user hitting backspace
				$str = sprintf("Location: conf_showPaperInfo.php?id=%s&pid=%s&np=true", $authorID, $pid);
				header($str);
			}
			else 
			{
				$message = "<strong>   ERROR: Uploading paper failed...please check file name and size!!</strong>";
				printSubmitNewPaperPage($_POST['title'], $_POST['author'] , $_POST['abstract'] , $authorID, $message, "delete.png");
			}	

   }
	else 
	{
		$message = "   User not found..Please try again..";
		printLoginPage($message,"delete.png" );
	}







?>