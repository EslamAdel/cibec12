﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"[]>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>CIBEC 2012</title>



    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" href="style.ie6.css" type="text/css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" href="style.ie7.css" type="text/css" media="screen" /><![endif]-->

    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="script.js"></script>
<script type='text/javascript'>

$(document).ready(function() {
$("#trackspan").hide();
$("#category").change(function() {
 if ($(this).val()!="rev"){
  $("#trackspan").hide();
 }else{
  $("#trackspan").show();
 }
});

$('#register').submit(function() {
if ($("#category").val()=="rev" && $("#track").val()==""){
  alert("Please select the track");
  return false;
 }
});

});


function formValidator(){
	// Make quick references to our fields

	var organization = document.getElementById('organization');
  var country = document.getElementById('country');
	var firstname = document.getElementById('firstname');
	var lastname = document.getElementById('lastname');
	var addr = document.getElementById('addr');
	var password = document.getElementById('password');
	var email = document.getElementById('email');

	// Check each input in the order that it appears in the form!
if(isAlphabet(organization, "Please enter your organization")){
  if(isAlphabet(country, "Please enter your country")){
	if(isAlphabet(firstname, "Please enter your first name")){
		if(isAlphabet(lastname, "Please enter your last name")){
			if(isAlphanumeric(addr, "Numbers and Letters Only for Address")){
				if(emailValidator(email, "Please enter a valid email address")){
					if(lengthRestriction(password, 5, 20)){
							return true;
						}
				}
			}
		}
	}
	}
}

	return false;

}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-z A-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9 a-zA-Z]+$/;
         return true;  //quick fix
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Invalid password: Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "-- Please Select (only US / Can / Aus)"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = ^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script>

</head>
<body>
<div id="cibec-main">
    <div class="cleared reset-box"></div>
    <div class="cibec-header">
        <div class="cibec-header-position">
            <div class="cibec-header-wrapper">
                <div class="cleared reset-box"></div>
                <div class="cibec-header-inner">
                <div class="cibec-logo">
                                                </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="cleared reset-box"></div>
    <div class="cibec-box cibec-sheet">
        <div class="cibec-box-body cibec-sheet-body">
            <div class="cibec-layout-wrapper">
                <div class="cibec-content-layout">
                    <div class="cibec-content-layout-row">
                        <div class="cibec-layout-cell cibec-sidebar1">
<div class="cibec-box cibec-vmenublock">
    <div class="cibec-box-body cibec-vmenublock-body">
                <div class="cibec-bar cibec-vmenublockheader">
                    <h3 class="t">Navigation</h3>
                </div>
                <div class="cibec-box cibec-vmenublockcontent">
                    <div class="cibec-box-body cibec-vmenublockcontent-body">
<script>
$('.cibec-vmenublockcontent-body').load('menu.php?m=0');
</script>
                
                                		<div class="cleared"></div>
                    </div>
                </div>
		<div class="cleared"></div>
    </div>
</div>
<div class="cibec-box cibec-block">
    <div class="cibec-box-body cibec-block-body">
                <div class="cibec-bar cibec-blockheader">
                    <h3 class="t">Sponsers</h3>
                </div>
                <div class="cibec-box cibec-blockcontent">
                    <div class="cibec-box-body cibec-blockcontent-body sponsers">
 <script>
$('.sponsers').load('sponsers.php');
</script>

                                		<div class="cleared"></div>
                    </div>
                </div>
		<div class="cleared"></div>
    </div>
</div>

                          <div class="cleared"></div>
                        </div>
                        <div class="cibec-layout-cell cibec-content">
<div class="cibec-box cibec-post">
    <div class="cibec-box-body cibec-post-body">
<div class="cibec-post-inner cibec-article">
                                <h2 class="cibec-postheader">Create an Account
                                </h2>
                                                <div class="cibec-postcontent">

<form action="conf_register.php" method="post" onsubmit="return (formValidator());"  name="register" id="register">

<table align="center" width="100%">

<tr><td style="border-width:0;text-align:right;" >User's Category</td>
<td style="border-width:0;" colspan="2">
<select name="category" id="category">
<option value="author">Author</option>
<option value="attendee">Attendee</option>
<option value="rev">Reviewer</option>
</select>

<?php
session_start();
include("conn.php");
 $tracksresult=mysqli_query($link, "SELECT name from track order by name ");
 printf("<span id=\"trackspan\">Reviewer Track");
	printf("<select name=\"track\" id=\"track\">\n");
  printf("<option value=''>Select Track</option>\n");
  while ( $a_row = mysqli_fetch_array( $tracksresult ) ) {
  printf("<option>%s</option>\n",$a_row["name"]);
    }
  printf("</select></span>\n");
 ?> 
 </td>
  </tr>

<tr>
<td style="border-width:0;text-align:right;">
Organization / Company *
</td><td  style="border-width:0;" colspan="2">
<input type="text" name="organization" id="organization" value="" size="50" maxlength="255">
</td></tr>

<tr>
<td style="border-width:0;text-align:right;">
Title
</td><td style="border-width:0;" colspan="2">
<input type="text" name="title" value="" size="20" maxlength="255">
</td>
</tr>

<tr>
<td style="border-width:0;text-align:right;">
First Name *
</td><td style="border-width:0;" colspan="2">
<input type="text" name="firstname"  id=firstname value="" size="50" maxlength="255">
</td>
</tr>

<tr>
<td style="border-width:0;text-align:right;">
Last Name *
</td><td  style="border-width:0;" colspan="2">
<input type="text" name="lastname" id=lastname value="" size="50" maxlength="255">
</td>
</tr>


<tr><td style="border-width:0;" colspan="3"></td>
</tr>

<tr>
<td style="border-width:0;text-align:right;"">
Address
</td><td  style="border-width:0;" colspan="2">
<input type="text" name="address"  id=addr value="" size="50" maxlength="255">
</td>
</tr>

<tr>
<td style="border-width:0;text-align:right;">
Postcode / ZIP Code
</td><td  style="border-width:0;" colspan="2">
<input type="text" name="zip"  id= zip value="" size="10" maxlength="10">
</td>
</tr>

<tr>
<td style="border-width:0;text-align:right;">
City
</td><td  style="border-width:0;" colspan="2">
<input type="text" name="city" value="" size="50" maxlength="255">
</td>
</tr>

<tr><td style="border-width:0;text-align:right;">
State / Territory / Province
</td><td style="border-width:0;" colspan="2">
<select name="state" id=state size="1">
<option value="-">-- Please Select (only US / Can / Aus) </option>
<option value="-">- US States --------------------------------- </option>
<option value="US,AL">Alabama </option>
<option value="US,AK">Alaska </option>
<option value="US,AZ">Arizona </option>
<option value="US,AR">Arkansas </option>
<option value="US,CA">California </option>
<option value="US,CO">Colorado </option>
<option value="US,CT">Connecticut </option>
<option value="US,DE">Delaware </option>
<option value="US,DC">District of Columbia </option>
<option value="US,FL">Florida </option>
<option value="US,GA">Georgia </option>
<option value="US,HI">Hawaii </option>
<option value="US,ID">Idaho </option>
<option value="US,IL">Illinois </option>
<option value="US,IN">Indiana </option>
<option value="US,IA">Iowa </option>
<option value="US,KS">Kansas </option>
<option value="US,KY">Kentucky </option>
<option value="US,LA">Louisiana </option>
<option value="US,ME">Maine </option>
<option value="US,MD">Maryland </option>
<option value="US,MA">Massachusetts </option>
<option value="US,MI">Michigan </option>
<option value="US,MN">Minnesota </option>
<option value="US,MS">Mississippi </option>
<option value="US,MO">Missouri </option>
<option value="US,MT">Montana </option>
<option value="US,NE">Nebraska </option>
<option value="US,NV">Nevada </option>
<option value="US,NH">New Hampshire </option>
<option value="US,NJ">New Jersey </option>
<option value="US,NM">New Mexico </option>
<option value="US,NY">New York </option>
<option value="US,NC">North Carolina </option>
<option value="US,ND">North Dakota </option>
<option value="US,OH">Ohio </option>
<option value="US,OK">Oklahoma </option>
<option value="US,OR">Oregon </option>
<option value="US,PA">Pennsylvania </option>
<option value="US,RI">Rhode Island </option>
<option value="US,SC">South Carolina </option>
<option value="US,SD">South Dakota </option>
<option value="US,TN">Tennessee </option>
<option value="US,TX">Texas </option>
<option value="US,UT">Utah </option>
<option value="US,VT">Vermont </option>
<option value="US,VA">Virginia </option>
<option value="US,WA">Washington </option>
<option value="US,WV">West Virginia </option>
<option value="US,WI">Wisconsin </option>
<option value="US,WY">Wyoming </option>
<option value="-">- Canadian Provinces ------------------------ </option>
<option value="CA,AB">Alberta </option>
<option value="CA,BC">British Columbia </option>
<option value="CA,MB">Manitoba </option>
<option value="CA,NB">New Brunswick </option>
<option value="CA,NL">Newfoundland and Labrador </option>
<option value="CA,NS">Nova Scotia </option>
<option value="CA,NU">Nunavut </option>
<option value="CA,ON">Ontario </option>
<option value="CA,PE">Prince Edward Island </option>
<option value="CA,QC">Qu&amp;eacute;bec </option>
<option value="CA,SK">Saskatchewan </option>
<option value="CA,NT">Northwest Territories </option>
<option value="CA,YT">Yukon Territory </option>
<option value="-">- Australian States / Territories ----------- </option>
<option value="AU,ACT">Australian Capital Territory </option>
<option value="AU,NSW">New South Wales </option>
<option value="AU,NT">Northern Territory </option>
<option value="AU,QLD">Queensland </option>
<option value="AU,SA">South Australia </option>
<option value="AU,TAS">Tasmania </option>
<option value="AU,VIC">Victoria </option>
<option value="AU,WA">Western Australia </option>
</select>
</td>
</tr>

<tr><td style="border-width:0;text-align:right;">
Country *
</td><td style="border-width:0;" colspan="2">
<select name="country" id="country" size="1">
<option value="-" selected>-- Please Select Country </option>
<option value="-">----------------------------------------- </option>
<option value="AF">Afghanistan </option>
<option value="AL">Albania </option>
<option value="DZ">Algeria </option>
<option value="AS">American Samoa </option>
<option value="AD">Andorra </option>
<option value="AO">Angola </option>
<option value="AI">Anguilla </option>
<option value="AQ">Antarctica </option>
<option value="AG">Antigua and Barbuda </option>
<option value="AR">Argentina </option>
<option value="AM">Armenia </option>
<option value="AW">Aruba </option>
<option value="AU">Australia </option>
<option value="AT">Austria </option>
<option value="AZ">Azerbaijan </option>
<option value="BS">Bahamas, The </option>
<option value="BH">Bahrain </option>
<option value="BD">Bangladesh </option>
<option value="BB">Barbados </option>
<option value="BY">Belarus </option>
<option value="BE">Belgium </option>
<option value="BZ">Belize </option>
<option value="BJ">Benin </option>
<option value="BM">Bermuda </option>
<option value="BT">Bhutan </option>
<option value="BO">Bolivia </option>
<option value="BA">Bosnia and Herzegovina </option>
<option value="BW">Botswana </option>
<option value="BV">Bouvet Island </option>
<option value="BR">Brazil </option>
<option value="IO">British Indian Ocean Territory </option>
<option value="BN">Brunei Darussalam </option>
<option value="BG">Bulgaria </option>
<option value="BF">Burkina Faso </option>
<option value="BI">Burundi </option>
<option value="KH">Cambodia </option>
<option value="CM">Cameroon </option>
<option value="CA">Canada </option>
<option value="CV">Cape Verde </option>
<option value="KY">Cayman Islands </option>
<option value="CF">Central African Republic </option>
<option value="TD">Chad </option>
<option value="CL">Chile </option>
<option value="CN">China, Peoples Republic of </option>
<option value="CX">Christmas Island </option>
<option value="CC">Cocos (Keeling) Islands </option>
<option value="CO">Colombia </option>
<option value="KM">Comoros </option>
<option value="CD">Congo, Democratic Republic of the </option>
<option value="CG">Congo </option>
<option value="CK">Cook Islands </option>
<option value="CR">Costa Rica </option>
<option value="HR">Croatia (Hrvatska) </option>
<option value="CU">Cuba </option>
<option value="CY">Cyprus </option>
<option value="CZ">Czech Republic </option>
<option value="DK">Denmark </option>
<option value="DJ">Djibouti </option>
<option value="DM">Dominica </option>
<option value="DO">Dominican Republic </option>
<option value="EC">Ecuador </option>
<option value="EG">Egypt </option>
<option value="SV">El Salvador </option>
<option value="GQ">Equatorial Guinea </option>
<option value="ER">Eritrea </option>
<option value="EE">Estonia </option>
<option value="ET">Ethiopia </option>
<option value="FK">Falkland Islands (Islas Malvinas) </option>
<option value="FO">Faroe Islands </option>
<option value="FJ">Fiji Islands </option>
<option value="FI">Finland </option>
<option value="FR">France </option>
<option value="GF">French Guiana </option>
<option value="PF">French Polynesia </option>
<option value="TF">French Southern Territories </option>
<option value="GA">Gabon </option>
<option value="GM">Gambia, The </option>
<option value="GE">Georgia </option>
<option value="DE">Germany </option>
<option value="GH">Ghana </option>
<option value="GI">Gibraltar </option>
<option value="GR">Greece </option>
<option value="GL">Greenland </option>
<option value="GD">Grenada </option>
<option value="GP">Guadaloupe </option>
<option value="GU">Guam </option>
<option value="GT">Guatemala </option>
<option value="GW">Guinea-Bissau </option>
<option value="GN">Guinea </option>
<option value="GY">Guyana </option>
<option value="HT">Haiti </option>
<option value="HM">Heard Island and McDonald Islands </option>
<option value="HN">Honduras </option>
<option value="HK">Hong Kong S.A.R. - China </option>
<option value="HU">Hungary </option>
<option value="IS">Iceland </option>
<option value="IN">India </option>
<option value="ID">Indonesia </option>
<option value="IR">Iran (Islamic Republic of) </option>
<option value="IQ">Iraq </option>
<option value="IE">Ireland (Republic of) </option>
<option value="IL">Israel </option>
<option value="IT">Italy </option>
<option value="CI">Ivory Coast </option>
<option value="JM">Jamaica </option>
<option value="JP">Japan </option>
<option value="JO">Jordan </option>
<option value="KZ">Kazakhstan </option>
<option value="KE">Kenya </option>
<option value="KI">Kiribati </option>
<option value="KP">Korea, North (Democratic People&amp;#39;s Republic of) </option>
<option value="KR">Korea, South (Republic of) </option>
<option value="KS">Kosovo </option>
<option value="KW">Kuwait </option>
<option value="KG">Kyrgyzstan </option>
<option value="LA">Laos (People&amp;#39;s Democratic Republic) </option>
<option value="LV">Latvia </option>
<option value="LB">Lebanon </option>
<option value="LS">Lesotho </option>
<option value="LR">Liberia </option>
<option value="LY">Libya Arab Jamahiriya </option>
<option value="LI">Liechtenstein </option>
<option value="LT">Lithuania </option>
<option value="LU">Luxembourg </option>
<option value="MO">Macau S.A.R. - China </option>
<option value="MK">Macedonia, Republic of </option>
<option value="MG">Madagascar </option>
<option value="MW">Malawi </option>
<option value="MY">Malaysia </option>
<option value="MV">Maldives </option>
<option value="ML">Mali </option>
<option value="MT">Malta </option>
<option value="MH">Marshall Islands </option>
<option value="MQ">Martinique </option>
<option value="MR">Mauritania </option>
<option value="MU">Mauritius </option>
<option value="YT">Mayotte </option>
<option value="MX">Mexico </option>
<option value="FM">Micronesia (Federated States of) </option>
<option value="MD">Moldova (Republic of) </option>
<option value="MC">Monaco </option>
<option value="MN">Mongolia </option>
<option value="ME">Montenegro, Republic of </option>
<option value="MS">Montserrat </option>
<option value="MA">Morocco </option>
<option value="MZ">Mozambique </option>
<option value="MM">Myanmar </option>
<option value="NA">Namibia </option>
<option value="NR">Nauru </option>
<option value="NP">Nepal </option>
<option value="AN">Netherlands Antilles </option>
<option value="NL">Netherlands, The </option>
<option value="NC">New Caledonia </option>
<option value="NZ">New Zealand </option>
<option value="NI">Nicaragua </option>
<option value="NG">Nigeria </option>
<option value="NE">Niger </option>
<option value="NU">Niue </option>
<option value="NF">Norfolk Island </option>
<option value="MP">Northern Mariana Islands </option>
<option value="NO">Norway </option>
<option value="OM">Oman </option>
<option value="PK">Pakistan </option>
<option value="PW">Palau </option>
<option value="PS">Palestinian Territories </option>
<option value="PA">Panama </option>
<option value="PG">Papua New Guinea </option>
<option value="PY">Paraguay </option>
<option value="PE">Peru </option>
<option value="PH">Philippines </option>
<option value="PN">Pitcairn Island </option>
<option value="PL">Poland </option>
<option value="PT">Portugal </option>
<option value="PR">Puerto Rico </option>
<option value="QA">Qatar </option>
<option value="RE">R&amp;eacute;union </option>
<option value="RO">Romania </option>
<option value="RU">Russian Federation </option>
<option value="RW">Rwanda </option>
<option value="ST">S&amp;atilde;o Tom&amp;eacute; and Pr&amp;iacute;ncipe </option>
<option value="SH">Saint Helena </option>
<option value="KN">Saint Kitts and Nevis </option>
<option value="LC">Saint Lucia </option>
<option value="PM">Saint Pierre and Miquelon </option>
<option value="VC">Saint Vincent and the Grenadines </option>
<option value="WS">Samoa </option>
<option value="SM">San Marino </option>
<option value="SA">Saudi Arabia </option>
<option value="SN">Senegal </option>
<option value="RS">Serbia, Republic of </option>
<option value="SC">Seychelles </option>
<option value="SL">Sierra Leone </option>
<option value="SG">Singapore </option>
<option value="SK">Slovakia </option>
<option value="SI">Slovenia </option>
<option value="SB">Solomon Islands </option>
<option value="SO">Somalia </option>
<option value="ZA">South Africa </option>
<option value="GS">South Georgia and the South Sandwich Islands </option>
<option value="ES">Spain </option>
<option value="LK">Sri Lanka </option>
<option value="SD">Sudan </option>
<option value="SR">Suriname </option>
<option value="SJ">Svalbard and Jan Mayen </option>
<option value="SZ">Swaziland </option>
<option value="SE">Sweden </option>
<option value="CH">Switzerland </option>
<option value="SY">Syria </option>
<option value="TW">Taiwan </option>
<option value="TJ">Tajikistan </option>
<option value="TZ">Tanzania </option>
<option value="TH">Thailand </option>
<option value="TL">Timor-Leste </option>
<option value="TG">Togo </option>
<option value="TK">Tokelau </option>
<option value="TO">Tonga </option>
<option value="TT">Trinidad and Tobago </option>
<option value="TN">Tunisia </option>
<option value="TR">Turkey </option>
<option value="TM">Turkmenistan </option>
<option value="TC">Turks and Caicos Islands </option>
<option value="TV">Tuvalu </option>
<option value="UG">Uganda </option>
<option value="UA">Ukraine </option>
<option value="AE">United Arab Emirates </option>
<option value="UK">United Kingdom </option>
<option value="UM">United States Minor and Outlying Islands </option>
<option value="US">United States of America </option>
<option value="UY">Uruguay </option>
<option value="UZ">Uzbekistan </option>
<option value="VU">Vanuatu </option>
<option value="VA">Vatican City State (Holy See) </option>
<option value="VE">Venezuela </option>
<option value="VN">Viet Nam </option>
<option value="VG">Virgin Islands, British </option>
<option value="VI">Virgin Islands, U.S. </option>
<option value="WF">Wallis and Fortuna Islands </option>
<option value="EH">Western Sahara </option>
<option value="YE">Yemen </option>
<option value="ZM">Zambia </option>
<option value="ZW">Zimbabwe </option>
</select>
</td>
</tr>

<tr><td style="border-width:0;" colspan="3" ></td></tr>
<tr  >
<td style="border-width:0;text-align:right;">
Phone
</td><td  style="border-width:0;" colspan="2">
<input type="text" name="phone" value="" size="50" maxlength="255">
</td>
</tr>

<tr>
<td  style="border-width:0;text-align:right;">
Fax
</td><td  style="border-width:0;" colspan="2">
<input type="text" name="fax" value="" size="50" maxlength="255">
</td>
</tr>

<tr>
<td  style="border-width:0;text-align:right;">
Homepage URL
</td><td  style="border-width:0;" colspan="2">
<input type="text" name="url" value="" size="50" maxlength="255">
</td></tr>

<br>
<tr><td style="border-width:0;" colspan="3">
<hr>
</td></tr>


<tr>
<td style="border-width:0;text-align:right;">
E-mail *
</td><td  style="border-width:0;" colspan="2">
<input type="text" name="email" id=email value="" size="50" maxlength="255">
</td>
</tr>

<tr><td style="border-width:0;text-align:right;">
Password *
</td><td style="border-width:0;" colspan="2">
<input type="password" name="password" id="password" value="" size="20" maxlength="255" autocomplete="off">
<br><span class="form_hint">Passwords must have at least five (5) characters. They should include at least one letter (a-z) and one number (0-9). </span>
</td></tr>

<tr><td style="border-width:0;" colspan="2">
<input class='button' type='submit' name="cmd_save_person" value="Register" >
<br><br>* Indicates required field.</td>
</tr>

<!--
<tr><td style="border-width:0;" colspan="3">
Having problems with the registration page?
<br>Please contact <a href="">Dr. </a> at @gmail.com with your questions.
</tr>
-->
</table>


</form>

                </div>
                <div class="cleared"></div>
                </div>

		<div class="cleared"></div>
    </div>
</div>

                          <div class="cleared"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cleared"></div>
            <div class="cibec-footer">
                <div class="cibec-footer-body">
                            <div class="cibec-footer-text">
                                <p>Copyright © 2012, CIBEC. All Rights Reserved.</p>
                                                            </div>
                    <div class="cleared"></div>
                </div>
            </div>
    		<div class="cleared"></div>
        </div>
    </div>
    <div class="cleared"></div>
    <p class="cibec-page-footer"></p>
    <div class="cleared"></div>
</div>

</body>
</html>