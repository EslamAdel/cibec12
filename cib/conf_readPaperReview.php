<?php


function loadReviewList($reviewList, $filename)
{
	if(!$xml = domxml_open_file($filename))
        {
          $reviewList['numReviews'] = 0;
          return 0;
         }
        	$root = $xml->document_element();

        	$nodes = $root->child_nodes();
	$numReviews = 0;
	foreach($nodes as $review)
	{
	  if($review->node_name() == "review") 
	   {
             		foreach ($review->child_nodes() as $item)
  	           	{
              			if($item->node_name() == "pid")	$reviewList[ $numReviews ]['pid']      	= $item->get_content();
	      		if($item->node_name() == "rev")	$reviewList[ $numReviews ]['rev']  	= $item->get_content();	      
             			if($item->node_name() == "comments")	$reviewList[ $numReviews ]['comments']	= $item->get_content();      
              			if($item->node_name() == "accept")	$reviewList[ $numReviews ]['accept'] 	= $item->get_content();	      
             			if($item->node_name() == "relevance")	$reviewList[ $numReviews ]['relevance'] 	= $item->get_content();      
              			if($item->node_name() == "language")	$reviewList[ $numReviews ]['language']	= $item->get_content();
	      		if($item->node_name() == "clarity")	$reviewList[ $numReviews ]['clarity']	= $item->get_content();
                        if($item->node_name() == "merit")	$reviewList[ $numReviews ]['merit']	= $item->get_content();
			if($item->node_name() == "verification")	$reviewList[ $numReviews ]['verification']	= $item->get_content();
            			if($item->node_name() == "analysis")	$reviewList[ $numReviews ]['analysis']	= $item->get_content();
             			if($item->node_name() == "literature")	$reviewList[ $numReviews ]['literature']	= $item->get_content();
             			if($item->node_name() == "application")	$reviewList[ $numReviews ]['application']	= $item->get_content();
	                        if($item->node_name() == "date")	$reviewList[ $numReviews ]['date']	= $item->get_content();
             

               		}
		$numReviews++;
           	  }
	}
	$reviewList['numReviews'] = $numReviews;
	return($numReviews);		
}




function loadReviewList1($reviewList, $filename)
{

	$reader = simplexml_load_file($filename);
	
	$numReviews = 0;
	foreach($reader->review as $review)
	{
		$reviewList[ $numReviews ]['rev']      	= $review->rev;
		$reviewList[ $numReviews ]['pid']   	= $review->pid;
		$reviewList[ $numReviews ]['comments']    = $review->comments;
		$reviewList[ $numReviews ]['accept']   	= $review->accept;
		$reviewList[ $numReviews ]['relevance']    = $review->relevance;
		$reviewList[ $numReviews ]['language'] 	= $review->language;
		$reviewList[ $numReviews ]['clarity'] 	= $review->clarity;
		$reviewList[ $numReviews ]['novelity'] 	= $review->novelity;
		$reviewList[ $numReviews ]['date'] 	= $review->date;
		
		
		
		$numReviews++;

	}
	$reviewList['numReviews'] = $numReviews;
	return($numReviews);		
}

function getReviewNumById($reviewList, $revID)
{
	$numReviews = $reviewList[ 'numReviews' ] ;
	for($i=0; $i < $numReviews; $i++){	
		if ( strcmp($reviewList[$i]['rev'],$revID) == 0) return($i);
	}
	
	return(-1);  //not in the system
}


function isReviewCompleted($reviewList, $revID)
{
	$numReviews = $reviewList[ 'numReviews' ] ;
	for($i=0; $i < $numReviews; $i++){	
		if ( strcmp($reviewList[$i]['rev'],$revID) == 0) return(1);
	}
	
	return(0);  //not in the list
}




function printReviewList($reviewList)
{
	$numReviews = $reviewList[ 'numReviews' ] ;
	for($i=0; $i <$numReviews;$i++){
		echo "<br>reviewID       = ".$reviewList[ $i ]['rev'];
		echo "<br>paper ID       = ".$reviewList[ $i ]['pid'];
		echo "<br>Comments   = ".$reviewList[ $i ]['comments'];
		echo "<br>Relevance    = ".$reviewList[ $i ]['relevance'];
		echo "<br>Language     = ".$reviewList[ $i ]['language'];
		echo "<br>Clarity           = ".$reviewList[ $i ]['clarity'];
		echo "<br>Accept          = ".$reviewList[ $i ]['accept'];
		
		
		echo "<br>";
	}		
}


function addReview($reviewList, $review)
{

	$numReviews = $reviewList[ 'numReviews' ] ;

	
	$reviewList[ $numReviews ]['pid']      	= $review['pid'];
	$reviewList[ $numReviews ]['rev']   	= $review['rev'];
	$reviewList[ $numReviews ]['comments'] 	= $review['comments'];
	$reviewList[ $numReviews ]['clarity'] 	= $review['clarity'];
	$reviewList[ $numReviews ]['language']   	= $review['language'];
	$reviewList[ $numReviews ]['relevance'] 	= $review['relevance'];
	$reviewList[ $numReviews ]['accept'] 	= $review['accept'];
	$reviewList[ $numReviews ]['date'] 	= $review['date'];
        $reviewList[ $numReviews ]['merit']	= $review['merit'];
	$reviewList[ $numReviews ]['verification']= $review['verification'];
        $reviewList[ $numReviews ]['analysis']	= $review['analysis'];
        $reviewList[ $numReviews ]['literature']= $review['literature'];
        $reviewList[ $numReviews ]['application']= $review['application'];
	
	
	
	$numReviews++;
	$reviewList[ 'numReviews' ] = $numReviews;

	
	
}



function saveReviewList($reviewList, $filename)
{
	$fp =fopen($filename,"w");
	
	$str = sprintf("<reviews>\n");
	fputs($fp, $str);
	$numReviews = $reviewList[ 'numReviews' ] ;
	for($i=0; $i <$numReviews;$i++){
		
		$str = sprintf("<review>\n");
		fputs($fp, $str);
		$str = sprintf("<date>%s</date>\n", 		$reviewList[ $i ]['date']);
		fputs($fp, $str);
		$str = sprintf("<pid>%s</pid>\n",       		$reviewList[ $i ]['pid']);
		fputs($fp, $str);
		$str = sprintf("<rev>%s</rev>\n", 		$reviewList[ $i ]['rev']);
		fputs($fp, $str);
		$str = sprintf("<merit>%s</merit>\n", 		$reviewList[ $i ]['merit']);
		fputs($fp, $str);
		$str = sprintf("<relevance>%s</relevance>\n",   	$reviewList[ $i ]['relevance']);
		fputs($fp, $str);
		$str = sprintf("<clarity>%s</clarity>\n", 		$reviewList[ $i ]['clarity']);
		fputs($fp, $str);
		$str = sprintf("<language>%s</language>\n", 	$reviewList[ $i ]['language']);
		fputs($fp, $str);
		$str = sprintf("<verification>%s</verification>\n", 	$reviewList[ $i ]['verification']);
		fputs($fp, $str);
		$str = sprintf("<analysis>%s</analysis>\n", 	$reviewList[ $i ]['analysis']);
		fputs($fp, $str);
		$str = sprintf("<literature>%s</literature>\n", 	$reviewList[ $i ]['literature']);
		fputs($fp, $str);
		$str = sprintf("<application>%s</application>\n", 	$reviewList[ $i ]['application']);
		fputs($fp, $str);		
		$str = sprintf("<comments>%s</comments>\n", 	$reviewList[ $i ]['comments']);
		fputs($fp, $str);
		$str = sprintf("<accept>%s</accept>\n", 		$reviewList[ $i ]['accept']);
		fputs($fp, $str);

		$str = sprintf("</review>\n");
		fputs($fp, $str);
	}	
	$str = sprintf("</reviews>\n");	
	fputs($fp, $str);
	fclose($fp);
}



/*
function saveReviewList($reviewList, $filename)
{
	$fp =fopen($filename,"w");
	
	fprintf($fp,"<reviews>\n");
	$numReviews = $reviewList[ 'numReviews' ] ;
	for($i=0; $i <$numReviews;$i++){
		
		fprintf($fp,"<review>\n");
		fprintf($fp,"<pid>%s</pid>\n",       	$reviewList[ $i ]['pid']);
		fprintf($fp,"<rev>%s</rev>\n", 	$reviewList[ $i ]['rev']);
		fprintf($fp,"<relevance>%s</relevance>\n",   $reviewList[ $i ]['relevance']);
		fprintf($fp,"<clarity>%s</clarity>\n", $reviewList[ $i ]['clarity']);
		fprintf($fp,"<language>%s</language>\n", $reviewList[ $i ]['language']);
		fprintf($fp,"<comments>%s</comments>\n", $reviewList[ $i ]['comments']);
		fprintf($fp,"<accept>%s</accept>\n", $reviewList[ $i ]['accept']);
		
		
		fprintf($fp,"</review>\n");
	}	
	fprintf($fp,"</reviews>\n");	
	fclose($fp);
}
*/





?>