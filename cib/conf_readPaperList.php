<?php

function loadPaperList($paperList, $filename)
{
        $xml = domxml_open_file($filename);
        $root = $xml->document_element();

        $nodes = $root->child_nodes();
	$numPapers = 0;
	$numReviewers = 0;
	foreach($nodes as $paper)
	{
          if($paper->node_name() == "paper") 
	   {
             foreach ($paper->child_nodes() as $item)
  	           {
              if($item->node_name() == "id")$paperList[ $numPapers ]['id']      = $item->get_content();
	      if($item->node_name() == "author")$paperList[ $numPapers ]['author']  = $item->get_content();	      
              if($item->node_name() == "abstract")$paperList[ $numPapers ]['abstract']= 	$item->get_content();      
              if($item->node_name() == "title")$paperList[ $numPapers ]['title'] = $item->get_content();	      
              if($item->node_name() == "status")$paperList[ $numPapers ]['status'] = 	$item->get_content();      
              if($item->node_name() == "authorID")$paperList[ $numPapers ]['authorID']= $item->get_content();
	      if($item->node_name() == "attachment")$paperList[ $numPapers ]['attachment']= $item->get_content();
              if($item->node_name() == "rev1")$paperList[ $numPapers ]['rev1']= $item->get_content();
              if($item->node_name() == "rev2")$paperList[ $numPapers ]['rev2']= $item->get_content();
              if($item->node_name() == "rev3")$paperList[ $numPapers ]['rev3']= $item->get_content();
              if($item->node_name() == "camera")$paperList[ $numPapers ]['camera']= $item->get_content();
               if($item->node_name() == "paid")$paperList[ $numPapers ]['paid']= $item->get_content();



               }
		$numPapers++;
           }
	}
	$paperList['numPapers'] = $numPapers;
	return($numPapers);		
}



function printPaperList($paperList)
{
	$numPapers = $paperList[ 'numPapers' ] ;
	for($i=0; $i <$numPapers;$i++){
		echo "<br>PaperID       = ".$paperList[ $i ]['id'];
		echo "<br>Title         = ".$paperList[ $i ]['title'];
		echo "<br>Author        = ".$paperList[ $i ]['author'];
		echo "<br>abstract      = ".$paperList[ $i ]['abstract'];
		echo "<br>attachment    = ".$paperList[ $i ]['attachment'];
		echo "<br>Status        = ".$paperList[ $i ]['status'];
                echo "<br>Rev1          = ".$paperList[ $i ]['rev1'];
		echo "<br>Rev2          = ".$paperList[ $i ]['rev2'];
		echo "<br>Rev3          = ".$paperList[ $i ]['rev3'];
		
		
		echo "<br>";
	}		
}


function addPaper($paperList, $paper)
{

	$numPapers = $paperList[ 'numPapers' ] ;

	//Pick a new pid
	$pid = $paperList[$numPapers -1]['id'] + 27;

	$paperList[ $numPapers ]['id']      	= $pid;
	$paperList[ $numPapers ]['title']   	= $paper['title'];
	$paperList[ $numPapers ]['author'] 	= $paper['author'];
	$paperList[ $numPapers ]['authorID'] 	= $paper['authorID'];
	$paperList[ $numPapers ]['abstract']   	= $paper['abstract'];
	$paperList[ $numPapers ]['status'] 	= $paper['status'];
	$paperList[ $numPapers ]['attachment'] 	= $paper['attachment'];
	$paperList[ $numPapers ]['rev1']	= $paper['rev1'];
	$paperList[ $numPapers ]['rev2']	= $paper['rev2'];
	$paperList[ $numPapers ]['rev3']	= $paper['rev3'];

	$numPapers++;
	$paperList[ 'numPapers' ] = $numPapers;

	return $pid;
	
}

function savePaperList($paperList, $filename)
{
	$fp =fopen($filename,"w");
	
	$str = sprintf("<papers>\n");
	fputs($fp, $str);

	$numPapers = $paperList[ 'numPapers' ] ;
	for($i=0; $i <$numPapers;$i++){
		
		$str = sprintf("<paper>\n");
		fputs($fp, $str);
		$str = sprintf("<id>%s</id>\n",       		$paperList[ $i ]['id']);
		fputs($fp, $str);
		$str = sprintf("<title>%s</title>\n", 		$paperList[ $i ]['title']);
		fputs($fp, $str);
		$str = sprintf("<author>%s</author>\n",   		$paperList[ $i ]['author']);
		fputs($fp, $str);
		$str = sprintf("<authorID>%s</authorID>\n", 	$paperList[ $i ]['authorID']);
		fputs($fp, $str);
		$str = sprintf("<abstract>%s</abstract>\n", 	$paperList[ $i ]['abstract']);
		fputs($fp, $str);
		$str = sprintf("<attachment>%s</attachment>\n",   	$paperList[ $i ]['attachment']);
		fputs($fp, $str);
                $str = sprintf("<camera>%s</camera>\n",   	$paperList[ $i ]['camera']);
		fputs($fp, $str);
		$str = sprintf("<status>%s</status>\n", $paperList[ $i ]['status']);
		fputs($fp, $str);

		$str = sprintf("<rev1>%s</rev1>\n", $paperList[ $i ]['rev1']);
		fputs($fp, $str);
		$str = sprintf("<rev2>%s</rev2>\n", $paperList[ $i ]['rev2']);
		fputs($fp, $str);
		$str = sprintf("<rev3>%s</rev3>\n", $paperList[ $i ]['rev3']);
		fputs($fp, $str);
		$str = sprintf("<paid>%s</paid>\n", $paperList[ $i ]['paid']);
		fputs($fp, $str);
		
				
		$str = sprintf("</paper>\n");
		fputs($fp, $str);
	}	
	$str = sprintf("</papers>\n");	
	fputs($fp, $str);

	fclose($fp);
}


/*
function savePaperList($paperList, $filename)
{
   $fp =fopen($filename,"w");
	
   $str = "<papers>\n"; fputs($fp, $str);

   $numPapers = $paperList[ 'numPapers' ] ;
   for($i=0; $i <$numPapers;$i++){
		
	$str = "<paper>\n";
        fputs($fp, $str);
	$str = "<id>".$paperList[ $i ]['id']."</id>\n";
        fputs($fp, $str);
	$str = "<title>".$paperList[ $i ]['title']."</title>\n";
        fputs($fp, $str);
	$str = "<author>".$paperList[ $i ]['author']."</author>\n"; 
        fputs($fp, $str);  
	$str = "<authorID>".$paperList[ $i ]['authorID']."</authorID>\n";
        fputs($fp, $str);
        $str = "<abstract>".$paperList[ $i ]['abstract']."</abstract>\n"; 
        fputs($fp, $str);
	$str="<attachment>".$paperList[$i]['attachment']."</attachment>\n";     
        fputs($fp, $str);
	$str = "<status>".$paperList[ $i ]['status']."</status>\n"; 
        fputs($fp, $str);
		
        $numReviewers = $paperList[$i][ 'numReviewers' ] ;
	for($j=0; $j <$numReviewers;$j++){
	  $str = "<reviewer>".$paperList[ $i ]['reviewer'][$j]."</reviewer>\n";
          fputs($fp, $str);
 	}
		
	$str = "</paper>\n";
        fputs($fp, $str);
   }	
	$str = "</papers>\n";
        fputs($fp, $str);	
	
        fclose($fp);
}
*/

?>