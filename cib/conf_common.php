<?php
include("conf_review.php");

function TestgetUserNumByID($usersList, $paperList)
{
	$numPapers = $paperList[ 'numPapers' ] ;

	for($i=0; $i < $numPapers ; $i++){
		$authorID = $paperList[$i]['authorID'];
		$userNum  = getUserNumById($usersList, $authorID);

		echo "<br>In ID".$authorID;
		echo "<br>out ID".$userNum;
	}
}

function cleanStr($input)
{
	for($i=0;$i<strlen($input);$i++) {
		$ch = substr($input, $i, 1);
		if(ord($ch) < 37)$ch = ' ';
		else if( ord($ch) >= 127) $ch = '?';
		else if($ch =='&' ) $ch = "and";
		else if($ch == '<' || $ch == '>') $ch = '?';
		
            		$output .= $ch;;

        	}

    return $output;
}

function displayUsersList($userNum, $start,$category)
{
	include("conn.php");
	$userresult=mysqli_query($link, "SELECT count(*) as cnt from user where category='$category' ");
	$user_row = mysqli_fetch_array( $userresult );

	$numUsers = $user_row["cnt"];
	
	if($start + 10 < $numUsers) $end = $start + 10;
	else $end = $numUsers;
	
	printf("<form action=\"conf_showUsersList.php\" method=\"get\" name=\"usercategory\" >\n");
	printf("<input type=\"hidden\" name=\"id\" value=\"$userNum\">\n");
	printf("<input type=\"hidden\" name=\"start\" value=\"$start\">\n");
	printf("<br>User Category : <select name=\"category\" onchange=\"usercategory.submit();\" >\n");
	($category=="author")?printf("<option value=\"author\" selected>Author</option>\n"):printf("<option value=\"author\">Author</option>\n");
	($category=="attendee")?printf("<option value=\"attendee\" selected>Attendee</option>\n"):printf("<option value=\"attendee\">Attendee</option>\n");
	($category=="rev")?printf("<option value=\"rev\" selected>Reviewer</option>\n"):printf("<option value=\"rev\">Reviewer</option>\n");
	printf("</select>\n");
	printf("</form>\n");
	
	printf("<table width=100%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'><div><table width=95%% align=center border=0 cellpading=0 cellspacing=5>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"3\" FACE=\"Verdana, Arial\" ><i>Num Registered Users: <b>%d</b><i></td>\n", $numUsers);
	printf("<td style='border-width:0;'><FONT SIZE=\"3\" FACE=\"Verdana, Arial\" ><i>Displaying <b>%d - %d</b></i> <td style='border-width:0;'>\n",
		$start+1, $end);

	printf("<td style='border-width:0;' align=right> <FONT SIZE=\"1\" FACE=\"Verdana, Arial\">\n");
	if($start - 10 >= 0){
		printf("<a href=\"conf_showUsersList.php?id=%s&start=%s&category=%s\">
  	<img src=\"previous.png\" alt=\"Previous Users\"/></a><i>previous</i>\n", $userNum , $start - 10,$category);
	}
	if($start + 10 < $numUsers){
		printf("<a href=\"conf_showUsersList.php?id=%s&start=%s&category=%s\">
  	<img src=\"next.png\" alt=\"Next Users\"/></a><i>next</i>\n", $userNum , $start + 10,$category);
	}
	printf("</td>\n");
	
	printf("</table></div>

\n");
	   
	printf("<table width=95%% align=center border=0 cellpading=0 cellspacing=1>\n");
	printf("<tr bgcolor=\"aliceblue\">\n");
	printf("<td style='border-width:0;'></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Name</b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>User ID</b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Email</b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Organization</b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Country</b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b></b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b></b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b></b></td>\n");
	

  $userresult=mysqli_query($link, "SELECT * from user where category='$category' order by id limit $start,10 ");
  $i=1;
	while($user_row = mysqli_fetch_array( $userresult )){
		printf("<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.background='yellow';\" onmouseout= \"this.style.background='white';\">

			<td style='border-width:0;'>%d</td>
			<td style='border-width:0;'>%s %s</td>
			<td style='border-width:0;'>%s</td>
			<td style='border-width:0;'>%s</td>
			<td style='border-width:0;'>%s</td>
			<td style='border-width:0;'>%s</td>\n", $i++, $user_row[ 'firstname' ], $user_row[ 'lastname' ],
			$user_row[ 'id' ],
			$user_row[ 'email' ], $user_row[ 'organization'], $user_row[ 'country' ]);

			//Edit, view, remove	
		printf("<td style='border-width:0;'><a href=\"conf_showUserInfo.php?id=%s\"><img src=\"info.png\" alt=\"info\"/></a></td>\n", $user_row[ 'id' ]);
		printf("<td style='border-width:0;'><a href=\"conf_editUserInfo.php?id=%s\"><img src=\"edit.png\" alt=\"edit\"/></a></td>\n", $user_row[ 'id' ]);
		printf("<td style='border-width:0;'><a href=\"conf_removeUser.php?id=%s\"><img src=\"remove.png\" alt=\"remove\"/></a></td>\n", $user_row[ 'id' ]);
			
	}

	printf("</table>

\n");

	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'><div><table width=95%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"1\" FACE=\"Verdana, Arial\" >__</td>\n");
	printf("</table></div>

\n");
	
	printf("</table>\n");
	
}	



function displayPaperList($userNum, $start,$assignedToTrackChair,$trackName,$authorId,$paperId,$revId,$camera)
{
  include("conn.php");
  
  $cond="";
if($assignedToTrackChair!="") { 
if($assignedToTrackChair=="assigned")
{
$cond .=" and paper.assignedtochair=1 ";
}elseif($assignedToTrackChair=="rejected")
{
$cond .=" and paper.rejectedbyadmin=1 ";
}elseif($assignedToTrackChair=="none")
{
$cond .=" and paper.rejectedbyadmin=0 and paper.assignedtochair=0 ";
}elseif($assignedToTrackChair=="status_accepted")
{
$cond .=" and paper.status='accepted' ";
}elseif($assignedToTrackChair=="status_rejected")
{
$cond .=" and paper.status='rejected' ";
}elseif($assignedToTrackChair=="status_reset")
{
$cond .=" and paper.status='Not reviewed' ";
}
}

if($trackName!="")
{
$cond .=" and paper.track='".$trackName."' ";
}

if($authorId!="")
{
$cond .=" and paper.authorID=$authorId ";
}

if($paperId!="")
{
$cond .=" and paper.id=$paperId ";
}

if($revId!="")
{
$cond .=" and paper.id in(select pid from rev where rev=$revId) ";
}

if($camera=="yes")
{
$cond .= "  and paper.camera <>'' ";
}elseif ($camera=="no"){
$cond .= "  and paper.camera ='' ";
}



	$paperresult=mysqli_query($link, "SELECT count(*) as cnt from paper,user where paper.authorID=user.id $cond");
	$paper_row = mysqli_fetch_array( $paperresult );

	$numPapers = $paper_row["cnt"];
	if($start + 10 < $numPapers) $end = $start + 10;
	else $end = $numPapers;
	
print("<br/>");	
print("<a href='papersStatistics.php' target='_blank'>Papers Statistics</a>");
print("<br/><br/>");
printf("<form name='showPaperList' action='conf_showPaperList.php' method='get'>\n");

printf("<input type='hidden' name='id' value='$userNum' />\n");
printf("<input type='hidden' name='start' value='0' />\n");
printf("Track:<select name='track' >\n");
printf("<option value=''></option>");
$trackresult=mysqli_query($link, "SELECT name from track order by name");
while($track_row = mysqli_fetch_array( $trackresult )){
($track_row["name"]==$trackName)?printf("<option value='$track_row[name]' selected>$track_row[name]</option>"):printf("<option value='$track_row[name]' >$track_row[name]</option>");  
}
printf("</select>\n");

printf("Status:<select name='assigned' >\n");
printf("<option value=''></option>");
($assignedToTrackChair=="assigned")?printf("<option value='assigned' selected>Assigned to track chair</option>"):printf("<option value='assigned'>Assigned to track chair</option>");
($assignedToTrackChair=="rejected")?printf("<option value='rejected' selected>Rejected by admin</option>"):printf("<option value='rejected'>Rejected by admin</option>");
($assignedToTrackChair=="none")?printf("<option value='none' selected>Not Assigned and not Rejected</option>"):printf("<option value='none'>Not Assigned and not Rejected</option>");
($assignedToTrackChair=="status_accepted")?printf("<option value='status_accepted' selected>Accepted</option>"):printf("<option value='status_accepted'>Accepted</option>");
($assignedToTrackChair=="status_rejected")?printf("<option value='status_rejected' selected>Rejected</option>"):printf("<option value='status_rejected'>Rejected</option>");
($assignedToTrackChair=="status_reset")?printf("<option value='status_reset' selected>Not reviewed</option>"):printf("<option value='status_reset'>Not reviewed</option>");
printf("</select>\n");

printf("Author Id:<input  name='author' value='$authorId' />\n");
printf("Paper Id:<input  name='paper' value='$paperId' />\n");


printf("Reviewer:<select  name=\"revid\">\n");
		printf("<option value=''></option>\n");
		$reviewersresult=mysqli_query($link, "SELECT * from user where category not in( 'author' ,'attendee' , 'admin') order by firstname");
   		 while($reviewers_row = mysqli_fetch_array( $reviewersresult ))
		{
      		$histresult=mysqli_query($link, "SELECT count(*) as cnt from rev where rev=$reviewers_row[id]"); 
     		 $hist_row = mysqli_fetch_array( $histresult ) ;
      $revSelect=($reviewers_row['id']==$revId)? "selected":"";
  		  printf("<option value='%s' title=\"Organization: %s \nCountry: %s \nNo of Assigned Papers: %s\" $revSelect>%s </option>\n",
				$reviewers_row['id'], $reviewers_row['organization'], $reviewers_row['country'],
				$hist_row['cnt'], $reviewers_row['firstname']." ".$reviewers_row['lastname']);
		}
printf("</select>\n");

printf("With Camera Ready:<select name='camera' >\n");
printf("<option value=''></option>");
($camera=="yes")?printf("<option value='yes' selected>Yes</option>"):printf("<option value='yes'>Yes</option>");
($camera=="no")?printf("<option value='no' selected>No</option>"):printf("<option value='no'>No</option>");
printf("</select>\n");
		
printf("<input type='submit' name='filter' value='Filter' />");
printf("</form>\n");
printf("<hr/>");
	
	printf("<table width=100%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'><div><table width=95%% align=center border=0 cellpading=0 cellspacing=5>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"3\" FACE=\"Verdana, Arial\" ><i>Total Papers # : <b>%d</b></i></td>\n", $numPapers);
	printf("<td style='border-width:0;'><FONT SIZE=\"3\" FACE=\"Verdana, Arial\" ><i>Displaying <b>%d - %d</b></i> <td style='border-width:0;'>\n",
		$start+1, $end);

	printf("<td style='border-width:0;' align=right> <FONT SIZE=\"1\" FACE=\"Verdana, Arial\">\n");
	if($start - 10 >= 0){
	printf("<a href=\"conf_showPaperList.php?id=%s&start=%s&track=%s&assigned=%s&author=%s&paper=%s&nu=false\">
  	<img src=\"first.png\" alt=\"First Papers\"/></a><i>first</i>\n", $userNum , 0,$trackName,$assignedToTrackChair,$authorId,$paperId);
		printf("<a href=\"conf_showPaperList.php?id=%s&start=%s&track=%s&assigned=%s&author=%s&paper=%s&nu=false\">
  	<img src=\"previous.png\" alt=\"Previous Papers\"/></a><i>previous</i>\n", $userNum , $start - 10,$trackName,$assignedToTrackChair,$authorId,$paperId);
  	
	}
	if($start + 10 < $numPapers){
		printf("<a href=\"conf_showPaperList.php?id=%s&start=%s&track=%s&assigned=%s&author=%s&paper=%s&nu=false\">
  	<img src=\"next.png\" alt=\"Next Papers\"/></a><i>next</i>\n", $userNum, $start + 10,$trackName,$assignedToTrackChair,$authorId,$paperId);
  	printf("<a href=\"conf_showPaperList.php?id=%s&start=%s&track=%s&assigned=%s&author=%s&paper=%s&nu=false\">
  	<img src=\"last.png\" alt=\"LastPapers\"/></a><i>last</i>\n", $userNum, floor(($numPapers/10))*10,$trackName,$assignedToTrackChair,$authorId,$paperId);
	}
	printf("</td>\n");
	

	printf("</table></div>

\n");
	
printf("<table width=95%% align=center border=0 cellpading=0 cellspacing=2>\n");
	printf("<tr bgcolor=\"aliceblue\">\n");
	printf("<td style='border-width:0;'>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Paper List</b></td>\n");

  $track[0]['title'] = "Select track......"; //null track
  $trackresult=mysqli_query($link, "SELECT name from track order by name");
  $i=1;
  while($track_row = mysqli_fetch_array( $trackresult )){
   $track[$i]['title'] =  $track_row["name"]  ;
   $i++;
  }
  


$paperresult=mysqli_query($link, "SELECT paper.*,user.*,paper.id as pid,paper.title as ptitle,user.id as uid from paper,user where paper.authorID=user.id $cond  order by paper.id limit $start,10 ");
	
  $i=1;
  
	while($paper_row = mysqli_fetch_array( $paperresult )){
		$authorID = $paper_row['authorID'];
		$userresult=mysqli_query($link, "SELECT * from user where id= $authorID");
	  $user_row = mysqli_fetch_array( $userresult );
	
		printf("<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.background='yellow';\" onmouseout= \"this.style.background='white';\">\n");
		

		printf("<td style='border-width:0;'>%d</td>\n", $i++);
		printf("<td style='border-width:0;' colspan=3>\n");
		printf("<table width=100%% cellpading=0 cellspacing=0>\n");
		printf("<tr>\n");
		


		printf("<td style='border-width:0;' width='40%%'><small><a href=\"conf_editPaperInfo.php?id=%s&pid=%s&del=true&camera=no\">
			<img src=\"deletep.png\" alt=\"Delete Paper\"/></a>\n<a href=\"conf_showPaperInfo.php?id=%s&pid=%s&np=false\">
			<img src=\"infop.png\" alt=\"Paper Info and full abstract\"/></a><b><i>Paper ID : </i></b>%s\n", 
			$paper_row[ 'uid' ], $paper_row[ 'pid' ],$paper_row[ 'uid' ], $paper_row[ 'pid' ],$paper_row[ 'pid' ]);

		printf("<td style='border-width:0;' align=right>\n");
		
		$chairresult=mysqli_query($link, "SELECT user.* from user,track where track.chair=user.id and track.name like '%$paper_row[track]%'");
	  $chair_row = mysqli_fetch_array( $chairresult );
	  
		//$userNum2 = getUserNumById($usersList, $paperList[$i]['rev1']);
		printf("<b><i>Track Chair : </i></b><a href=\"conf_showPaperList.php?id=%s&start=0&nu=false\" 
			title=\"Display paper/track list for %s %s\">%s %s</a>\n", 
		$chair_row['id'],
		$chair_row[ 'firstname' ],$chair_row[ 'lastname' ],
		$chair_row[ 'firstname' ],$chair_row[ 'lastname' ]);
		printf("<br><i><small>Email:</i><a href=\"mailto:%s\"><i><small>%s</i></a>\n", 
			$chair_row[ 'email' ], $chair_row[ 'email' ]);


		printf("<tr><td style='border-width:0;'><small><b><i>Author:</i></b>
			<a href=\"conf_showPaperList.php?id=%s&start=0&nu=false\"  title=\"Organization: %s\nCountry: %s\">%s %s</a>\n", 
			$user_row[ 'id' ],
			$user_row[ 'organization' ],$user_row[ 'country' ],
			$user_row[ 'firstname' ],$user_row[ 'lastname' ]);

		printf("<td style='border-width:0;' align=right><small>\n");
		printf("<form method=\"get\" action=\"conf_assignTrack.php\" name=\"application\">\n");
		printf("<select  name=\"track\">\n");
		printf("<option value='-1'>%s</option>\n",  $track[0]['title']);
    for($j=1; $j < count($track); $j++)
		{
			if($paper_row[ 'track' ] == $track[$j]['title'])
				printf("<option selected>%s</option>\n",  $track[$j]['title']);
			else
				printf("<option>%s</option>\n",  $track[$j]['title']);
		}
		printf("</select>\n");
		printf("<input type=\"hidden\" name=\"id\" value=%s> \n", $userNum);
		printf("<input type=\"hidden\" name=\"pid\" value=%s> \n", $paper_row['pid']);
		printf("<input type=\"hidden\" name=\"start\" value=%s> \n", $start);
		printf("<input type=\"submit\" value=\"Update Track\" title=\"Select a track from the list then press this button to assign paper to track...The track chair name for the paper will be updated accordingly\"> \n");
		printf("</form>\n");
if($paper_row[ 'assignedtochair' ]==0){
		printf("<tr>\n");
		printf("<td style='border-width:0;'>\n");
		printf("<form   method=\"get\" action=\"conf_assignToChair.php\" name=\"assignToChair\">\n");
		printf("<input type=\"hidden\" name=\"id\" value=%s> \n", $userNum);
		printf("<input type=\"hidden\" name=\"pid\" value=%s> \n", $paper_row['pid']);
		printf("<input type=\"hidden\" name=\"start\" value=%s> \n", $start);
		if($paper_row['rejectedbyadmin']==1)
		printf("<input type=\"submit\" value=\"Assign The Paper to Track Chair\" disabled=\"disabled\" title=\"\"> \n");
		else
		printf("<input type=\"submit\" value=\"Assign The Paper to Track Chair\" title=\"\"> \n");
		printf("</form>\n");
}		
		printf("<tr><td style='border-width:0;' colspan=2><small><b><i>Title : </i></b><a href=\"papers/%s\" title=\"Paper ID: %s\nAbstract: %s \n\nClick to download..\"> %s </a>\n",
			$paper_row[ 'attachment' ], $paper_row[ 'pid' ], $paper_row[ 'abstract' ], $paper_row[ 'ptitle' ]);
	//	if($paper_row[ 'attachmentwithoutauthor' ]!=null)
   // {
     printf("<tr><td style='border-width:0;' colspan=2><small><b><i>Paper without author name : </i></b><a href=\"papers/%s\" title=\"Paper ID: %s\nAbstract: %s \n\nClick to download..\"> %s </a>\n",
			$paper_row[ 'attachmentwithoutauthor' ], $paper_row[ 'pid' ], $paper_row[ 'abstract' ], $paper_row[ 'ptitle' ]);
			if($paper_row[ 'camera' ]!=""){
			printf("<tr><td style='border-width:0;' colspan=2><small><b><i>Paper with camera ready : </i></b><a href=\"papers/%s\" title=\"Paper ID: %s\nAbstract: %s \n\nClick to download..\"> %s </a>\n",
			$paper_row[ 'camera' ], $paper_row[ 'pid' ], $paper_row[ 'abstract' ], $paper_row[ 'ptitle' ]); }
  //  }else{
		printf("<tr>\n");
		printf("<td style='border-width:0;'>\n");
		printf("<form  enctype=\"multipart/form-data\" method=\"post\" action=\"conf_uploadPaperWithoutAuthor.php\" name=\"application1\">\n");
    printf("<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"20000000\" />\n");
    printf("<input name= \"uploadedfile\" type=\"file\" size=\"70\" />\n");
		printf("<input type=\"hidden\" name=\"id\" value=%s> \n", $userNum);
		printf("<input type=\"hidden\" name=\"pid\" value=%s> \n", $paper_row['pid']);
		printf("<input type=\"hidden\" name=\"aid\" value=%s> \n", $paper_row['authorID']);
		printf("<input type=\"hidden\" name=\"start\" value=%s> \n", $start);
		printf("<input type=\"submit\" value=\"Uplaod The Paper without the Author\" title=\"\"> \n");
		printf("</form>\n");
 //  }
    
    $revresult=mysqli_query($link, "SELECT firstname,lastname,email,rev from rev,user,paper where rev.pid=paper.id and rev.rev=user.id and pid=$paper_row[pid]  ");
    $revno=1;
    while($rev_row = mysqli_fetch_array( $revresult ))
		{

		printf("<tr>\n");
		printf("<td style='border-width:0;' colspan=2><small>\n");
		//$userNum2 = getUserNumById($usersList, $paperList[$i]['rev2']);
		printf("<b><i>Reviewer %d : </i></b><a href=\"conf_showPaperList.php?id=%s&start=0&nu=false\"
			title=\"Display paper/track list for %s %s\">%s %s</a>\n", 
		$revno++,$rev_row['rev'],
		$rev_row[ 'firstname' ],$rev_row[ 'lastname' ],
		$rev_row[ 'firstname' ],$rev_row[ 'lastname' ]);
		printf("Email : <a href=\"mailto:%s\"><i><small>%s</i></a>\n", 
			$rev_row[ 'email' ], $rev_row[ 'email' ]);
    }


		printf("<tr><td style='border-width:0;'>\n");
		printf("<form action=\"conf_emailPaperRejectForm.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"emailPaperRejectForm\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $paper_row[ 'pid' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"authorID\"/>\n", $authorID);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"uid\"/>\n", $userNum);
                printf("<input type=\"hidden\" name=\"start\" value=%s> \n", $start);
                if($paper_row['rejectedbyadmin']==0)
    		printf("<input type=\"submit\" value=\"Email Paper Rejection to Author\"  title=\"Email paper rejection to author\"/>\n");	
    		printf("</form></td>\n"); 
		
		printf("<td style='border-width:0;' align=right><small><b><i>Status: </i></b>%s\n", $paper_row['status']);

//------------------------------------------------------------------------
//------------------------------------------------------------------------
                printf("<form action=\"conf_emailPaperReviewsForm.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $paper_row[ 'pid' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"authorID\"/>\n", $authorID);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"uid\"/>\n", $userNum);
                printf("<input type=\"hidden\" name=\"start\" value=%s> \n", $start);
    		printf("<input type=\"submit\" value=\"Email Reviews to Author\"  title=\"Email Reviews and decision to author\"/>\n");	
    		printf("</form></td>\n"); 
    		
    		printf("<tr>\n");
		printf("<td style='border-width:0;' colspan='2'>\n");
		printf("<form   method=\"get\" action=\"conf_blockedReviewer.php\" name=\"blockedReviewer\">\n");
		printf("<input type=\"hidden\" name=\"id\" value=%s> \n", $userNum);
		printf("<input type=\"hidden\" name=\"pid\" value=%s> \n", $paper_row['pid']);
		printf("<input type=\"hidden\" name=\"start\" value=%s> \n", $start);
		printf("<select  name=\"rev\">\n");
		printf("<option value=0>....Select Reviewer....</option>\n");
		$reviewersresult=mysqli_query($link, "SELECT * from user where category not in( 'author' ,'attendee' , 'admin') and id not in(SELECT uid from paper_blocked_reviewer where pid=$paper_row[pid])  order by firstname");
   		 while($reviewers_row = mysqli_fetch_array( $reviewersresult ))
		{
      		$histresult=mysqli_query($link, "SELECT count(*) as cnt from rev where rev=$reviewers_row[id]"); 
     		 $hist_row = mysqli_fetch_array( $histresult ) ;
      
  		  printf("<option value=%s title=\"Organization: %s \nCountry: %s \nNo of Assigned Papers: %s\">%s </option>\n",
				$reviewers_row['id'], $reviewers_row['organization'], $reviewers_row['country'],
				$hist_row['cnt'], $reviewers_row['firstname']." ".$reviewers_row['lastname']);
		}
		printf("</select>\n");
		
		printf("<input type=\"submit\" value=\"+ >>\" name=\"addblockedreviewer\" title=\"Add to blocked reviewers\"> \n");
		
		printf("<select  name=\"blockedrev\">\n");
		printf("<option value=0>....Blocked Reviewers....</option>\n");
		$reviewersresult=mysqli_query($link, "SELECT * from user,paper_blocked_reviewer where user.id=paper_blocked_reviewer.uid and  paper_blocked_reviewer.pid=$paper_row[pid]  order by firstname");
   		 while($reviewers_row = mysqli_fetch_array( $reviewersresult ))
		{
      		$histresult=mysqli_query($link, "SELECT count(*) as cnt from rev where rev=$reviewers_row[id]"); 
     		 $hist_row = mysqli_fetch_array( $histresult ) ;
      
  		  printf("<option value=%s title=\"Organization: %s \nCountry: %s \nNo of Assigned Papers: %s\">%s </option>\n",
				$reviewers_row['id'], $reviewers_row['organization'], $reviewers_row['country'],
				$hist_row['cnt'], $reviewers_row['firstname']." ".$reviewers_row['lastname']);
		}
		printf("</select>\n");
		
		printf("<input type=\"submit\" value=\"-\" name=\"removeblockedreviewer\" title=\"Remove from blocked reviewers\"> \n");
		printf("</form>\n");
		
	printf("</table>\n");
		
//-------------------------------------------------------------------------
//------------------------------------------------------------------------
			
	}

	printf("</table>\n");


	
	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'><div><table width=95%% align=center border=0 cellpading=0 cellspacing=0>\n");
	//printf("<td style='border-width:0;'><FONT SIZE=\"1\" FACE=\"Verdana, Arial\" >___</td>\n");
	
	printf("<td style='border-width:0;'><FONT SIZE=\"3\" FACE=\"Verdana, Arial\" ><i>Total Papers # : <b>%d</b></i></td>\n", $numPapers);
	printf("<td style='border-width:0;'><FONT SIZE=\"3\" FACE=\"Verdana, Arial\" ><i>Displaying <b>%d - %d</b></i> <td style='border-width:0;'>\n",
		$start+1, $end);
	printf("<td style='border-width:0;' align=right> <FONT SIZE=\"1\" FACE=\"Verdana, Arial\">\n");
	if($start - 10 >= 0){
	printf("<a href=\"conf_showPaperList.php?id=%s&start=%s&track=%s&assigned=%s&author=%s&paper=%s&nu=false\">
  	<img src=\"first.png\" alt=\"First Papers\"/></a><i>first</i>\n", $userNum , 0,$trackName,$assignedToTrackChair,$authorId,$paperId);
		printf("<a href=\"conf_showPaperList.php?id=%s&start=%s&track=%s&assigned=%s&author=%s&paper=%s&nu=false\">
  	<img src=\"previous.png\" alt=\"Previous Papers\"/></a><i>previous</i>\n", $userNum , $start - 10,$trackName,$assignedToTrackChair,$authorId,$paperId);
  	
	}
	if($start + 10 < $numPapers){
		printf("<a href=\"conf_showPaperList.php?id=%s&start=%s&track=%s&assigned=%s&author=%s&paper=%s&nu=false\">
  	<img src=\"next.png\" alt=\"Next Papers\"/></a><i>next</i>\n", $userNum, $start + 10,$trackName,$assignedToTrackChair,$authorId,$paperId);
  	printf("<a href=\"conf_showPaperList.php?id=%s&start=%s&track=%s&assigned=%s&author=%s&paper=%s&nu=false\">
  	<img src=\"last.png\" alt=\"LastPapers\"/></a><i>last</i>\n", $userNum, floor(($numPapers/10))*10,$trackName,$assignedToTrackChair,$authorId,$paperId);
	}
	printf("</td>\n");
	
	printf("</table></div>

\n");
	
	printf("</table>\n");

	
    
}	




function displayUserPaperList($uid, $start)
{
  include("conn.php");
  $paperresult=mysqli_query($link, "SELECT count(*) as cnt from paper where authorID=$uid ");
	$paper_row = mysqli_fetch_array( $paperresult );

	$numPapers = $paper_row['cnt'];
	if($start + 10 < $numPapers) $end = $start + 10;
	else $end = $numPapers;
	

  if($numPapers==0) {
		return;
	}
	


	printf("<table width=100%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'><div><table width=100%% align=center border=0 cellpading=0 cellspacing=5>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i>Submitted Papers: <b>%d</b></i></td>\n", $numPapers);
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i>Displaying <b>%d - %d</b></i> <td style='border-width:0;'>\n",
		$start+1, $end);

	printf("<td style='border-width:0;' align=right> <FONT SIZE=\"1\" FACE=\"Verdana, Arial\">\n");
	if($start - 10 >= 0){
		printf("<a href=\"conf_showPaperList.php?id=%s&start=%s&nu=false\">
  	<img src=\"previous.png\" alt=\"Previous Papers\"/></a><i>previous</i>\n", $uid, $start - 10);
	}
	if($start + 10 < $numPapers){
		printf("<a href=\"conf_showPaperList.php?id=%s&start=%s&nu=false\">
  	<img src=\"next.png\" alt=\"Next Papers\"/></a><i>next</i>\n", $uid, $start + 10);
	}
	printf("</td>\n");
	

	printf("</table></div>

\n");
			
	
   
	printf("<table width=100%% align=center border=0 cellpading=0 cellspacing=1>\n");
	printf("<tr bgcolor=\"aliceblue\">\n");
	printf("<td style='border-width:0;'></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Paper ID</b></td>\n");
	printf("<td style='border-width:0;'></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Title</b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Status</b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b></b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b></b></td>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b></b></td>\n");
	

        $found = 0;
      $paperresult=mysqli_query($link, "SELECT * from paper where authorID=$uid order by id limit $start,10  ");
   $i=1;
   while ( $paper_row = mysqli_fetch_array( $paperresult ) ) {
		printf("<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.background='yellow';\" onmouseout= \"this.style.background='white';\">

			<td style='border-width:0;'>%d</td>
			<td style='border-width:0;'>%s</td>
			<td style='border-width:0;'><img src=\"pdf.gif\">
			<td style='border-width:0;'><a href=\"papers/%s\"> %s </a>\n",
			$i++, $paper_row[ 'id' ],
			$paper_row[ 'attachment' ], $paper_row[ 'title' ]);
		 
  
if($paper_row[ 'status' ] == "accepted")
{
 $found = 1;
 
 if(strlen($paper_row[ 'camera' ]) > 0)
 {

       printf("<br><img src=\"check2.png\"/><a href=\"papers/%s\"><i> Camera Ready Version</i></a>\n", $paper_row[ 'camera' ]);

 }
  else
  {
      printf("<br><img src=\"info.png\"><i> Awaiting for Camera Ready Version...</i>\n");
  }

}


printf("<td style='border-width:0;'>%s\n", $paper_row[ 'status' ]);

		   //Edit, view, remove	

if($paper_row[ 'status' ] == "accepted")
{
		printf("<td style='border-width:0;'><a href=\"conf_editPaperInfo.php?camera=yes&id=%s&pid=%s&del=false\"><img src=\"camera.png\" alt=\"Upload Camera Ready Version\"/></a></td>\n", $paper_row[ 'authorID' ],$paper_row[ 'id' ]);
}
/*
		printf("<td><a href=\"conf_editPaperInfo.php?id=%s&pid=%s&del=true\"><img src=\"remove.png\" alt=\"remove\"/></a></td>\n", $paper_row[ 'authorID' ],$paper_row[ 'id' ]);
*/

  	$revresult=mysqli_query($link, "SELECT count(*) as cnt from rev where pid=$paper_row[id] ");
	$rev_row = mysqli_fetch_array( $revresult);

	$numRev = $rev_row['cnt'];
	if($numRev>0)
	{
	printf("<td style='border-width:0;'><a href=\"conf_editPaper.php?spf=no&id=%s&pid=%s\"><img src=\"edit.png\" title=\"Edit\" alt=\"Edit\"/>Edit</a></td>\n", $paper_row[ 'authorID' ],$paper_row[ 'id' ]);
	}else{
	printf("<td style='border-width:0;'><a href=\"conf_editPaper.php?spf=yes&id=%s&pid=%s\"><img src=\"edit.png\" title=\"Edit\" alt=\"Edit\"/>Edit</a></td>\n", $paper_row[ 'authorID' ],$paper_row[ 'id' ]);
	}
          printf("<td style='border-width:0;'><td style='border-width:0;'>\n");
		
	}

	printf("</table>

\n");

	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'><div><table width=95%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<td style='border-width:0;'><FONT SIZE=\"1\" FACE=\"Verdana, Arial\" >___</td>\n");
	printf("</table></div>

\n");
	
	printf("</table>\n");

       //$found=1;
	if($found)
       {
       printf("<i>To upload camera ready version, please click on the camera icon.</i>\n");
        }
	
}	


function printUpperBanner($m=1)
{
 print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"[]>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>CIBEC 2012</title>



    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" href="style.ie6.css" type="text/css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" href="style.ie7.css" type="text/css" media="screen" /><![endif]-->

    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="script.js"></script>
    
    <script type="text/javascript" >
    
    $(document).ready(function() {
    
   $(".cibec-vmenublockcontent-body").load("menu.php?m='.$m.'");
   $(".sponsers").load("sponsers.php");
   
 });
    
    </script>

</head>
<body>
<div id="cibec-main">
    <div class="cleared reset-box"></div>
    <div class="cibec-header">
        <div class="cibec-header-position">
            <div class="cibec-header-wrapper">
                <div class="cleared reset-box"></div>
                <div class="cibec-header-inner">
                <div class="cibec-logo">
                                                </div>
                </div>
            </div>
        </div>

    </div>
    <div class="cleared reset-box"></div>
    <div class="cibec-box cibec-sheet">
        <div class="cibec-box-body cibec-sheet-body">
            <div class="cibec-layout-wrapper">
                <div class="cibec-content-layout">
                    <div class="cibec-content-layout-row">
                        <div class="cibec-layout-cell cibec-sidebar1">
<div class="cibec-box cibec-vmenublock">
    <div class="cibec-box-body cibec-vmenublock-body">
                <div class="cibec-bar cibec-vmenublockheader">
                    <h3 class="t">Navigation</h3>
                </div>
                <div class="cibec-box cibec-vmenublockcontent">
                    <div class="cibec-box-body cibec-vmenublockcontent-body">


                                		<div class="cleared"></div>
                    </div>
                </div>
		<div class="cleared"></div>
    </div>
</div>
<div class="cibec-box cibec-block">
    <div class="cibec-box-body cibec-block-body">
                <div class="cibec-bar cibec-blockheader">
                    <h3 class="t">Sponsers</h3>
                </div>
                <div class="cibec-box cibec-blockcontent">
                    <div class="cibec-box-body cibec-blockcontent-body sponsers">


                                		<div class="cleared"></div>
                    </div>
                </div>
		<div class="cleared"></div>
    </div>
</div>

                          <div class="cleared"></div>
                        </div>
                        <div class="cibec-layout-cell cibec-content">
<div class="cibec-box cibec-post">
    <div class="cibec-box-body cibec-post-body">
<div class="cibec-post-inner cibec-article">
                               <!-- <h2 class="cibec-postheader">Workshops
                                </h2>  -->
                                                <div class="cibec-postcontent">';
  	
		
}




function printFooter()
{
 print '               </div>
                <div class="cleared"></div>
                </div>

		<div class="cleared"></div>
    </div>
</div>

                          <div class="cleared"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cleared"></div>
            <div class="cibec-footer">
                <div class="cibec-footer-body">
                            <div class="cibec-footer-text">
                                <p>Copyright &#169; 2012, CIBEC. All Rights Reserved.</p>
                                                            </div>
                    <div class="cleared"></div>
                </div>
            </div>
    		<div class="cleared"></div>
        </div>
    </div>
    <div class="cleared"></div>
    <p class="cibec-page-footer"></p>
    <div class="cleared"></div>
</div>

</body>
</html>';

}



function printLoginPage($message, $icon)
{
	printUpperBanner();
	printLoginScreen($message, $icon);
	printFooter();

}



function printLoginScreen($message, $icon)
{

	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
        printf("<tr><td style='border-width:0;'><P>&nbsp;</P>\n");
	printf(" <tr class=titleBar>\n");
	printf(" <td style='border-width:0;'></td>\n");
	printf(" <tr><td style='border-width:0;'>\n");


     printf("<p><img src=\"%s\"><strong>%s</strong></p>\n", $icon, $message);
     printf("<table width=100%% cellspacing =0 border=1>\n");
	printf("<td >\n");
	printf("<form action=\"conf_login.php\"  accept-charset=\"UTF-8\" method=\"post\" id=\"user-register\">\n");
	printf("<table>\n");
		printf("<td style='border-width:0;' colspan=2><h2>Author Login</h2>\n");
		printf("<tr>\n");
		printf("<td style='border-width:0;'><label>E-mail address: </label>\n");
 		printf("<td style='border-width:0;'><input type=\"text\" maxlength=\"64\" name=\"email\" size=\"50\" value=\"\" />\n");
	
		printf("<tr>\n");
 		printf("<td style='border-width:0;'><label>Password: </label>\n");
 		printf("<td style='border-width:0;'><input type=\"password\" maxlength=\"64\" name=\"password\"  size=\"40\" value=\"\"  />\n");

		printf("<tr>\n");
		printf("<td style='border-width:0;'><input type=\"submit\" value=\"Sign in\"  />\n");
                printf("<input type=\"hidden\" name =\"retrieve\" value=\"login\"  />\n");
                printf("</form>\n");

	printf("</table>\n");
	printf("</td>\n");
	printf("<td  valign=top>\n");
	printf("<table>\n");
		printf("<td style='border-width:0;' colspan=2><h2>New Author</h2>\n");
		printf("<tr>\n");
		printf("<td style='border-width:0;'><label><b>Don't have an account? </b></label>\n");
		printf("<tr>\n");
		printf("<td style='border-width:0;'><a href=\"register.php\">Register</a>\n");
	printf("</table>\n");
	printf("</td>\n");
      printf("</table>\n");
      
      printf("<form action=\"conf_login.php\"  accept-charset=\"UTF-8\" method=\"post\" id=\"retrieve\">\n");
		printf("<i><small>Forgot your password? ..Please enter your e-mail address below </i> \n");
 		printf("<br>E-mail address: <input type=\"text\" maxlength=\"64\" name=\"email\" size=\"50\" value=\"\" />\n");
		printf("<input type=\"submit\" value=\"Retrieve\"  />\n");
		printf("<input type=\"hidden\" name =\"retrieve\" value=\"retrievemypassword\"  />\n");
	printf("</form>\n");

   printf("</table>\n");
   

}



function printSubmitNewPaperPage($title, $author, $abstract, $userNum, $message, $icon)
{
  if(!isset($_SESSION['id']))
  {
    $str = sprintf("Location: submit.html");
		header($str);
  }
	printUpperBanner();
	printPaperForm($title, $author, $abstract, $userNum, $message, $icon);
	//print "Closed";
	printFooter();
}


function printPaperForm($title, $author, $abstract,  $userNum, $message, $icon)
{
 include("conn.php");
  $userresult=mysqli_query($link, "SELECT * from user where id=$userNum ");
	$user_row = mysqli_fetch_array( $userresult );
  $tracksresult=mysqli_query($link, "SELECT name from track order by name ");
	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
	
	printf(" <tr><td style='border-width:0;'>\n");

    
     printf("<br><div class=title>\n");	
    printf("<table width =100%%>\n");	
    printf("<td style='border-width:0;' align=left><img src=\"user.png\"><i><b>%s %s - %s</b></i>\n", $user_row['firstname'], $user_row['lastname'],$userNum);
    printf("<td  style='border-width:0;' align=right><a href=\"conf_showPaperList.php?id=%s&start=0\">
	<img src=\"openfolder.png\" alt=\"My Papers\"/></a><i>My Papers</i>\n", $user_row['id']);
    printf("<a href=\"conf_submitNewPaper.php?id=%s\">
	<img src=\"arrow.png\" alt=\"Submit a paper\"/></a><i>Submit Paper</i>\n", $user_row['id']);
    printf("<a href=\"conf_logout.php\"><img src=\"logout.png\" alt=\"Logout\"/></a><i>Logout</i>\n");
    
    printf("</table>");
    printf("</div>\n");

     printf("<div class=info_background>\n");
   
    printf("<p style='text-align:justify;'><img src=\"%s\">%s</p>\n",$icon, $message);
     printf("<div class=info>\n");
    printf("<table>\n");
    printf('<script language="javascript">
    
  function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}
    
   function validate(chk){
   var agree1 = document.getElementById("agree1");
   var agree2 = document.getElementById("agree2");
   var agree3 = document.getElementById("agree3");
   var track = document.getElementById("track");
   var title = document.getElementById("title");
   var authors = document.getElementById("authors");
   var abstract = document.getElementById("abstract");
   if (notEmpty(title, "you have to insert paper title") && notEmpty(authors, "you have to insert paper authors") && notEmpty(abstract, "you have to insert paper abstract"))
   {
   if (agree1.checked == 1 && agree2.checked == 1 && agree3.checked == 1 && track.value!="-1" )
   {
   return true;
   }else{
   alert("You have to select track and agree with confirmations.")
    return false;
   }
   }else{
     return false;
   }
  }
  

   </script>');
	 printf("<form enctype=\"multipart/form-data\" action=\"conf_uploadPaper.php\" method=\"post\" onsubmit=\" return(validate());\">\n");
	
  printf("<tr><td style='border-width:0;' valign=top>Track *</td>\n");
	printf("<td style='border-width:0;'><select name=\"track\" id=\"track\">\n");
  printf("<option value='-1'>Select Track</option>\n");
  while ( $a_row = mysqli_fetch_array( $tracksresult ) ) {
  printf("<option>%s</option>\n",$a_row["name"]);
    }
  printf("</select>\n");
  
	printf("<tr><td style='border-width:0;' valign=top>Title *</td>\n");
	printf("<td style='border-width:0;'><textarea  rows=\"3\" cols=\"60\" name=\"title\" id=\"title\">%s</textarea>\n", $title);

	printf("<tr><td  style='border-width:0;' valign=top>Authors *</td>\n");
	printf("<td style='border-width:0;'><textarea  rows=\"3\" cols=\"60\" name=\"authors\" id=\"authors\">%s</textarea>\n", $author);

	printf("<tr><td style='border-width:0;' valign=top>Abstract *</td>\n");
	printf("<td style='border-width:0;'><textarea  rows=\"15\" cols=\"60\" name=\"abstract\" id=\"abstract\">%s</textarea>\n", $abstract);
	
	printf("<tr><td style='border-width:0;' >Paper File *</td>\n");
	printf("<td style='border-width:0;'><input name= \"uploadedfile\" type=\"file\" size=\"70\" />\n");
	printf("<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"20000000\" />\n");
	printf("<tr><td colspan='2' style='border-width:0;'><h3>I confirm that: *<h3><tr> <td colspan='2'style='border-width:0;'><input type='checkbox' id='agree1' /> The submitted full paper is in electronic PDF format according to the IEEE Xplore Digital Library <a href='http://www.ieee.org/conferences_events/conferences/publishing/templates.html' target='_blank'>format/template</a>.");
	printf("<tr> <td colspan='2'style='border-width:0;'><input type='checkbox' id='agree2' /> My paper is a suitably anonymised pdf document, and meets the requirements for double-blind reviewing according to the double blind paper submission guidelines.");
  printf("<tr> <td colspan='2'style='border-width:0;'><input type='checkbox' id='agree3' /> The submitted full (and original research) paper is NOT submitted or published or under consideration anywhere in other conferences/journals.");
  
  //printf("<tr><td style='border-width:0;' colspan='2'><b><u>Note</u>: Uploaded files have to be in <u>PDF format</u></b>. Please refer to <a href=\"http://embs.papercept.net/conferences/support/word.php#wordtemplates\"> Paper template </a> in preparing your submmission.\n");

	printf("<input type=\"hidden\" value=\"%s\" name=\"id\" />\n", $user_row['id']);
	
    
	printf("<tr><td style='border-width:0;' colspan=2><img src=\"upload.png\" valign=bottom ><input type=\"submit\" value=\"Upload Paper\" />\n");
	printf("<br><br>* Indicates required field.\n");
	printf("</form></table>\n");

 printf("</div></div>\n");	
    printf("</table>\n");
   
 }


function printWelcomePage( $userNum, $message, $icon, $start)
{
  if(!isset($_SESSION['id']))
  {
    $str = sprintf("Location: submit.html");
		header($str);
  }
	printUpperBanner();
	printUserMainScreen( $userNum, $message, $icon, $start);
	printFooter();
}


function printUserMainScreen( $userNum, $message, $icon, $start)
{
  include("conn.php");
  $userresult=mysqli_query($link, "SELECT *  from user where id=$userNum ");
	$user_row = mysqli_fetch_array( $userresult );
	

	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
        printf("<tr><td style='border-width:0;'><P>&nbsp;</P>\n");
	printf(" <tr><td style='border-width:0;'>\n");


   
   printf("<br><div class=title>\n");
    printf("<table width =100%%>\n");	
    //printf("<td><H1>CIBEC 2010 Paper Submission</H1>\n");
    printf("<td style='border-width:0;' align=left><img src=\"user.png\"><i><b>%s %s - %s</b></i>\n", $user_row['firstname'], $user_row['lastname'],$userNum);

if($user_row['category']!="attendee"){
    printf("<td style='border-width:0;' align=right><a href=\"conf_showPaperList.php?id=%s&start=0&nu=false\">
  	<img src=\"openfolder.png\" alt=\"My Papers\"/></a><i>My Papers</i>\n", $userNum );
   // printf("<a href=\"conf_submitNewPaper.php?id=%s\"><img src=\"arrow.png\" alt=\"Submit a paper\"/></a><i>Submit Paper</i>\n", $userNum );
}	
     printf("<a href=\"conf_logout.php\"><img src=\"logout.png\" alt=\"Logout\"/></a><i>Logout</i>\n");
  	
    printf("</table>");	
    printf("</div>\n");
    
    //printf("<p><img src=\"user.png\"><b>%s %s</b></p>\n", $usersList[$userNum ]['firstname'], $usersList[$userNum ]['lastname']);

   // printf("<div class=admin-info>\n");
    printf("<br><img src=\"%s\">%s</p>\n",$icon, $message);
    

if( strlen($user_row['paid']) > 0)
{
    printf("<p><img src=\"check2.png\"><i><b> You are registered for the conference</b></i>\n");
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
// Payment Button
//-----------------------------------------------------------------------
// Note: Comment this section using regular C-style comments to hide the
// payment button
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

printf("<p><table width=90%%>\n");
printf("<td style='border-width:0;'  valign=center align=right>Online conference registration is now open..<td style='border-width:0;' align=right><a href=\"conf_paymentInterface.php?id=%s\"><img src=\"register-new.gif\"></a>\n", $userNum);
printf("</table>\n");

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------




    printf("<p>\n");
    displayUserPaperList($userNum ,  $start);



printf("<p>\n");
   displayReviewerPaperList( $userNum , 0);


   printf("<p>\n");
   displayTrackChairPaperList(  $userNum , 0);





   printf("</table>\n");

   }

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//Admin
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

function printPaperListScreen( $userNum, $start,$assignedToTrackChair,$track,$authorId,$paperId,$revId,$camera)
{
  if(!isset($_SESSION['id']))
  {
    $str = sprintf("Location: submit.html");
		header($str);
  }
	printUpperBanner();
	printPaperListView( $userNum, $start,$assignedToTrackChair,$track,$authorId,$paperId,$revId,$camera);
	printFooter();
}


function printPaperListView( $userNum, $start,$assignedToTrackChair,$track,$authorId,$paperId,$revId,$camera)
{

    

	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
        printf("<tr><td style='border-width:0;'><P>&nbsp;</P>\n");
		printf(" <tr><td style='border-width:0;'>\n");



    printf("<br><div class=title>\n");
    printf("<table width =100%%>\n");	
    printf("<td style='border-width:0;' align=left><img src=\"user.png\"><i><b>Admin</b></i>\n");
    //printf("<td align=center><b>CIBEC 2010 Paper List</b>\n");
    printf("<td style='border-width:0;' align=right><a href=\"conf_showPaperList.php?id=%s&start=0&nu=false\">
	<img src=\"openfolder.png\" alt=\"Paper List\"/></a><i>Paper List</i>\n", $userNum);
    printf("<a href=\"conf_showUsersList.php?id=%s&start=0&category=author\">
	<img src=\"users.png\" alt=\"List of Registered Users\"/></a><i>Users List</i>\n", $userNum );
   printf("<a href=\"conf_showEmailList.php\">
	<img src=\"users.png\" alt=\"Email List\"/></a><i>Email List</i>\n");
    printf("<a href=\"conf_logout.php\"><img src=\"logout.png\" alt=\"Logout\"/></a><i>Logout</i>\n");
    
    printf("</table>");	
    printf("</div>\n");
    
    printf("<div class=admin-info>\n");
    displayPaperList( $userNum, $start,$assignedToTrackChair,$track,$authorId,$paperId,$revId,$camera);
    
   printf("</div>");


    printf("<p><table>\n");
    printf("<td style='border-width:0;'>\n");
    printf("<form action=\"conf_showUsersList.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $userNum);
    printf("<input type=\"hidden\" value=\"0\"  name=\"start\"/>\n");
    printf("<input type=\"submit\" value=\"Show Registered Users List\"  />\n");	
    printf("</form>\n");

    printf("<td style='border-width:0;'>\n");
    printf("<form action=\"conf_showPaperList.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $userNum );
    printf("<input type=\"hidden\" value=\"0\"  name=\"start\"/>\n");
   
    printf("<input type=\"submit\" value=\"Show Submitted Paper List\"  />\n");	
    printf("</form>\n");

    printf("</table>\n");
    


   printf("</table>\n");
    

    
   

   }


function printUsersListScreen( $userNum, $start,$category)
{
  if(!isset($_SESSION['id']))
  {
    $str = sprintf("Location: submit.html");
		header($str);
  }
	printUpperBanner();
	printUsersListView( $userNum, $start,$category);
	printFooter();
}


function printUsersListView( $userNum, $start,$category)
{


	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
        printf("<tr><td style='border-width:0;'><P>&nbsp;</P>\n");
		printf(" <tr><td style='border-width:0;'>\n");


     printf("<br><div class=title>\n");   
    printf("<table width =100%%>\n");	
    printf("<td style='border-width:0;' align=left><img src=\"user.png\"><i><b>Admin</b></i>\n");
    //printf("<td align=center><b>CIBEC 2010 Users List</b>\n");
    printf("<td style='border-width:0;' align=right><a href=\"conf_showPaperList.php?id=%s&start=0&nu=false\">
	<img src=\"openfolder.png\" alt=\"List of Papers\"/></a><i>Paper List</i>\n", $userNum );
    printf("<a href=\"conf_showUsersList.php?id=%s&start=0&category=author\">
	<img src=\"users.png\" alt=\"List of Registered Users\"/></a><i>Users List</i>\n", $userNum );
   printf("<a href=\"conf_showEmailList.php\">
	<img src=\"users.png\" alt=\"Email List\"/></a><i>Email List</i>\n");
    printf("<a href=\"conf_logout.php\"><img src=\"logout.png\" alt=\"Logout\"/></a><i>Logout</i>\n");
    printf("</table>");	
    printf("</div>\n");
    
     printf("<div class=admin-info>\n");
    displayUsersList($userNum, $start,$category);
    
      printf("</div>\n");

    printf("<p><table>\n");
    printf("<td style='border-width:0;'>\n");
    printf("<form action=\"conf_showUsersList.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $userNum );
    printf("<input type=\"hidden\" value=\"0\"  name=\"start\"/>\n");
    printf("<input type=\"hidden\" value=\"author\"  name=\"category\"/>\n");
    printf("<input type=\"submit\" value=\"Show Registered Users List\"  />\n");	
    printf("</form>\n");

    printf("<td style='border-width:0;'>\n");
    printf("<form action=\"conf_showPaperList.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $userNum );
    printf("<input type=\"hidden\" value=\"0\"  name=\"start\"/>\n");
    
    printf("<input type=\"submit\" value=\"Show Submitted Paper List\"  />\n");	
    printf("</form>\n");

    printf("</table>\n");
  
  printf("</table>\n");


  
}



function printMessage($userNum, $message, $icon)
{
  include("conn.php");
  $userresult=mysqli_query($link, "SELECT * from user where id=$userNum ");
	$user_row = mysqli_fetch_array( $userresult );

	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
        printf("<tr><td><P>&nbsp;</P>\n");
		printf(" <tr><td>\n");
	
    	printf("<table width =80%%>\n");	
    	printf("<td><H1>CIBEC 2012 Paper Submission</H1>\n");
    	printf("<td align=center><img src=\"user.png\"><b>%s %s - %s</b>\n", $user_row['firstname'], $user_row['lastname'],$userNum);
    	printf("<td><a href=\"conf_showPaperList.php?id=%s&start=0&nu=false\">
		<img src=\"openfolder.png\" alt=\"My Papers\"/></a>\n", $userNum );
    	printf("<td><a href=\"conf_submitNewPaper.php?id=%s&start=0\">
		<img src=\"arrow.png\" alt=\"Submit a paper\"/></a>\n", $userNum);
       printf("<a href=\"conf_logout.php\"><img src=\"logout.png\" alt=\"Logout\"/></a><i>Logout</i>\n");
    	printf("</table>");
    	printf("<hr>\n");

    	printf("<p><img src=\"%s\">%s</p>\n",$icon, $message);
	

printf("</table>\n");



  }




function printEditPaperPage($userNum, $paperNum, $message, $icon,$camera='yes')
{
	if(!isset($_SESSION['id']))
  {
    $str = sprintf("Location: submit.html");
		header($str);
  }
	printUpperBanner();
	printEditPaperForm( $userNum, $paperNum, $message, $icon,$camera);
	printFooter();
}


function getPaperNumById($paperList, $pid)
{
	$numPapers = $paperList[ 'numPapers' ] ;
	for($i=0; $i < $numPapers; $i++){	
		if ( strcmp($paperList[$i]['id'],$pid) == 0) return($i);
	}
	
	return(-1);  //not in the system
}


function printEditPaperForm( $userNum, $paperNum, $message, $icon,$camera)
{
     
  include("conn.php");
  $paperresult=mysqli_query($link, "SELECT paper.*,user.firstname,user.lastname from paper,user where paper.authorID=user.id and paper.id=$paperNum ");
	$paper_row = mysqli_fetch_array( $paperresult );
	
	$tracksresult=mysqli_query($link, "SELECT name from track order by name ");

	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
        printf("<tr><td style='border-width:0;'><P>&nbsp;</P>\n");
		printf(" <tr><td style='border-width:0;'>\n");

       printf("<br><div class=title>\n");
    printf("<table width =100%%>\n");	
    //printf("<td><H1>CIBEC 2010 Paper Submission</H1>\n");
    printf("<td style='border-width:0;' align=left><img src=\"user.png\"><i><b>%s %s - %s</b></i>\n", $paper_row['firstname'],$paper_row['lastname'],$userNum);
    printf("<td style='border-width:0;' align=right><a href=\"conf_showPaperList.php?id=%s&start=0&nu=false\">
	<img src=\"openfolder.png\" alt=\"My Papers\"/></a><i>My Papers</i>\n", $userNum );
	 printf("<a href=\"conf_logout.php\"><img src=\"logout.png\" alt=\"Logout\"/></a><i>Logout</i>\n");
/*
    printf("<a href=\"conf_submitNewPaper.php?id=%s\">
	<img src=\"arrow.png\" alt=\"Submit a paper\"/></a><i>Submit Paper</i>\n", $usersList[$userNum ]['id']);
  */  
    printf("</table>");
    printf("</div>\n");

    printf("<div class=info_background>\n");
    printf("<p><img src=\"%s\">%s</p>\n",$icon, $message);
    printf("<div class=info>\n");
    
   printf("<table width=100%% cellspacing=0 cellpadding=0 border=0>\n");
	 printf("<form enctype=\"multipart/form-data\" action=\"conf_updatePaperInfo.php\" method=\"post\">\n");
	
  printf("<tr><td style='border-width:0;' valign=top><b>Track</b></td>\n");
	printf("<td style='border-width:0;'><select name=\"track\">\n");
	printf("<option value='-1'>Select Track</option>\n");
	while ( $a_row = mysqli_fetch_array( $tracksresult ) ) {
    ($paper_row['track']==$a_row["name"])? printf("<option selected>%s</option>\n",$a_row["name"]) : printf("<option>%s</option>\n",$a_row["name"]) ;
  }
  printf("</select>\n");
  
	printf("<tr><td style='border-width:0;' valign=top><b>Title</b></td>\n");
	printf("<td style='border-width:0;'><textarea  rows=\"3\" cols=\"60\" name=\"title\">%s</textarea>\n", $paper_row['title']);

	printf("<tr><td style='border-width:0;' valign=top><b>Authors</b></td>\n");
	printf("<td style='border-width:0;'><textarea  rows=\"3\" cols=\"60\" name=\"authors\">%s</textarea>\n", $paper_row['author']);

	printf("<tr><td style='border-width:0;' valign=top><b>Abstract</b></td>\n");
	printf("<td style='border-width:0;'><textarea  rows=\"15\" cols=\"60\" name=\"abstract\">%s</textarea>\n", $paper_row['abstract']);
	
	printf("<tr><td style='border-width:0;'>&nbsp\n");
	
  if($camera=='yes')
  {
        printf("<tr><td style='border-width:0;' colspan=2><input type = \"checkbox\" name =\"att\" checked>\n");
		
        printf("<i><b>Upload Camera Ready Version</b></i></td>\n");
	printf("<tr><td style='border-width:0;'><td style='border-width:0;'><input name= \"uploadedfile\" type=\"file\" size=\"70\" />\n");
        printf("<tr><td style='border-width:0;'><td style='border-width:0;'><b><u>Note</u>: Uploaded files have to be in <u>PDF format</u> and filenames cannot contain spaces.</b> Please refer to <a href=\"http://embs.papercept.net/conferences/support/word.php#wordtemplates\"> Paper template </a> in preparing your submmission.\n");
  }
	printf("<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"20000000\" />\n");

	printf("<input type=\"hidden\" value=\"%s\" name=\"id\" />\n", $userNum);
	printf("<input type=\"hidden\" value=\"%s\" name=\"pid\" />\n", $paperNum);
    
	printf("<tr><td style='border-width:0;'>&nbsp\n");
    	printf("<tr><td style='border-width:0;'><input type=\"submit\" value=\"Update Paper\" />\n");
	printf("</form>\n");

	//cancel button
        printf("<td style='border-width:0;'><form enctype=\"multipart/form-data\" action=\"conf_showPaperList.php\" method=\"get\">\n");
	printf("<input type=\"hidden\" value=\"%s\" name=\"id\" />\n", $userNum);
	printf("<input type=\"hidden\" value=\"0\" name=\"start\" />\n");
	printf("<input type=\"hidden\" value=\"false\" name=\"nu\" />\n");
        printf("<input type=\"submit\" value=\"Cancel\" />\n");
	printf("</form>\n");
	
        printf("</table>\n");

  printf("</div>\n");
   printf("</div>\n");

printf("</table>\n");


 }



function printDeletePaperPage( $userNum, $paperNum, $message, $icon)
{
	if(!isset($_SESSION['id']))
  {
    $str = sprintf("Location: submit.html");
		header($str);
  }
	printUpperBanner();
	printDeletePaperForm( $userNum, $paperNum, $message, $icon);
	printFooter();
}



function printDeletePaperForm( $userNum, $paperNum, $message, $icon)
{

  include("conn.php");
  $userresult=mysqli_query($link, "SELECT * from user where id=$userNum ");
	$user_row = mysqli_fetch_array( $userresult );
  
  $paperresult=mysqli_query($link, "SELECT paper.*,user.firstname,user.lastname from paper,user where paper.authorID=user.id and paper.id=$paperNum ");
	$paper_row = mysqli_fetch_array( $paperresult );

	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
        printf("<tr><td style='border-width:0;'><P>&nbsp;</P>\n");
		printf(" <tr><td style='border-width:0;'>\n");

    printf("<br><div class=title>\n");
    printf("<table width =100%%>\n");	
    //printf("<td><H1>CIBEC 2010 Paper Submission</H1>\n");
    printf("<td align=left style='border-width:0;'><img src=\"user.png\"><i><b>%s %s - %s</b><i>\n", $user_row['firstname'], $user_row['lastname'],$userNum);
    printf("<td align=right style='border-width:0;'><a href=\"conf_showPaperList.php?id=%s&start=0&nu=false\">
	<img src=\"openfolder.png\" alt=\"My Papers\"/></a><i>My papers</i>\n", $userNum );
    printf("<a href=\"conf_submitNewPaper.php?id=%s\">
	<img src=\"arrow.png\" alt=\"Submit a paper\"/></a><i>Submit Paper</i>\n", $userNum );
	 printf("<a href=\"conf_logout.php\"><img src=\"logout.png\" alt=\"Logout\"/></a><i>Logout</i>\n");
    
    printf("</table>");

    printf("</div>\n");

    
   
    printf("<div class=info_background>\n");
    printf("<p>\n");
    printf("<table width = 100%%>\n");
    printf("<td style='border-width:0;'><img src=\"%s\">%s</p>\n",$icon, $message);
    printf("</table>\n");


    printf("<div class=info>\n");
    printf("<table width=100%%>\n");
	
	printf("<tr><td align=right style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i><b>Status:</b> %s</i></td>\n", $paper_row['status']);

	
	
	printf("<tr><td style='border-width:0;'>&nbsp\n");
	printf("<tr><td align=center style='border-width:0;'><FONT SIZE=\"4\" FACE=\"Verdana, Arial\" ><b>%s</b></td>\n", $paper_row['title']);
	printf("<tr><td align=center style='border-width:0;'><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i>By: %s</i></td>\n", $paper_row['author']);

	printf("<tr><td style='border-width:0;'>&nbsp\n");
	printf("<tr><td style='border-width:0;'><a href=\"papers/%s\"><img src=\"pdf.gif\"></a><i> Full Paper</i>\n", $paper_row['attachment']);
	
	
	printf("<tr><td style='border-width:0;'>&nbsp\n");
	printf("<tr><td style='border-width:0;'><b><u>Abstract</u></b></td>\n");
	printf("<p><tr><td style='border-width:0;'>%s</td>\n", $paper_row['abstract']);
	
	
		
	printf("</table>\n");
   printf("</div>\n");
   printf("</div>\n");

  printf("<table>\n");
	 printf("<form enctype=\"multipart/form-data\" action=\"conf_deletePaper.php\" method=\"post\">\n");
	printf("<input type=\"hidden\" value=\"%s\" name=\"id\" />\n", $userNum);
	printf("<input type=\"hidden\" value=\"%s\" name=\"pid\" />\n", $paperNum);
    
	printf("<tr><td style='border-width:0;'>&nbsp\n");
	printf("<tr><td colspan=2 style='border-width:0;'><i><b>Delete submission, are you sure?</b></i></td>\n");
    	printf("<tr><td style='border-width:0;'><input type=\"submit\" value=\"Confirm\" />\n");
	printf("</form>\n");

	//cancel button
        printf("<td style='border-width:0;'><form enctype=\"multipart/form-data\" action=\"conf_showPaperList.php\" method=\"get\">\n");
	printf("<input type=\"hidden\" value=\"%s\" name=\"id\" />\n", $userNum);
	printf("<input type=\"hidden\" value=\"0\" name=\"start\" />\n");
	printf("<input type=\"hidden\" value=\"false\" name=\"nu\" />\n");
        printf("<input type=\"submit\" value=\"Cancel\" />\n");
	printf("</form>\n");
	
        printf("</table>\n");

printf("</table>\n");



 
}

function sendEmail($user, $subject, $message)
{
     	$bcc ="publication@cibec2012.org";

     	$headers = "From: publication@cibec2012.org\r\n";
	$headers .= "Reply-To: publication@cibec2012.org\r\n";
	$headers .= "Return-Path: publication@cibec2012.org\r\n";
        $headers .= "Bcc: ".$bcc."\r\n";
	
      	$headers .= "Content-type: text/html\r\n"; 


	
         	$to = $user['email'];
        
         
        	if (mail($to,$subject,$message,$headers) ) {
	   //echo "email sent";
	} else {
	   //echo "email could not be sent";
	}
}
?>