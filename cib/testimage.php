<?

$docRoot = $_SERVER['DOCUMENT_ROOT']; 

$outfilename = "mytest.jpg";
//$infilename = "image010.png";
$infilename = "scan.jpg";


$query = strtolower($_GET['query']);
if(!isset($_GET['id']))
{
  $id = rand();
} 
else
{
  $id = $_GET['id'];
}

$oldfilename = "mytest".$id.".jpg";
unlink($oldfilename);

$id = $id + 11;
$outfilename = "mytest".$id.".jpg";




//unlink($outfilename);
highlightImage($query, $infilename, $docRoot."/".$outfilename, &$numResults);
chmod($outfilename, 766);


printPage($query, $outfilename, $id, $numResults);







function printPage($query, $outfilename, $id, $numResults)
{
   
$text = "Descreening using Segmentation-Based Adaptive Filtering <br><br> Mohamed N. Ahmed and Ahmed H. Eid <br> Lexmark International Inc. Lexington, KY, USA <br><br> ABSTRACT <br>

Ordered halftone patterns in the original document interact with the
periodic sampling of the scanner, producing objectionable moire patterns.
These are exacerbated when the copy is reprinted with an ordered halftone pattern. Although crude, a simple, small low-pass filter 
can be used to
descreen the image and to correct the majority of moire artifacts, low-pass filtering affects detail as well, blurring it even further. In this paper, we present a new segmentation-based descreening technique. Scanned images 
are segmented into text, images and halftone classes using a multiresolution classification of intensity and edge features. The 
segmentation results guide a nonlinear, adaptive filter to favor sharpening or blurring of image pixels belonging to different classes.
Our experimental results show the ability of the non-linear, segmentation driven filter of successfully descreening halftone areas while sharpening small size text contents.<br><br> INTRODUCTION <br>

Digital copying, in which a digital image is obtained from a scanning device
and then printed, involves a variety of inherent factors that compromise image
quality.  Ordered halftone patterns in the original document interact with the
periodic sampling of the scanner, producing objectionable moire patterns.
These are exacerbated when the copy is reprinted with an ordered halftone
pattern.  In addition, limited scan resolution blurs edges, degrading the
appearance of detail such as text.  Fine detail also suffers from flare,
caused by the reflection and scattering of light from the scanner's
illumination source.  Flare blends together nearby colors, blurring the
high-frequency content of the document.<br><br>

To suppress moir\'{e}, a filter may be constructed that is customized to the
frequencies of interest.  However, both the detection of the input halftone
frequencies and the frequency-domain filtering itself can require 
significant
computational effort.  Although crude, a simple, small low-pass filter 
can be used to
descreen the image and to correct the majority of moire artifacts, low-pass filtering affects detail as well, blurring it even further.<br><br>



Sharpening improves the appearance of text and fine detail, countering the
effects of limited scan resolution and flare.  Edges become clear and
distinct. Of course, other artifacts such as noise and moire become sharper
as well. Adaptive nonlinear filtering based on image features such as the 
magnitude and the direction of image gradient can also be used. However, 
non careful tuning of such filters could either cause
 damage to small details while descreening the halftone areas, or result 
in less descreening while
 sharpening small details.<br><br>

From the above discussion, we can conclude that for an image resizing system 
to work properly, a preprocessing step should include a segmentation
of the document into text, halftone and background. If this step is successfully completed, 
the application of an appropriate filter should be straightforward. <br><br>

Several approaches for document segmentation have been proposed.
These techniques can be broadly classified as bottom-up or top-down. Bottom-up methods 
start from the pixel level and merge regions together into larger and larger components. 
Top-down techniques apply a priori knowledge about the page to hypothesize and 
split the page into blocks which are subsequently identified and further subdivided. 
Top-down approaches work well with pre-specified layouts such as technical papers. However, 
the performance of these techniques degrades significantly when different components 
are touching or overlapping. Among bottom-up approaches, texture-based schemes have 
attracted much attention.";


$arabic = "Descreening &#1576;&#1607; &#1575;&#1604;&#1573;&#1606;&#1602;&#1587;&#1575;&#1605; &#1608;&#1576;&#1606;&#1575;&#1569; &#1593;&#1604;&#1609; &#1575;&#1604;&#1578;&#1603;&#1610;&#1601; &#1578;&#1589;&#1601;&#1610;&#1577;
<br><br>
&#1605;&#1581;&#1605;&#1583; &#1606;&#1575;&#1589;&#1585; &#1571;&#1581;&#1605;&#1583; &#1608;&#1571;&#1581;&#1605;&#1583; &#1581;&#1587;&#1606; &#1593;&#1610;&#1583;
&#1604;&#1603;&#1587;&#1605;&#1575;&#1585;&#1603; &#1575;&#1606;&#1578;&#1585;&#1606;&#1575;&#1588;&#1610;&#1608;&#1606;&#1575;&#1604; &#1604;&#1610;&#1603;&#1587;&#1610;&#1606;&#1594;&#1578;&#1608;&#1606; &#1548; &#1603;&#1606;&#1578;&#1575;&#1603;&#1610; &#1548; &#1575;&#1604;&#1608;&#1604;&#1575;&#1610;&#1575;&#1578; &#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1577; &#1575;&#1604;&#1571;&#1605;&#1585;&#1610;&#1603;&#1610;&#1577;
<br><br>
&#1575;&#1604;&#1605;&#1604;&#1582;&#1589;
&#1601;&#1610; &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1585;&#1602;&#1577; &#1548; &#1606;&#1602;&#1583;&#1605; &#1578;&#1602;&#1606;&#1610;&#1577; &#1580;&#1583;&#1610;&#1583;&#1577; &#1578;&#1580;&#1586;&#1574;&#1577; descreening &#1571;&#1587;&#1575;&#1587;. &#1575;&#1604;&#1589;&#1608;&#1585; &#1575;&#1604;&#1605;&#1605;&#1587;&#1608;&#1581;&#1577; &#1590;&#1608;&#1574;&#1610;&#1575; &#1608;&#1605;&#1580;&#1586;&#1571;&#1577; &#1601;&#1610; &#1575;&#1604;&#1589;&#1608;&#1585; &#1608;&#1575;&#1604;&#1606;&#1589;&#1608;&#1589; &#1608;&#1575;&#1604;&#1571;&#1604;&#1608;&#1575;&#1606; &#1575;&#1604;&#1606;&#1589;&#1601;&#1610;&#1577; &#1575;&#1604;&#1591;&#1576;&#1602;&#1575;&#1578; &#1576;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605; &#1578;&#1589;&#1606;&#1610;&#1601; multiresolution &#1605;&#1606; &#1575;&#1604;&#1605;&#1610;&#1586;&#1575;&#1578; &#1608;&#1588;&#1583;&#1577; &#1575;&#1604;&#1581;&#1575;&#1601;&#1577;. &#1575;&#1604;&#1606;&#1578;&#1575;&#1574;&#1580; &#1583;&#1604;&#1610;&#1604; &#1593;&#1604;&#1609; &#1578;&#1580;&#1586;&#1574;&#1577; &#1594;&#1610;&#1585; &#1575;&#1604;&#1582;&#1591;&#1610;&#1577; &#1548; &#1593;&#1604;&#1609; &#1575;&#1604;&#1578;&#1603;&#1610;&#1601; &#1604;&#1589;&#1575;&#1604;&#1581; &#1605;&#1585;&#1588;&#1581; &#1588;&#1581;&#1584; &#1571;&#1608; &#1593;&#1583;&#1605; &#1608;&#1590;&#1608;&#1581; &#1589;&#1608;&#1585;&#1577; &#1576;&#1603;&#1587;&#1604; &#1575;&#1604;&#1584;&#1610;&#1606; &#1610;&#1606;&#1578;&#1605;&#1608;&#1606; &#1573;&#1604;&#1609; &#1601;&#1574;&#1575;&#1578; &#1605;&#1582;&#1578;&#1604;&#1601;&#1577;. &#1606;&#1578;&#1575;&#1574;&#1580;&#1606;&#1575; &#1578;&#1592;&#1607;&#1585; &#1575;&#1604;&#1578;&#1580;&#1585;&#1610;&#1576;&#1610;&#1577; &#1602;&#1583;&#1585;&#1577; &#1578;&#1580;&#1586;&#1574;&#1577; &#1548; &#1608;&#1594;&#1610;&#1585; &#1605;&#1583;&#1601;&#1608;&#1593;&#1577; &#1578;&#1589;&#1601;&#1610;&#1577; &#1582;&#1591;&#1610; &#1605;&#1606; descreening &#1576;&#1606;&#1580;&#1575;&#1581; &#1575;&#1604;&#1605;&#1606;&#1575;&#1591;&#1602; &#1575;&#1604;&#1571;&#1604;&#1608;&#1575;&#1606; &#1575;&#1604;&#1606;&#1589;&#1601;&#1610;&#1577; &#1601;&#1610; &#1581;&#1610;&#1606; &#1588;&#1581;&#1584; &#1575;&#1604;&#1589;&#1594;&#1610;&#1585;&#1577; &#1605;&#1581;&#1578;&#1608;&#1610;&#1575;&#1578; &#1581;&#1580;&#1605; &#1575;&#1604;&#1606;&#1589;.
<br><br>
&#1605;&#1602;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1606;&#1587;&#1582; &#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1577; &#1548; &#1608;&#1575;&#1604;&#1578;&#1610; &#1610;&#1578;&#1605; &#1575;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609; &#1575;&#1604;&#1589;&#1608;&#1585; &#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1577; &#1605;&#1606; &#1580;&#1607;&#1575;&#1586; &#1575;&#1604;&#1605;&#1587;&#1581; &#1575;&#1604;&#1590;&#1608;&#1574;&#1610; &#1608;&#1591;&#1576;&#1593; &#1576;&#1593;&#1583; &#1584;&#1604;&#1603; &#1548; &#1610;&#1606;&#1591;&#1608;&#1610; &#1593;&#1604;&#1609; &#1605;&#1580;&#1605;&#1608;&#1593;&#1577; &#1605;&#1578;&#1606;&#1608;&#1593;&#1577; &#1605;&#1606; &#1575;&#1604;&#1593;&#1608;&#1575;&#1605;&#1604; &#1575;&#1604;&#1603;&#1575;&#1605;&#1606;&#1577; &#1575;&#1604;&#1578;&#1610; &#1580;&#1608;&#1583;&#1577; &#1575;&#1604;&#1589;&#1608;&#1585;&#1577; &#1575;&#1604;&#1578;&#1587;&#1608;&#1610;&#1577;. &#1571;&#1606;&#1605;&#1575;&#1591; &#1575;&#1604;&#1571;&#1604;&#1608;&#1575;&#1606; &#1575;&#1604;&#1606;&#1589;&#1601;&#1610;&#1577; &#1605;&#1591;&#1604;&#1608;&#1576; &#1601;&#1610; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1575;&#1604;&#1571;&#1589;&#1604;&#1610;&#1577; &#1575;&#1604;&#1578;&#1601;&#1575;&#1593;&#1604; &#1605;&#1593; &#1571;&#1582;&#1584; &#1575;&#1604;&#1593;&#1610;&#1606;&#1575;&#1578; &#1575;&#1604;&#1583;&#1608;&#1585;&#1610;&#1577; &#1605;&#1606; &#1575;&#1604;&#1605;&#1575;&#1587;&#1581; &#1575;&#1604;&#1590;&#1608;&#1574;&#1610; &#1548; &#1608;&#1575;&#1606;&#1578;&#1575;&#1580; &#1605;&#1608;&#1610;&#1585; &#1575;&#1593;&#1578;&#1585;&#1575;&#1590; \ '{&#1607;} &#1571;&#1606;&#1605;&#1575;&#1591;. &#1608;&#1578;&#1578;&#1601;&#1575;&#1602;&#1605; &#1607;&#1584;&#1607; &#1593;&#1606;&#1583;&#1605;&#1575; &#1610;&#1578;&#1605; &#1591;&#1576;&#1593; &#1606;&#1587;&#1582;&#1577; &#1608;&#1606;&#1605;&#1591; &#1575;&#1604;&#1571;&#1604;&#1608;&#1575;&#1606; &#1575;&#1604;&#1606;&#1589;&#1601;&#1610;&#1577; &#1571;&#1605;&#1585;. &#1608;&#1576;&#1575;&#1604;&#1573;&#1590;&#1575;&#1601;&#1577; &#1573;&#1604;&#1609; &#1584;&#1604;&#1603; &#1548; &#1578;&#1601;&#1581;&#1589; &#1605;&#1581;&#1583;&#1608;&#1583;&#1577; &#1575;&#1604;&#1602;&#1585;&#1575;&#1585; &#1610;&#1591;&#1605;&#1587; &#1575;&#1604;&#1581;&#1608;&#1575;&#1601; &#1548; &#1575;&#1604;&#1605;&#1607;&#1610;&#1606;&#1577; &#1605;&#1592;&#1607;&#1585; &#1605;&#1606; &#1575;&#1604;&#1578;&#1601;&#1575;&#1589;&#1610;&#1604; &#1605;&#1579;&#1604; &#1575;&#1604;&#1606;&#1589;. &#1594;&#1585;&#1575;&#1605;&#1577; &#1576;&#1575;&#1604;&#1578;&#1601;&#1589;&#1610;&#1604; &#1578;&#1593;&#1575;&#1606;&#1610; &#1571;&#1610;&#1590;&#1575; &#1605;&#1606; &#1605;&#1590;&#1610;&#1574;&#1577; &#1548; &#1576;&#1587;&#1576;&#1576; &#1575;&#1606;&#1593;&#1603;&#1575;&#1587; &#1608;&#1578;&#1588;&#1578;&#1578; &#1575;&#1604;&#1590;&#1608;&#1569; &#1605;&#1606; &#1605;&#1589;&#1583;&#1585; &#1575;&#1604;&#1573;&#1590;&#1575;&#1569;&#1577; &#1575;&#1604;&#1605;&#1575;&#1587;&#1581; &#1575;&#1604;&#1590;&#1608;&#1574;&#1610;. &#1610;&#1605;&#1586;&#1580; &#1576;&#1610;&#1606; &#1578;&#1608;&#1607;&#1580; &#1575;&#1604;&#1571;&#1604;&#1608;&#1575;&#1606; &#1575;&#1604;&#1602;&#1585;&#1610;&#1576;&#1577; &#1605;&#1593;&#1575; &#1548; &#1605;&#1605;&#1575; &#1610;&#1591;&#1605;&#1587; &#1575;&#1604;&#1578;&#1585;&#1583;&#1583;&#1575;&#1578; &#1575;&#1604;&#1593;&#1575;&#1604;&#1610;&#1577; &#1575;&#1604;&#1605;&#1581;&#1578;&#1608;&#1609; &#1605;&#1606; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.
<br><br>
&#1604;&#1602;&#1605;&#1593; &#1605;&#1608;&#1610;&#1585; \ '{&#1607;} &#1548; &#1605;&#1585;&#1588;&#1581; &#1602;&#1583; &#1610;&#1603;&#1608;&#1606; &#1588;&#1610;&#1583;&#1578; &#1575;&#1604;&#1578;&#1610; &#1610;&#1578;&#1605; &#1578;&#1582;&#1589;&#1610;&#1589;&#1607;&#1575; &#1604;&#1604;&#1578;&#1585;&#1583;&#1583;&#1575;&#1578; &#1605;&#1606; &#1575;&#1604;&#1601;&#1575;&#1574;&#1583;&#1577;. &#1608;&#1605;&#1593; &#1584;&#1604;&#1603; &#1548; &#1610;&#1605;&#1603;&#1606; &#1575;&#1604;&#1603;&#1588;&#1601; &#1593;&#1606; &#1603;&#1604; &#1575;&#1604;&#1578;&#1585;&#1583;&#1583;&#1575;&#1578; &#1606;&#1589;&#1601;&#1610;&#1577; &#1575;&#1604;&#1573;&#1583;&#1582;&#1575;&#1604; &#1608;&#1606;&#1591;&#1575;&#1602; &#1578;&#1585;&#1583;&#1583; &#1578;&#1585;&#1588;&#1610;&#1581; &#1606;&#1601;&#1587;&#1607; &#1578;&#1578;&#1591;&#1604;&#1576; &#1580;&#1607;&#1583;&#1575; &#1603;&#1576;&#1610;&#1585;&#1575; &#1575;&#1604;&#1581;&#1575;&#1587;&#1608;&#1576;&#1610;&#1577;. &#1608;&#1593;&#1604;&#1609; &#1575;&#1604;&#1585;&#1594;&#1605; &#1605;&#1606; &#1575;&#1604;&#1582;&#1575;&#1605; &#1548; &#1608;&#1607;&#1610; &#1589;&#1594;&#1610;&#1585;&#1577; &#1608;&#1576;&#1587;&#1610;&#1591;&#1577; &#1605;&#1585;&#1588;&#1581; &#1575;&#1604;&#1605;&#1585;&#1608;&#1585; &#1575;&#1604;&#1605;&#1606;&#1582;&#1601;&#1590; &#1610;&#1605;&#1603;&#1606; &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1607;&#1575; &#1604;&#1573;&#1586;&#1575;&#1604;&#1577; &#1575;&#1604;&#1578;&#1605;&#1608;&#1580; &#1575;&#1604;&#1589;&#1608;&#1585;&#1577; &#1608;&#1578;&#1589;&#1581;&#1610;&#1581; &#1594;&#1575;&#1604;&#1576;&#1610;&#1577; &#1605;&#1608;&#1610;&#1585; \ '{&#1607;} &#1575;&#1604;&#1578;&#1581;&#1601; &#1548; &#1608;&#1575;&#1606;&#1582;&#1601;&#1575;&#1590; &#1578;&#1605;&#1585;&#1610;&#1585; &#1578;&#1589;&#1601;&#1610;&#1577; &#1610;&#1572;&#1579;&#1585; &#1575;&#1604;&#1578;&#1601;&#1575;&#1589;&#1610;&#1604; &#1603;&#1584;&#1604;&#1603; &#1548; &#1593;&#1583;&#1605; &#1608;&#1590;&#1608;&#1581; &#1571;&#1606;&#1607;&#1575; &#1571;&#1576;&#1593;&#1583; &#1605;&#1606; &#1584;&#1604;&#1603;.
<br><br>
&#1588;&#1581;&#1584; &#1610;&#1581;&#1587;&#1606; &#1605;&#1592;&#1607;&#1585; &#1575;&#1604;&#1606;&#1589; &#1608;&#1578;&#1601;&#1575;&#1589;&#1610;&#1604;&#1607; &#1548; &#1608;&#1605;&#1608;&#1575;&#1580;&#1607;&#1577; &#1570;&#1579;&#1575;&#1585; &#1602;&#1585;&#1575;&#1585; &#1605;&#1587;&#1581; &#1605;&#1581;&#1583;&#1608;&#1583;&#1577; &#1608;&#1605;&#1590;&#1610;&#1574;&#1577;. &#1575;&#1604;&#1581;&#1608;&#1575;&#1601; &#1578;&#1589;&#1576;&#1581; &#1608;&#1575;&#1590;&#1581;&#1577; &#1608;&#1605;&#1605;&#1610;&#1586;&#1577;. &#1576;&#1591;&#1576;&#1610;&#1593;&#1577; &#1575;&#1604;&#1581;&#1575;&#1604; &#1548; &#1608;&#1594;&#1610;&#1585;&#1607;&#1575; &#1605;&#1606; &#1575;&#1604;&#1571;&#1593;&#1605;&#1575;&#1604; &#1575;&#1604;&#1601;&#1606;&#1610;&#1577; &#1605;&#1579;&#1604; &#1575;&#1604;&#1590;&#1608;&#1590;&#1575;&#1569; &#1608;&#1605;&#1608;&#1610;&#1585; \ '{&#1607;} &#1578;&#1589;&#1576;&#1581; &#1571;&#1603;&#1579;&#1585; &#1608;&#1590;&#1608;&#1581;&#1575; &#1571;&#1610;&#1590;&#1575;. &#1578;&#1589;&#1601;&#1610;&#1577; &#1610;&#1587;&#1578;&#1606;&#1583; &#1593;&#1604;&#1609; &#1575;&#1604;&#1578;&#1603;&#1610;&#1601; &#1575;&#1604;&#1582;&#1591;&#1610;&#1577; &#1593;&#1604;&#1609; &#1605;&#1604;&#1575;&#1605;&#1581; &#1575;&#1604;&#1589;&#1608;&#1585;&#1577; &#1605;&#1579;&#1604; &#1581;&#1580;&#1605; &#1608;&#1575;&#1578;&#1580;&#1575;&#1607; &#1575;&#1604;&#1575;&#1606;&#1581;&#1583;&#1575;&#1585; &#1589;&#1608;&#1585;&#1577; &#1610;&#1605;&#1603;&#1606; &#1571;&#1606; &#1578;&#1587;&#1578;&#1582;&#1583;&#1605; &#1571;&#1610;&#1590;&#1575;. &#1608;&#1605;&#1593; &#1584;&#1604;&#1603; &#1548; &#1610;&#1605;&#1603;&#1606; &#1590;&#1576;&#1591; &#1583;&#1602;&#1610;&#1602; &#1593;&#1583;&#1605; &#1578;&#1587;&#1576;&#1576; &#1607;&#1584;&#1607; &#1575;&#1604;&#1601;&#1604;&#1575;&#1578;&#1585; &#1573;&#1605;&#1575; &#1575;&#1604;&#1571;&#1590;&#1585;&#1575;&#1585; &#1575;&#1604;&#1578;&#1610; &#1604;&#1581;&#1602;&#1578; &#1575;&#1604;&#1578;&#1601;&#1575;&#1589;&#1610;&#1604; &#1575;&#1604;&#1589;&#1594;&#1610;&#1585;&#1577; &#1601;&#1610; &#1581;&#1610;&#1606; descreening &#1575;&#1604;&#1605;&#1606;&#1575;&#1591;&#1602; &#1606;&#1589;&#1601;&#1610;&#1577; &#1548; &#1571;&#1608; &#1601;&#1610; &#1571;&#1602;&#1604; &#1606;&#1578;&#1610;&#1580;&#1577; descreening &#1601;&#1610; &#1581;&#1610;&#1606; &#1588;&#1581;&#1584; &#1575;&#1604;&#1578;&#1601;&#1575;&#1589;&#1610;&#1604; &#1575;&#1604;&#1589;&#1594;&#1610;&#1585;&#1577;.
<br><br>
&#1605;&#1606; &#1575;&#1604;&#1605;&#1606;&#1575;&#1602;&#1588;&#1577; &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1571;&#1593;&#1604;&#1575;&#1607; &#1548; &#1610;&#1605;&#1603;&#1606;&#1606;&#1575; &#1571;&#1606; &#1606;&#1587;&#1578;&#1606;&#1578;&#1580; &#1571;&#1606; &#1604;&#1606;&#1592;&#1575;&#1605; &#1578;&#1594;&#1610;&#1610;&#1585; &#1581;&#1580;&#1605; &#1575;&#1604;&#1589;&#1608;&#1585; &#1604;&#1604;&#1593;&#1605;&#1604; &#1576;&#1588;&#1603;&#1604; &#1589;&#1581;&#1610;&#1581; &#1548; &#1608;&#1607;&#1610; &#1582;&#1591;&#1608;&#1577; &#1578;&#1580;&#1607;&#1610;&#1586;&#1607;&#1575; &#1610;&#1606;&#1576;&#1594;&#1610; &#1571;&#1606; &#1610;&#1578;&#1590;&#1605;&#1606; &#1578;&#1602;&#1587;&#1610;&#1605; &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1573;&#1604;&#1609; &#1575;&#1604;&#1571;&#1604;&#1608;&#1575;&#1606; &#1575;&#1604;&#1606;&#1589;&#1601;&#1610;&#1577; &#1548; &#1608;&#1575;&#1604;&#1606;&#1589; &#1608;&#1575;&#1604;&#1582;&#1604;&#1601;&#1610;&#1577;. &#1573;&#1584;&#1575; &#1578;&#1605; &#1576;&#1606;&#1580;&#1575;&#1581; &#1607;&#1584;&#1607; &#1575;&#1604;&#1582;&#1591;&#1608;&#1577; &#1548; &#1610;&#1580;&#1576; &#1578;&#1591;&#1576;&#1610;&#1602; &#1593;&#1575;&#1605;&#1604; &#1578;&#1589;&#1601;&#1610;&#1577; &#1575;&#1604;&#1605;&#1606;&#1575;&#1587;&#1576;&#1577; &#1578;&#1603;&#1608;&#1606; &#1608;&#1575;&#1590;&#1581;&#1577;.
<br><br>
&#1608;&#1602;&#1583; &#1575;&#1602;&#1578;&#1585;&#1581;&#1578; &#1593;&#1583;&#1577; &#1571;&#1587;&#1575;&#1604;&#1610;&#1576; &#1604;&#1578;&#1602;&#1587;&#1610;&#1605; &#1575;&#1604;&#1605;&#1587;&#1578;&#1606;&#1583;. &#1608;&#1610;&#1605;&#1603;&#1606; &#1607;&#1584;&#1607; &#1575;&#1604;&#1578;&#1602;&#1606;&#1610;&#1575;&#1578; &#1593;&#1604;&#1609; &#1606;&#1591;&#1575;&#1602; &#1608;&#1575;&#1587;&#1593; &#1603;&#1605;&#1575; &#1589;&#1606;&#1601;&#1578; &#1605;&#1606; &#1571;&#1587;&#1601;&#1604; &#1573;&#1604;&#1609; &#1571;&#1593;&#1604;&#1609; &#1571;&#1608; &#1605;&#1606; &#1571;&#1593;&#1604;&#1609; &#1573;&#1604;&#1609; &#1571;&#1587;&#1601;&#1604;. &#1591;&#1585;&#1602; &#1576;&#1583;&#1569; &#1605;&#1606; &#1571;&#1587;&#1601;&#1604; &#1573;&#1604;&#1609; &#1571;&#1593;&#1604;&#1609; &#1605;&#1606; &#1605;&#1587;&#1578;&#1608;&#1609; &#1576;&#1603;&#1587;&#1604; &#1608;&#1583;&#1605;&#1580; &#1575;&#1604;&#1605;&#1606;&#1575;&#1591;&#1602; &#1575;&#1604;&#1605;&#1603;&#1608;&#1606;&#1575;&#1578; &#1605;&#1593;&#1575; &#1601;&#1610; &#1571;&#1603;&#1576;&#1585; &#1608;&#1571;&#1603;&#1576;&#1585;. &#1578;&#1602;&#1606;&#1610;&#1575;&#1578; &#1571;&#1593;&#1604;&#1609; &#1604;&#1571;&#1587;&#1601;&#1604; &#1578;&#1591;&#1576;&#1610;&#1602; &#1575;&#1604;&#1605;&#1593;&#1585;&#1601;&#1577; &#1575;&#1604;&#1605;&#1587;&#1576;&#1602;&#1577; &#1593;&#1606; &#1575;&#1604;&#1589;&#1601;&#1581;&#1577; &#1573;&#1604;&#1609; &#1575;&#1601;&#1578;&#1585;&#1575;&#1590; &#1608;&#1578;&#1602;&#1587;&#1610;&#1605; &#1589;&#1601;&#1581;&#1577; &#1573;&#1604;&#1609; &#1603;&#1578;&#1604; &#1575;&#1604;&#1578;&#1610; &#1578;&#1605; &#1578;&#1581;&#1583;&#1610;&#1583;&#1607;&#1575; &#1601;&#1610; &#1608;&#1602;&#1578; &#1604;&#1575;&#1581;&#1602; &#1608;&#1576;&#1593;&#1583; &#1584;&#1604;&#1603; &#1602;&#1587;&#1605;&#1578;. &#1575;&#1604;&#1606;&#1607;&#1580; &#1605;&#1606; &#1571;&#1593;&#1604;&#1609; &#1604;&#1571;&#1587;&#1601;&#1604; &#1578;&#1593;&#1605;&#1604; &#1576;&#1588;&#1603;&#1604; &#1580;&#1610;&#1583; &#1605;&#1593; &#1578;&#1582;&#1591;&#1610;&#1591;&#1575;&#1578; &#1575;&#1604;&#1605;&#1581;&#1583;&#1583;&#1577; &#1605;&#1587;&#1576;&#1602;&#1575; &#1605;&#1579;&#1604; &#1575;&#1604;&#1608;&#1585;&#1602;&#1575;&#1578; &#1575;&#1604;&#1578;&#1602;&#1606;&#1610;&#1577;. &#1608;&#1605;&#1593; &#1584;&#1604;&#1603; &#1548; &#1601;&#1573;&#1606; &#1571;&#1583;&#1575;&#1569; &#1607;&#1584;&#1607; &#1575;&#1604;&#1578;&#1602;&#1606;&#1610;&#1575;&#1578; &#1576;&#1588;&#1603;&#1604; &#1605;&#1604;&#1581;&#1608;&#1592; &#1593;&#1606;&#1583;&#1605;&#1575; &#1610;&#1581;&#1591; &#1605;&#1582;&#1578;&#1604;&#1601; &#1605;&#1603;&#1608;&#1606;&#1575;&#1578; &#1607;&#1610; &#1604;&#1605;&#1587; &#1571;&#1608; &#1605;&#1578;&#1583;&#1575;&#1582;&#1604;&#1577;. &#1576;&#1610;&#1606; &#1606;&#1607;&#1580; &#1605;&#1606; &#1571;&#1587;&#1601;&#1604; &#1573;&#1604;&#1609; &#1571;&#1593;&#1604;&#1609; &#1548; &#1580;&#1584;&#1576;&#1578; &#1605;&#1588;&#1575;&#1585;&#1610;&#1593; &#1606;&#1587;&#1610;&#1580; &#1610;&#1587;&#1578;&#1606;&#1583; &#1575;&#1604;&#1603;&#1579;&#1610;&#1585; &#1605;&#1606; &#1575;&#1604;&#1575;&#1607;&#1578;&#1605;&#1575;&#1605;.
";

$french = " D�tramage en utilisant la segmentation bas�e filtrage adaptatif
<br><br>
Mohamed N. Ahmed et Ahmed H. Eid
Lexington Lexmark International Inc, KY, USA
<br><br>
R�SUM�
<br><br>
Dans cet article, nous pr�sentons une technique nouvelle segmentation bas�e d�tramage. Les images num�ris�es sont segment�s en texte, des images et des classes en demi-teinte en utilisant une classification des caract�ristiques multir�solution l'intensit� et le bord. Les r�sultats de segmentation un guide non lin�aire, filtre adaptatif pour favoriser nettet� ou le flou de pixels de l'image appartenant � diff�rentes classes. Nos r�sultats exp�rimentaux montrent la capacit� de la non lin�aires, la segmentation conduit filtre de d�tramage avec succ�s en demi-teinte domaines tout en aiguisant petite taille du texte contenu.
<br><br>
INTRODUCTION
<br><br>
La copie num�rique, dans lequel une image num�rique est obtenue � partir d'un p�riph�rique de num�risation, puis imprim�s, implique une vari�t� de facteurs inh�rents que la qualit� de l'image de compromis. motifs en demi-teinte Ordonne dans le document original interagir avec les �chantillonnages p�riodiques du scanner, la production Moir r�pr�hensible \ '{e} mod�les. Ces probl�mes sont exacerb�s lorsque la copie est reproduit avec un motif en demi-teinte command�. En outre, la num�risation limit�e brouille la r�solution bords, d�gradant l'apparition de d�tail comme du texte. Finesse des d�tails souffre �galement d'une fus�e, caus� par la r�flexion et de diffusion de la lumi�re � partir de source d'�clairage du scanner. m�langes Flare ensemble des couleurs � proximit�, ce qui brouille le contenu haute fr�quence du document.
<br><br>
Pour supprimer Moir \ '{e}, un filtre peut �tre construit qui est adapt� aux fr�quences d'int�r�t. Cependant, � la fois la d�tection des fr�quences d'entr�e en demi-teinte et le domaine fr�quentiel se filtrage peut exiger d'importants efforts de calcul. Bien brut, un simple, petit col bas filtre peut �tre utilis� pour d�tramage l'image et de corriger la majorit� des Moir \ '{e} artefacts, filtrage passe-bas affecte d�tail ainsi, il brouiller encore plus loin.
<br><br>
Aiguisage am�liore l'apparence du texte et des d�tails pr�cis, la lutte contre les effets de limiter la r�solution de num�risation et les reflets. Les bords deviennent claires et distinctes. Bien entendu, d'autres objets tels que le bruit et Moir \ '{e} deviennent plus nettes ainsi. Adaptive filtrage non lin�aire bas�e sur les caract�ristiques de l'image tels que l'ampleur et la direction du gradient de l'image peut �galement �tre utilis�. Cependant, non tuning attention de ces filtres peut soit causer des dommages aux petits d�tails alors que les zones en demi-teinte de d�tramage, ou le r�sultat en moins de d�tramage tout en aiguisant les petits d�tails.
<br><br>
De la discussion ci-dessus, nous pouvons conclure que pour un syst�me de redimensionnement d'image fonctionne correctement, une �tape de pr�traitement devrait inclure une segmentation du document en texte, en demi-teinte et le fond. Si cette �tape est termin�e avec succ�s, l'application d'un filtre appropri� devrait �tre simple.
<br><br>
Plusieurs approches pour la segmentation de documents ont �t� propos�es. Ces techniques peuvent �tre class�es comme de bas en haut ou de haut en bas. Bottom up � partir de m�thodes au niveau des pixels et une fusion des r�gions ainsi que dans les composants de plus en plus grande. Top techniques � appliquer une connaissance a priori sur la page de l'hypoth�se et de diviser la page en blocs qui sont ensuite identifi�s et subdivis�. des approches top down bien travailler avec les mises en page pr� sp�cifi�s tels que les documents techniques. Toutefois, la performance de ces techniques se d�grade de fa�on significative lorsque les diff�rentes composantes se touchent ou se chevauchent. Parmi les approches de bas en haut, les r�gimes de base de la texture ont attir� beaucoup d'attention.
";


printf("<html>\n");

   printf("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1 \"/>\n");
printf("<LINK href=\"icip.css\" type=text/css rel=stylesheet>\n");

	printf("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://yui.yahooapis.com/2.8.2r1/build/fonts/fonts-min.css\" />\n");
	printf("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://yui.yahooapis.com/2.8.2r1/build/tabview/assets/skins/sam/tabview.css\" />\n");
	printf("<script type=\"text/javascript\" src=\"http://yui.yahooapis.com/2.8.2r1/build/yahoo-dom-event/yahoo-dom-event.js\"></script>\n");
	printf("<script type=\"text/javascript\" src=\"http://yui.yahooapis.com/2.8.2r1/build/element/element-min.js\"></script>\n");
	printf("<script type=\"text/javascript\" src=\"http://yui.yahooapis.com/2.8.2r1/build/tabview/tabview-min.js\"></script>\n");
 

printf("<script>
(function() {
    var tabView = new YAHOO.widget.TabView('demo');
    var tabView1 = new YAHOO.widget.TabView('demo1');
    YAHOO.log(\"ll\", \"info\", \"example\");
})();
</script>\n");






printf("<body class=\"yui-skin-sam\">\n");

printf("<center><br>\n");
printf("<table cellspacing=10 cellpadding=0>\n");
printf("<td><table><td><img src=images.jpg>\n");
printf("<td><form METHOD=\"get\" ACTION=\"http://www.cibec2010.org/testimage.php\" name=\"application\">\n");
printf("<input type=\"text\" NAME=\"query\" SIZE=\"30\" value=\"%s\" style=\"width:500px; font-weight:bold; font-size:18\"> \n", $query);
printf("<INPUT TYPE=\"HIDDEN\" VALUE=\"%d\" name=\"id\">\n", $id);
printf("<INPUT TYPE=\"SUBMIT\" VALUE=\"Search\">\n");
printf("</form></table>\n");
   
//printf("</table>\n");


//printf("<table>\n");

printf("<tr><td>\n");
printf("<div class=search>Search Results for <b>%s</b> (%d)</div>\n", $query, $numResults);


printf("<tr><td><div class=content>\n");
printf("<table cellpadding=5 cellspacing=2 align=center>");

printf("<td valign=top bgcolor=#AAAAAA><img src=scan.jpg width=150 height=225> \n");


printf("<td valign=top>\n");
printf("<div id = \"demo1\" class=\"yui-navset\">\n");
   	printf(" <ul class=\"yui-nav\">\n");
   		printf("<li class=\"selected\"><a href=\"#tab11\">Scanned Image </a></li>\n");
		printf("<li><a href=\"#tab22\">Enhanced </a></li>\n");
   	printf(" </ul>\n");
     
printf(" <div class=yui-content>\n");
printf(" <div id=\"tab11\">\n");
printf("<img src=\"http://www.cibec2010.org/%s\">\n", $outfilename);
printf("</div>\n");


printf(" <div id=\"tab22\">\n");
printf("<img src=\"http://www.cibec2010.org/scan-enhanced.jpg\">\n");
printf("</div>\n");


printf("</div>\n");






printf("<td valign=top>\n");
   printf("<div id = \"demo\" class=\"yui-navset\">\n");
   	printf(" <ul class=\"yui-nav\">\n");
   		printf("<li class=\"selected\"><a href=\"#tab1\">English Text </a></li>\n");
		printf("<li><a href=\"#tab2\">French Text </a></li>\n");
                printf("<li><a href=\"#tab3\">Arabic Text </a></li>\n");
   	printf(" </ul>\n");
     
printf(" <div class=yui-content>\n");


//Text div
printf(" <div id=\"tab1\" class=text>\n");
$token = strtok($text, " -");
while ($token != false)
  {
  if($query == strtolower($token)) printf("<b><u><FONT COLOR=\"AA0000\">%s</FONT></u></b> ", $token);
  else printf("%s ", $token);
  $token = strtok(" -");
  } 
 printf("</div>\n");


//MT div
printf(" <div id=\"tab2\" class=mt>\n");
$token = strtok($french, " -");
while ($token != false)
  {
  if($query == strtolower($token)) printf("<b><u><FONT COLOR=\"0000AA\">%s</FONT></u></b> ", $token);
  else printf("%s ", $token);
  $token = strtok(" -");
  } 
 printf("</div>\n");


//MT div
printf(" <div id=\"tab3\" class=mt>\n");
$token = strtok($arabic, " -");
while ($token != false)
  {
  if($query == strtolower($token)) printf("<b><u><FONT COLOR=\"0000AA\">%s</FONT></u></b> ", $token);
  else printf("%s ", $token);
  $token = strtok(" -");
  } 
 printf("</div>\n");




   printf("</div>\n");
  printf("</div>\n");

   




printf("</table>\n");
printf("</table>\n");

printf("</body>\n");
printf("</html>\n");

}




function highlightImage($query, $infilename, $outfilename, $numResults)
{

$r = 255;
$g = 255;
$b = 1;
$alpha = 80;

$numResults = 0;


$canvas = @imagecreatefromjpeg($infilename); 
   

// Allocate colors
$pink = imagecolorallocate($canvas, 255, 105, 180);
$white = imagecolorallocate($canvas, 255, 255, 255);
$green = imagecolorallocate($canvas, 132, 135, 28);

$highlight =imagecolorallocatealpha($canvas, $r, $g, $b, $alpha);

imagealphablending($canvas, true);

// Draw three rectangles each with its own color



if( strcmp($query,'ahmed') == 0)
{
      //echo "<br>1)".$query;
      imagefilledrectangle($canvas, 280, 102, 320, 114, $highlight);
      imagefilledrectangle($canvas, 345, 102, 386, 114, $highlight);
      $numResults = 2;
}
else if( strcmp($query,'halftone') == 0)
{
      //echo "<br>2)".$query;
      imagefilledrectangle($canvas, 100, 168, 140, 178, $highlight);
      imagefilledrectangle($canvas, 490, 185, 530, 195, $highlight);
      imagefilledrectangle($canvas, 500, 235, 540, 245, $highlight);
      imagefilledrectangle($canvas, 455, 260, 495, 270, $highlight);
      imagefilledrectangle($canvas, 425, 295, 465, 305, $highlight);
      imagefilledrectangle($canvas, 178, 325, 217, 335, $highlight);
     
      imagefilledrectangle($canvas, 334, 386, 373, 396, $highlight);
      imagefilledrectangle($canvas, 227, 410, 265, 420, $highlight);
      imagefilledrectangle($canvas, 182, 477, 221, 487, $highlight);
      imagefilledrectangle($canvas, 335, 599, 374, 609, $highlight);

      $numResults = 10;
}



//imagerectangle($canvas, 45, 60, 120, 100, $white);
//imagerectangle($canvas, 100, 120, 75, 160, $green);

//Save image as a jpeg
imagejpeg($canvas, $outfilename);

// Output and free from memory
imagedestroy($canvas);

}

?>

