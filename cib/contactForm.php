<?php
session_start();
include("conf_common.php");
include("conn.php");

if(isset($_REQUEST["send"]))
{
	if(trim($_REQUEST["name"])!="" && trim($_REQUEST["email"])!="" && trim($_REQUEST["message"])!="")
	{
		$to =(isset($_REQUEST["to"])&& $_REQUEST["to"]!="")?$_REQUEST["to"]:"cibec@cibec2012.org";

     		$headers = "From: ".trim($_REQUEST["email"])."\r\n";
		$headers .= "Return-Path: cibec2012@cibec2012.org\r\n";
      		$headers .= "Content-type: text/html\r\n"; 
      		
      		$subject="contact from ".trim($_REQUEST["name"]);
      		$message.=$_REQUEST["message"]."<br />from:".trim($_REQUEST["name"]);

       		//In case any of our lines are larger than 70 characters        
       		$message = wordwrap($message, 70);


        	if (mail($to,$subject,$message,$headers) ) 
        	{
        	printUpperBanner(16);
	   	echo "<p style='color:green;'>email sent</p>";
	   	printFooter();
		} 
        	else 
        	{
        	printUpperBanner(16);
	   	echo "<p style='color:green;'>email could not be sent</p>";
	   	printFooter();
		}

	
	}else{
	$errorMessage = "Please send all fields.";
	header("Location: contactForm.php?errorMessage=".$errorMessage);
	
	}

}else{
printUpperBanner(16);

if(isset($_REQUEST["errorMessage"]))
print "<p style='color:red;'>$_REQUEST[errorMessage]</p>";

print '
<form name="contact" method="post" action="">
<input type="hidden" name="to" value="'.$_REQUEST["to"].'" />
<ul style="list-style-type:none;">
<li style="background-image:none;">Name: *<input name="name" value="" size="50" />
<li style="background-image:none;">E-mail: *<input name="email" value="" size="50" />
<li style="background-image:none;">Message: *
<li style="background-image:none;"><textarea name="message" cols="70" rows="15"> </textarea>
<li style="background-image:none;"><input type="submit" name="send" value="Send" />
</ul>
</form>';

printFooter();
}
?>