<?php

session_start();
	include("conf_common.php");
	
	$start 		= $_GET['start'];
	$pid 		= $_GET['pid'];
	$authorID 	= $_GET['authorID'];
	$uid 		= $_GET['uid'];

    if(!isset($_SESSION['id']))
  {
    $str = sprintf("Location: submit.html");
		header($str);
}

    printShowReviewPage($uid, $authorID, $pid, $message, $icon, $start);
	   


function printShowReviewPage($uid, $userNum, $paperNum, $message, $icon, $start)
{
	
	printUpperBanner();
	printShowPaperForm($uid, $userNum, $paperNum, $message, $icon, $start);
	printFooter();
}




function printShowPaperForm($uid, $userNum, $paperNum, $message, $icon, $start)
{
 	include("conn.php");
  $paperresult=mysqli_query($link, "SELECT paper.*,user.*,paper.title as ptitle from paper,user where paper.authorID=user.id and paper.authorID=$userNum and paper.id=$paperNum");
  $paper_row = mysqli_fetch_array( $paperresult ) ;
      
	printf(" <table width = 100%% cellscaping=0 cellpading=0>\n");
        printf("<tr><td style='border-width:0;'  ><P>&nbsp;</P>\n");
		printf(" <tr><td style='border-width:0;'  >\n");


   $paperTitle	= $paper_row['ptitle'];
   $paperAuthors	= $paper_row['author'];
   $paperStatus	= $paper_row['status'];
   $registeredAuthor = $paper_row['firstname'] . " " . $paper_row['lastname'] ;
   $track=$paper_row['track'];
    
   
    printf("<div class=info_background>\n");
    


    printf("<div class=info>\n");
    printf("<table width=100%%>\n");
	
	printf("<tr><td style='border-width:0;'   align=right><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i>Final Status: <b> %s</b></i></td>\n", $paperStatus );

	
	
	printf("<tr><td style='border-width:0;'   >&nbsp\n");
	printf("<tr><td style='border-width:0;'   align=center><FONT SIZE=\"4\" FACE=\"Verdana, Arial\" ><b>%s</b></td>\n", $paperTitle);
	printf("<tr><td style='border-width:0;'   align=center><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i>By: %s</i></td>\n", $paperAuthors);

	
	$revForm[0]['text'] = "Relevance to the conference";
   	$revForm[0]['tag'] 	= "relevance";
    $revForm[1]['text'] = "Scientific merit of contents";
    $revForm[1]['tag'] 	= "merit";
    $revForm[2]['text'] = "Clarity and novelty of methdology";
    $revForm[2]['tag'] 	= "clarity";
    $revForm[3]['text'] = "Experimental verification";
    $revForm[3]['tag'] 	= "verification";
    $revForm[4]['text'] = "Analysis of results and discussion of methods advantages/limitations";
    $revForm[4]['tag'] 	= "analysis";
    $revForm[5]['text'] = "Review of literature and comparison to previous techniques";
    $revForm[5]['tag'] 	= "literature";
   	$revForm[6]['text'] = "Potential for clinical application";
    $revForm[6]['tag'] 	= "application";
	$revForm[7]['text'] = "Paper style and English language";
    $revForm[7]['tag'] 	= "language";
	

	 printf("<form enctype=\"multipart/form-data\" action=\"conf_emailReviews.php\" method=\"post\">\n");


	
	if($paperStatus == "accepted")
	{
		$review ="<p>Dear " . $registeredAuthor .":<br>\n";
		$review .= "\n<br>Congratulations! Your paper: ";
		$review .= $paperTitle . ", " . $paperNum;
		$review .= " has been accepted for presentation at the CIBEC 2012, the 6th Cairo International Conference on Biomedical Engineering, to be held in Hilton Dreamland Hotel, 6th of October City on the outskirts of Cairo on December 20-22, 2012. This paper will be included in the proceedings of the conference provided that you implement the requested changes and prepare the Camera-Ready version on time. <br>\n";
 
                $review .= "\n<br>Please note that for final acceptance for inclusion in the program and the proceedings, at least one of the authors must register at the conference. Upon final acceptance for inclusion in the program and the proceedings, we will get in touch with you to let you know the type of the presentation (oral or poster) and the exact date and time for the presentation. <br>\n";
 
                $review .= "\n<br>Please remember that submitting the camera ready version deadline is <b>November 5</b>, 2012. Strict use of the template provided on the conference website in preparing the final pdf file is a necessary requirement. Please follow that template closely to avoid undue exclusion of your paper from the program and the proceedings. <br>\n";
 
                $review .= "\n<br>On behalf of CIBEC 2012 Program Team we look forward to your participation at the conference in Cairo.<br>\n";
                
                $review.="Best regards,<br>\n";
                $review.=$track." Track Chair<br>\n";


                $review .= "\n<br><br>REVIEW RESULTS:<br>\n";
        $review .= "Note: Listed scores for each reviewer are out of 5.<br>\n";

 
	}
	else if($paperStatus == "rejected")
	{
		$review ="<p>Dear " . $registeredAuthor .":<br>\n";
		$review .= "\n<br>Thank you for submitting your paper ";
		$review .= $paperNum . " ( " .$paperTitle . " ) " ;
		$review .= " to CIBEC 2012. The review process is completed and we regret to inform you that your paper has not been accepted for presentation at the conference. We have received many good contributions this year and the competition was very high.<br>\n";
		$review .= "The reviewers' comments are below. These comments have served as basis for the Technical Committee's decision on your paper. We hope that they offer a useful feedback on your work.<br>\n";
		$review .= "While we recognize that the final decision is disappointing to you, we would encourage and cordially invite you to attend the conference and/or workshops, and to enter into debate and discussion with colleagues in your field of interest.<br>\n";
		$review .= "We hope to meet you in Cairo in December.<br>\n";
		$review.="Best regards,<br>\n";
                $review.=$track." Track Chair<br>\n";
                $review .= "<br>REVIEW RESULTS:<br>\n";
        $review .= "\n<br>Note: Listed scores for each reviewer are out of 5.<br><br>\n";


	}
	else
	{
		$review .= "<p>Final Decision for paper: ";
		$review .= $paperTitle;
		$review .= " is not ready yet ";
	}

	$revresult=mysqli_query($link, "SELECT *,paper.title as ptitle from rev,user,paper where rev.pid=paper.id and rev.rev=user.id and pid=$paperNum  ");
	$numReviews = mysqli_num_rows( $revresult );
	if($numReviews==0) $review .= "<br>No reviews available\n";
	else
	{
	$i=1;
  while($rev_row = mysqli_fetch_array( $revresult ))
		{

			//$review .= "\n<br><br>Reviewer ID : " . $rev_row['rev'] ."<br>";

                        $review .= "\n<br><br>Review " . $i++ .":<br>";

			
			$score = 0;
			for($ii=0; $ii < 8; $ii++)
			{
				$score += $rev_row[$revForm[$ii]['tag']];
				/*
                                $review .= "\n<br>" . $revForm[$ii]['text'] . " = " . $reviewList[$i][$revForm[$ii]['tag']] ;
*/
		     
			}

			$review .= "\n<br>Average Score: " . $score/8;
			$review .= "\n<br>Recommendation: " . $rev_row['accept'];
			$review .= "\n<br>Comments: ";
                        $review .= "\n<br>". $rev_row['comments'];
                        $review .= "\n<br>";

						
			
		}
		
	}
	printf("<tr><td style='border-width:0;'  ><textarea  rows=35 cols=90 name=\"reviews\">%s</textarea>\n", $review);
	printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $paperNum);
	printf("<input type=\"hidden\" value=\"%s\"  name=\"authorID\"/>\n", $paper_row['authorID']);
	printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);		printf("<input type=\"hidden\" value=\"%s\"  name=\"start\"/>\n", $start);		

	printf("<tr><td style='border-width:0;'  ><img src=\"upload.png\" valign=bottom ><input type=\"submit\" value=\"Email Reviews\" />\n");
	printf("</form>\n");
		
	printf("</table>\n");
   printf("</div>\n");
   printf("</div>\n");

	
printf("</table>\n");



    }

?>