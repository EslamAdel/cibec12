<?php
include("conf_readPaperReview.php");

function getReviewStatus($pid, $revID)
{
	$docRoot = $_SERVER['DOCUMENT_ROOT']; 
	$reviewsfilename = sprintf("%s/papers/%s_rev.xml", $docRoot, $pid);
	$numReviews = loadReviewList($reviewList, $reviewsfilename);
	if(isReviewCompleted($reviewList, $revID))
		$status = "Completed";
	else 
		$status = "Not completed";
 
	return $status;
}


function displayTrackChairPaperList( $uid, $start)
{
 include("conn.php");
 
  $trackresult=mysqli_query($link, "SELECT track.name as category from track where chair=$uid or cochair=$uid");
	while($track_row = mysqli_fetch_array( $trackresult )){
 $trackname=$track_row["category"];
 $paperresult=mysqli_query($link, "select * from paper where assignedtochair=1 and track = '$trackname' ");
  $numPapers = mysqli_num_rows( $paperresult );
  $end = $numPapers;
  

	

	if($numPapers==0) {
		continue;
	}
	



	//Get reviewers List


 
	
	printf("<table width=100%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'><div><table width=100%% align=center border=0 cellpading=0 cellspacing=5>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i>Track List: %s</i></td>\n", $track_row['category']);
	printf("</table></div>

\n");
			
	
   
	printf("<table width=95%% align=center border=0 cellpading=0 cellspacing=3>\n");
  $i=1;
	while($paper_row = mysqli_fetch_array( $paperresult )){

		printf("<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.background='yellow';\" onmouseout= \"this.style.background='white';\">\n");
		printf("<td style='border-width:0;'  ><table width=100%% border=0 cellpading=0 cellspacing = 1>\n");
			printf("<tr>\n");
			
			printf("<td style='border-width:0;'  >%d)</td>\n", $i++);
			printf("<td style='border-width:0;'   width=15%%><b>ID</b>: %s</td>\n", $paper_row[ 'id' ]);
			printf("<td style='border-width:0;'   width=40%%  align = center valign=top><a href=\"conf_showPaperReview.php?id=%s&pid=%s\" title=\"Show

Reviews\"><img src=\"info.png\" alt=\"Show Reviews\"/>Reviews</a></td>\n", $uid,$paper_row[ 'id' ]);



			printf("<td style='border-width:0;'  ><form action=\"conf_acceptPaper.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $paper_row[ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);
		printf("<input type=\"hidden\" value=\"accepted\"  name=\"status\"/>\n");
    		printf("<input type=\"submit\" value=\"Accept\"  />\n");
    		printf("</form>\n"); 

		 printf("<td style='border-width:0;'  ><form action=\"conf_acceptPaper.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $paper_row[ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);
		printf("<input type=\"hidden\" value=\"rejected\"  name=\"status\"/>\n");
    		printf("<input type=\"submit\" value=\"Reject\"  />\n");
    		printf("</form></td>\n"); 

		printf("<td style='border-width:0;'   colspan=2><form action=\"conf_acceptPaper.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $paper_row[ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);
		printf("<input type=\"hidden\" value=\"Not reviewed\"  name=\"status\"/>\n");
    		printf("<input type=\"submit\" value=\"Reset\"  title=\"Reset decision to 'Not reviewed' \" />\n");
    		printf("</form>\n"); 

//------------------------------------------------------------------------
//------------------------------------------------------------------------

/*printf("<td style='border-width:0;'  ><form action=\"conf_emailPaperReviewsForm.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $paper_row[ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"authorID\"/>\n", $paper_row['authorID']);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"uid\"/>\n", $uid);
    		printf("<input type=\"submit\" value=\"Email Reviews\"  title=\"Email Reviews and decision to author\"/>\n");	
    		printf("</form></td>\n"); 

*/
//-----------------------------------------------------------------------
//------------------------------------------------------------------------

		printf("<tr>\n");
		printf("<td style='border-width:0;'  >\n");
		printf("<td style='border-width:0;'   colspan=6><small><a href=\"papers/%s\" title=\"Paper ID: %s \nAbstract: %s\"> <img src=\"pdf.gif\"> %s </a></td>\n",
				$paper_row[ 'attachmentwithoutauthor' ],
				$paper_row[ 'id' ],
				$paper_row[ 'abstract' ],
				$paper_row[ 'title' ]);
			

		  $revresult=mysqli_query($link, "SELECT * from rev,user where rev.rev=user.id and pid=$paper_row[id]");
      $revno=1;
	    while($rev_row = mysqli_fetch_array( $revresult )){

$accepttoreview ="(Wait for acceptance)";	    
if($rev_row['accepttoreview']=="yes")
$accepttoreview = "(Accept to review)";
else if($rev_row['accepttoreview']=="no")
$accepttoreview ="(Reject to review)" ;

if($rev_row['done']==1)
$accepttoreview ="(Reviewed)" ;

  		printf("<tr>\n");
			printf("<td style='border-width:0;'  ></td>\n");
			if($rev_row["accept"] != null) printf("<td style='border-width:0;'   align=right><img src=\"check2.png\"></td>\n");
			else printf("<td style='border-width:0;'  ></td>\n");
			printf("<td style='border-width:0;'   colspan=1><small>Reviewer %d %s: <a href= \"mailto:%s\" title=\"Organization: %s\nCountry: %s\nClick to email Reviewer...\">%s %s</a></td>\n", $revno++,$accepttoreview ,$rev_row['email'], $rev_row['organization'], $rev_row['country'], $rev_row['firstname'], $rev_row['lastname']);

			//printf("<td style='border-width:0;'   colspan=4>Status: <i>%s</i></td>\n", $status);


		printf("<td style='border-width:0;'   colspan=4 valign=bottom>\n");
		printf("<form method=\"get\" action=\"conf_assignReviewer.php\" name=\"application\">\n");
		printf("<select  name=\"revID\">\n");
		printf("<option value=0>....Select Track Reviewer....</option>\n");
		$reviewersresult=mysqli_query($link, "SELECT * from user where category = 'rev' and revtrack='$track_row[category]' and id not in(SELECT uid from paper_blocked_reviewer where pid=$paper_row[id])  order by firstname");
    		while($reviewers_row = mysqli_fetch_array( $reviewersresult))
		{
      		$histresult=mysqli_query($link, "SELECT count(*) as cnt from rev where rev=$reviewers_row[id]");
      		$hist_row = mysqli_fetch_array( $histresult ) ;
      
			if($rev_row['rev'] == $reviewers_row['id'])
			      printf("<option value=%s selected title=\"Organization: %s \nCountry: %s \nNo of Assigned Papers: %s\">%s</option>\n", 
				$reviewers_row['id'], $reviewers_row['organization'], $reviewers_row['country'],
				$hist_row['cnt'], $reviewers_row['firstname']." ".$reviewers_row['lastname']);
			else
			  printf("<option value=%s title=\"Organization: %s \nCountry: %s \nNo of Assigned Papers: %s\">%s </option>\n",
				$reviewers_row['id'], $reviewers_row['organization'], $reviewers_row['country'],
				$hist_row['cnt'], $reviewers_row['firstname']." ".$reviewers_row['lastname']);
		}
		
		printf("<option value=0>....Select Other Reviewer....</option>\n");
		$reviewersresult=mysqli_query($link, "SELECT * from user where category not in( 'author' ,'attendee' , 'admin')  and id not in(SELECT id from user where category ='rev' and revtrack='$track_row[category]') and id not in(SELECT uid from paper_blocked_reviewer where pid=$paper_row[id]) and id<>20120911100107  order by firstname");
    		while($reviewers_row = mysqli_fetch_array( $reviewersresult))
		{
      		$histresult=mysqli_query($link, "SELECT count(*) as cnt from rev where rev=$reviewers_row[id]");
      		$hist_row = mysqli_fetch_array( $histresult ) ;
      
			if($rev_row['rev'] == $reviewers_row['id'])
			      printf("<option value=%s selected title=\"Organization: %s \nCountry: %s \nNo of Assigned Papers: %s\">%s</option>\n", 
				$reviewers_row['id'], $reviewers_row['organization'], $reviewers_row['country'],
				$hist_row['cnt'], $reviewers_row['firstname']." ".$reviewers_row['lastname']);
			else
			  printf("<option value=%s title=\"Organization: %s \nCountry: %s \nNo of Assigned Papers: %s\">%s </option>\n",
				$reviewers_row['id'], $reviewers_row['organization'], $reviewers_row['country'],
				$hist_row['cnt'], $reviewers_row['firstname']." ".$reviewers_row['lastname']);
		}
		
		printf("</select>\n");
		printf("<input type=\"hidden\" name=\"id\" value=%s> \n", $uid);
		printf("<input type=\"hidden\" name=\"pid\" value=%s> \n", $paper_row['id']);
		printf("<input type=\"hidden\" name=\"rev\" value=\"%s\"> \n", $rev_row['rev']);
		printf("<input type=\"submit\" value=\"Replace Rev.\" title =\"Select a reviewer from the list then press this button to assign paper to reviewer\"> \n");
		printf("</form>\n");
		
		printf("<form method=\"get\" action=\"conf_removeReviewer.php\" name=\"RemoveReviewer\" id=\"RemoveReviewer\">\n");
		printf("<input type=\"hidden\" name=\"id\" value=%s> \n", $uid);
		printf("<input type=\"hidden\" name=\"pid\" value=%s> \n", $paper_row['id']);
		printf("<input type=\"hidden\" name=\"rev\" value=\"%s\"> \n", $rev_row['rev']);
		printf("<input type=\"submit\" value=\"Remove Rev.\" onclick=\" if(!confirm('Are you shure deleting reviewer?')) return false; \" title =\"Select a reviewer from the list then press this button to remove reviewer\"> \n");
		printf("</form>\n");	

    }


			printf("<tr>\n");printf("<tr><td style='border-width:0;'  >\n");
			printf("<td style='border-width:0;'  ></td>\n");
			printf("<td style='border-width:0;'  ></td>\n");
			//printf("<td style='border-width:0;'  ></td>\n");

			$status = $paper_row[ 'status' ];
			if( $status == "accepted")
				printf("<td style='border-width:0;'   colspan=5 align=right>Recommendation: <FONT color=\"#00aa00\"><b> %s</b></td>\n", $status);
			else if( $status == "rejected")
				printf("<td style='border-width:0;'   colspan=5 align=right>Recommendation: <FONT color=\"#cc0000\"><b> %s</b></td>\n", $status);
			else printf("<td style='border-width:0;'   colspan=5 align=right>Recommendation: <b> %s</b></td>\n", $status);

		   	
			printf("<tr>\n");
		  printf("<td style='border-width:0;'   colspan=6>\n");
   	printf("<h4>Add Reviewer</h4>");
    printf("<form method=\"get\" action=\"conf_assignReviewer.php\" name=\"application\">\n");
		printf("<select  name=\"revID\">\n");
		printf("<option value=0>....Select Track Reviewer....</option>\n");
		$reviewersresult=mysqli_query($link, "SELECT * from user where category ='rev' and revtrack='$track_row[category]' and id not in(SELECT uid from paper_blocked_reviewer where pid=$paper_row[id]) order by firstname");
   		 while($reviewers_row = mysqli_fetch_array( $reviewersresult ))
		{
      		$histresult=mysqli_query($link, "SELECT count(*) as cnt from rev where rev=$reviewers_row[id]");
     		 $hist_row = mysqli_fetch_array( $histresult ) ;
      
  		  printf("<option value=%s title=\"Organization: %s \nCountry: %s \nNo of Assigned Papers: %s\">%s </option>\n",
				$reviewers_row['id'], $reviewers_row['organization'], $reviewers_row['country'],
				$hist_row['cnt'], $reviewers_row['firstname']." ".$reviewers_row['lastname']);
		}
		
		printf("<option value=0>....Select Other Reviewer....</option>\n");
		$reviewersresult=mysqli_query($link, "SELECT * from user where category not in( 'author' ,'attendee' , 'admin') and id not in(SELECT id from user where category ='rev' and revtrack='$track_row[category]') and id not in(SELECT uid from paper_blocked_reviewer where pid=$paper_row[id]) and id<>20120911100107  order by firstname");
   		 while($reviewers_row = mysqli_fetch_array( $reviewersresult ))
		{
      		$histresult=mysqli_query($link, "SELECT count(*) as cnt from rev where rev=$reviewers_row[id]"); 
     		 $hist_row = mysqli_fetch_array( $histresult ) ;
      
  		  printf("<option value=%s title=\"Organization: %s \nCountry: %s \nNo of Assigned Papers: %s\">%s </option>\n",
				$reviewers_row['id'], $reviewers_row['organization'], $reviewers_row['country'],
				$hist_row['cnt'], $reviewers_row['firstname']." ".$reviewers_row['lastname']);
		}
		
		printf("</select>\n");
		printf("<input type=\"hidden\" name=\"id\" value=%s> \n", $uid);
		printf("<input type=\"hidden\" name=\"pid\" value=%s> \n", $paper_row['id']);
		printf("<input type=\"hidden\" name=\"rev\" value=\"new\"> \n");
		printf("<input type=\"submit\" value=\"Assign Rev.\" title =\"Select a reviewer from the list then press this button to assign paper to reviewer\"> \n");
		printf("</form>\n");
		printf("</table>\n");

		
	}

	printf("</table>

\n");

	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'  ><div><table width=95%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"1\" FACE=\"Verdana, Arial\" >___</td>\n");
	printf("</table></div>

\n");
	
	printf("</table>\n");
}
	
	
}	





function displayReviewerPaperList($uid,  $start)
{
  include("conn.php");
  
  $paperresult=mysqli_query($link, "SELECT paper.*,rev.accepttoreview from paper,rev where paper.id=rev.pid and rev.rev=$uid and rev.accepttoreview!='no' ");
  $numPapers = mysqli_num_rows( $paperresult );
	$end = $numPapers;




	
	if($numPapers==0) {
		return;
	}


	printf("<table width=100%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'  ><div><table width=95%% align=center border=0 cellpading=0 cellspacing=5>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i>Review List: </i></td>\n");

	printf("</table></div>

\n");
			
	
   
	printf("<table width=95%% align=center border=0 cellpading=0 cellspacing=1>\n");
	printf("<tr bgcolor=\"aliceblue\">\n");
	printf("<td style='border-width:0;'  ></td>\n");
	printf("<td style='border-width:0;'  ><small><FONT SIZE=\"1\" FACE=\"Verdana, Arial\"><b>Paper ID</b></td>\n");
	printf("<td style='border-width:0;'  ></td>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"1\" FACE=\"Verdana, Arial\"><b>Title</b></td>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"1\" FACE=\"Verdana, Arial\"><b>Due Date</b></td>\n");
	//printf("<td style='border-width:0;'  ><FONT SIZE=\"1\" FACE=\"Verdana, Arial\"><b>Status</b></td>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"1\" FACE=\"Verdana, Arial\"><b> </b></td>\n");
	
  $i=1;
	while($paper_row = mysqli_fetch_array( $paperresult )){

		$status =$paper_row['status'];
if($paper_row['accepttoreview']=="yes"){
		printf("<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.background='yellow';\" onmouseout= \"this.style.background='white';\">

			<td style='border-width:0;'  >%d</td>
			<td style='border-width:0;'  ><small>%s</td>
			<td style='border-width:0;'  ><img src=\"pdf.gif\">
			<td style='border-width:0;'  ><small><a href=\"papers/%s\"> %s </a></td>
			<td style='border-width:0;'  ><small>September 30, 2012</td>
			\n", $i++, $paper_row[ 'id' ],
			$paper_row[ 'attachmentwithoutauthor' ], $paper_row[ 'title' ]);

               //Disabled 
	       printf("<td style='border-width:0;'   align=center><form action=\"conf_performReview.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");

	       
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $paper_row[ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);
		printf("<input type=\"hidden\" value=\"true\"  name=\"nrv\"/>\n");

		printf("<input type=\"submit\" value=\"Review\"  />\n");	
    		printf("</form></td>\n"); 
}else{
		printf("<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.background='yellow';\" onmouseout= \"this.style.background='white';\">

			<td style='border-width:0;'  >%d</td>
			<td style='border-width:0;'  ><small>%s</td>
			<td style='border-width:0;'  ><img src=\"pdf.gif\">
			<td style='border-width:0;'  ><small> %s</td>
			<td style='border-width:0;'  ><small>September 30, 2012</td>
			\n", $i++, $paper_row[ 'id' ],
			 $paper_row[ 'title' ]);

               //Disabled 
	       printf("<td style='border-width:0;'   align=center><form action=\"conf_acceptToReview.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"acceptToReview\">\n");

	       
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $paper_row[ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);
		printf("<input type=\"hidden\" value=\"true\"  name=\"nrv\"/>\n");

		printf("<input type=\"submit\" name=\"accept\" value=\"Accept to Review\"  /><input type=\"submit\" name=\"reject\" value=\"Reject to Review\"  />\n");	
    		printf("</form></td>\n"); 

}


	}

	printf("</table>

\n");

	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'  ><div><table width=95%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"1\" FACE=\"Verdana, Arial\" >___</td>\n");
	printf("</table></div>

\n");
	
	printf("</table>\n");

	
	
}	





function displayTrackChairPaperList1( $uid,  $start)
{

	$numPapers = $paperList[ 'numPapers' ] ;
	$count = 0;
	for($i=0; $i < $numPapers ; $i++){
		$revID = $paperList[$i]['rev1'];
		if(strcmp($revID, $uid) == 0){
			$myPaperList[$count]['id'] 		= $paperList[$i]['id'];
			$myPaperList[$count]['authorID'] 	= $paperList[$i]['authorID'];
			$myPaperList[$count]['title'] 		= $paperList[$i]['title'];
			$myPaperList[$count]['attachment'] 	= $paperList[$i]['attachment'];
			$myPaperList[$count]['status'] 		= $paperList[$i]['status'];
			$myPaperList[$count]['rev1'] 		= $paperList[$i]['rev1'];
			$myPaperList[$count]['rev2'] 		= $paperList[$i]['rev2'];
			$myPaperList[$count]['rev3'] 		= $paperList[$i]['rev3'];

			$count++;
		}

	}

	$numPapers = $count;
	$end = $numPapers;
	
	if($numPapers==0) {
		return;
	}

	$userNum = getUserNumById($usersList, $uid);
	printf("<table width=100%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'  ><div><table width=95%% align=center border=0 cellpading=0 cellspacing=5>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"3\" FACE=\"Verdana, Arial\" ><i>My Track List : %s</i></td>\n", $usersList[$userNum]['category']);
	printf("</table></div>

\n");
			
	
   
	printf("<table width=95%% align=center border=0 cellpading=0 cellspacing=3>\n");

	for($i=$start; $i < $end ; $i++){

		printf("<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.background='yellow';\" onmouseout= \"this.style.background='white';\">\n");
		printf("<td style='border-width:0;'  ><table width=100%% border=0 cellpading=0 cellspacing = 1>\n");
			printf("<tr>\n");
			
			printf("<td style='border-width:0;'  >%d)</td>\n", $i+1);
			printf("<td style='border-width:0;'   width=15%%><b>ID</b>: %s</td>\n", $myPaperList[$i][ 'id' ]);
			printf("<td style='border-width:0;'   width=50%%  align = center valign=top><a href=\"conf_showPaperReview.php?id=%s&pid=%s\"><img src=\"info.png\" alt=\"Show Reviews\"/>Reviews</a></td>\n", $uid,$myPaperList[$i][ 'id' ]);
			printf("<td style='border-width:0;'  ><form action=\"conf_acceptPaper.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $myPaperList[$i][ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);
		printf("<input type=\"hidden\" value=\"accepted\"  name=\"status\"/>\n");
    		printf("<input type=\"submit\" value=\"Accept\"  />\n");	
    		printf("</form>\n"); 

		 printf("<td style='border-width:0;'  ><form action=\"conf_acceptPaper.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $myPaperList[$i][ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);
		printf("<input type=\"hidden\" value=\"rejected\"  name=\"status\"/>\n");
    		printf("<input type=\"submit\" value=\"Reject\"  />\n");	
    		printf("</form></td>\n"); 

		printf("<td style='border-width:0;'  ><form action=\"conf_acceptPaper.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $myPaperList[$i][ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);
		printf("<input type=\"hidden\" value=\"Not reviewed\"  name=\"status\"/>\n");
    		printf("<input type=\"submit\" value=\"Reset\" title=\"Reset decision to 'Not reviewed' \" />\n");	
    		printf("</form>\n"); 

/*
		printf("<td style='border-width:0;'  ><form action=\"conf_emailPaperReviews.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $myPaperList[$i][ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);
		printf("<input type=\"hidden\" value=\"rejected\"  name=\"status\"/>\n");
    		printf("<input type=\"submit\" value=\"Email Reviews\"   title=\"Email Reviews and decision to author\"/>\n");	
    		printf("</form></td>\n"); 
*/

 printf("<td style='border-width:0;'  >\n");



		printf("<tr>\n");
		printf("<td style='border-width:0;'  >\n");
		printf("<td style='border-width:0;'   colspan=6><a href=\"papers/%s\"> <img src=\"pdf.gif\"> %s </a></td>\n",
				$myPaperList[$i][ 'attachment' ], 
				$myPaperList[$i][ 'title' ]);
			
			$userNum1 = getUserNumById($usersList, $myPaperList[$i][ 'rev2' ]);
			$userNum2 = getUserNumById($usersList, $myPaperList[$i][ 'rev3' ]);
			

			$status = getReviewStatus($myPaperList[$i][ 'id' ], $usersList[$userNum1]['id']);
			printf("<tr>\n");
			printf("<td style='border-width:0;'  ></td>\n");
			if($status == "Completed") printf("<td style='border-width:0;'   align=right><img src=\"check2.png\"></td>\n");
			else printf("<td style='border-width:0;'  ></td>\n");
			printf("<td style='border-width:0;'   colspan=1>Reviewer 1: <a href= \"mailto:%s\" title=\"Send email to %s %s\">%s %s</a></td>\n",
				$usersList[$userNum1]['email'], $usersList[$userNum1]['firstname'], $usersList[$userNum1]['lastname'], $usersList[$userNum1]['firstname'], $usersList[$userNum1]['lastname']);

			printf("<td style='border-width:0;'   colspan=4>Status: <i>%s</i></td>\n", $status);

			$status = getReviewStatus($myPaperList[$i][ 'id' ], $usersList[$userNum2]['id']);
			printf("<tr>\n");
			printf("<td style='border-width:0;'  ></td>\n");
			if($status == "Completed") printf("<td style='border-width:0;'   align=right><img src=\"check2.png\"></td>\n");
			else printf("<td style='border-width:0;'  ></td>\n");
			printf("<td style='border-width:0;'   colspan=1>Reviewer 2: <a href= \"mailto:%s\" title=\"Send email to %s %s\">%s %s</a></td>\n",
			$usersList[$userNum2]['email'], $usersList[$userNum2]['firstname'], $usersList[$userNum2]['lastname'], $usersList[$userNum2]['firstname'], $usersList[$userNum2]['lastname']);

			printf("<td style='border-width:0;'   colspan=4>Status: <i>%s</i></td>\n", $status);

			
			printf("<tr>\n");
			printf("<td style='border-width:0;'  ></td>\n");
			printf("<td style='border-width:0;'  ></td>\n");
			printf("<td style='border-width:0;'  ></td>\n");

			$status = $myPaperList[$i][ 'status' ];
			if( $status == "accepted")
				printf("<td style='border-width:0;'   colspan=4 align=right>Final Decision: <FONT color=\"#00aa00\"><b> %s</b></td>\n", $status);
			else if( $status == "rejected")
				printf("<td style='border-width:0;'   colspan=4 align=right>Final Decision: <FONT color=\"#cc0000\"><b> %s</b></td>\n", $status);
			else printf("<td style='border-width:0;'   colspan=4 align=right>Final Decision: <b> %s</b></td>\n", $status);

		   
		

		printf("</table>\n");

		
	}

	printf("</table>

\n");

	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'  ><div><table width=95%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"1\" FACE=\"Verdana, Arial\" >___</td>\n");
	printf("</table></div>

\n");
	
	printf("</table>\n");

	
	
}	





function displayReviewerPaperList1($uid,  $start)
{

	$numPapers = $paperList[ 'numPapers' ] ;
	$count = 0;
	for($i=0; $i < $numPapers ; $i++){
		$revID = $paperList[$i]['rev2'];
		if(strcmp($revID, $uid) == 0){
			$myPaperList[$count]['id'] 		= $paperList[$i]['id'];
			$myPaperList[$count]['authorID'] 	= $paperList[$i]['authorID'];
			$myPaperList[$count]['title'] 		= $paperList[$i]['title'];
			$myPaperList[$count]['attachment'] 	= $paperList[$i]['attachment'];
			$myPaperList[$count]['status'] 		= $paperList[$i]['status'];

			$count++;
		}
		$revID = $paperList[$i]['rev3'];
		if(strcmp($revID, $uid) == 0){
			$myPaperList[$count]['id'] 		= $paperList[$i]['id'];
			$myPaperList[$count]['authorID'] 	= $paperList[$i]['authorID'];
			$myPaperList[$count]['title'] 		= $paperList[$i]['title'];
			$myPaperList[$count]['attachment'] 	= $paperList[$i]['attachment'];
			$myPaperList[$count]['status'] 		= $paperList[$i]['status'];

			$count++;
		}
		
	}

	$numPapers = $count;
	$end = $numPapers;
	
	if($numPapers==0) {
		return;
	}


	printf("<table width=100%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'  ><div><table width=95%% align=center border=0 cellpading=0 cellspacing=5>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"2\" FACE=\"Verdana, Arial\" ><i>Papers in Review List: <b>%s</b></i></td>\n", $numPapers);

	printf("</table></div>

\n");
			
	
   
	printf("<table width=95%% align=center border=0 cellpading=0 cellspacing=1>\n");
	printf("<tr bgcolor=\"aliceblue\">\n");
	printf("<td style='border-width:0;'  ></td>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Paper ID</b></td>\n");
	printf("<td style='border-width:0;'  ></td>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Title</b></td>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Due Date</b></td>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Status</b></td>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"2\" FACE=\"Verdana, Arial\"><b>Perform Review</b></td>\n");
	

	for($i=$start; $i < $end ; $i++){

		$status = getReviewStatus($myPaperList[$i][ 'id' ], $uid);

		printf("<tr bgcolor=\"#ffffff\" onmouseover=\"this.style.background='yellow';\" onmouseout= \"this.style.background='white';\">

			<td style='border-width:0;'  >%d</td>
			<td style='border-width:0;'  >%s</td>
			<td style='border-width:0;'  ><img src=\"pdf.gif\">
			<td style='border-width:0;'  ><a href=\"papers/%s\"> %s </a></td>
			<td style='border-width:0;'  >9/15/2010</td>
			<td style='border-width:0;'  >%s</td>\n", $i+1, $myPaperList[$i][ 'id' ],
			$myPaperList[$i][ 'attachment' ], $myPaperList[$i][ 'title' ], $status);
		   
		printf("<td style='border-width:0;'   align=center><form action=\"conf_performReview.php\"  accept-charset=\"UTF-8\" method=\"get\" id=\"user-register\">\n");
    		printf("<input type=\"hidden\" value=\"%s\"  name=\"pid\"/>\n", $myPaperList[$i][ 'id' ]);
		printf("<input type=\"hidden\" value=\"%s\"  name=\"id\"/>\n", $uid);
		printf("<input type=\"hidden\" value=\"true\"  name=\"nrv\"/>\n");

		printf("<input type=\"submit\" value=\"Review\"  />\n");	
    		printf("</form></td>\n"); 

		
	}

	printf("</table>

\n");

	printf("<tr bgcolor=\"c2c9cf\">\n");
	printf("<td style='border-width:0;'  ><div><table width=95%% align=center border=0 cellpading=0 cellspacing=0>\n");
	printf("<td style='border-width:0;'  ><FONT SIZE=\"1\" FACE=\"Verdana, Arial\" >___</td>\n");
	printf("</table></div>

\n");
	
	printf("</table>\n");

	
	
}	

?>