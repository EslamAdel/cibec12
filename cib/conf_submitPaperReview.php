<?php
session_start();

if(!isset($_SESSION['id']))
  {
    $str = sprintf("Location: submit.html");
		header($str);
}

	include("conn.php");
	include("conf_common.php");
	//include("conf_readPaperReview.php");

	$date = date("Y-m-d");

	$pid = $_POST['pid'];
	$uid = $_POST['id'] ;

	//echo $userNum;
	//echo "<br>".$paperNum;

  mysqli_query($link, "update rev set language=$_POST[language],clarity=$_POST[clarity],relevance=$_POST[relevance],comments='".mysqli_real_escape_string($link, $_POST["comments"])."',accept='$_POST[status]',merit=$_POST[merit],analysis=$_POST[analysis],verification=$_POST[verification],literature=$_POST[literature],application=$_POST[application],date='$date',done=1  where rev=$uid and pid=$pid ");

//-------------------------------------------------------------------------
//Send email to track chair
//--------------------------------------------------------------------------

  $tcresult=mysqli_query($link, "SELECT paper.*,user.*,paper.title as ptitle from paper,user,track where user.id=track.chair and track.name=paper.track and paper.id=$pid ");
  $tc_row = mysqli_fetch_array( $tcresult );
            
	$email  = $tc_row['email'];
	$subject = "CIBEC 2012 Paper Number " . $pid . " Reviews Submitted";
	
	$message = "Dear Dr. " . $tc_row['firstname'] . " " . $tc_row['lastname'] . "\n<br>";
	$message .= "\n<br>This email is to confirm that reviews have been submitted to the following paper:";
        $message .="\n<br>Paper Number: " . $pid ;
	$message .="\n<br>Paper Title: ".  $tc_row['ptitle'] . ".";
	$message .="\n<br><br>Please logon to your CIBEC 2012 account (www.cibec2012.org/submit.html) to check the reviews.";
	$message .="\n<br><br>Thank you very much for your contributions to CIBEC 2012.\n<br><br>CIBEC 2012 Infostructure Chair.";

       sendEmailToTrackChair($email, $subject, $message);
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
				
//redirect to prevent resubmission of form in case of user hitting backspace
$str = sprintf("Location: conf_showPaperList.php?id=%s&start=0&nu=false", $uid);
header($str);
					

        	




function sendEmailToTrackChair($to, $subject, $message)
{

	//$to ="mohamed.nooman@gmail.com";
	$bcc ="review@cibec2012.org";
	
     	$headers = "From: review@cibec2012.org\r\n";
	$headers .= "Reply-To: review@cibec2012.org\r\n";
	$headers .= "Return-Path: review@cibec2012.org\r\n";
	$headers .= "Bcc: ".$bcc."\r\n";

	
      	$headers .= "Content-type: text/html\r\n"; 

	
        //echo "<br>". $to;
	//echo "<br>". $subject;
	//echo "<br>". $message;
        
         
        if (mail($to,$subject,$message,$headers) ) {
	   echo "email sent";
	} else {
	   echo "email could not be sent";
	}

}




?>